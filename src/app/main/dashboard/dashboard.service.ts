import { Injectable } from '@angular/core';
import { NetworkService } from '../../shared/network.service';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private networkservice:NetworkService) { }

  dashboardCount() {
    return this.networkservice.get("api/dashboard/",null,null,'bearer')
  }
  getblog(userId:any) {
    return this.networkservice.get("api/blog?offset=0&limit=3&userId="+userId,null,null,'bearer')
  }
  deleteBlog(blogId:any) {
    return this.networkservice.delete("api/blog/"+blogId,null,null,'bearer')
  }

}
