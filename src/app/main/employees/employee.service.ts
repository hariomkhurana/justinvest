import { Injectable } from '@angular/core';
import { NetworkService } from '../../shared/network.service';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  constructor(private networkservice:NetworkService) { }


  getResourceApi(query:any) {
    return this.networkservice.get("api/resource?"+query,null, null,"bearer");
  }
  getResourceApiBySkill(skillId:any) {
    return this.networkservice.get("api/skill/resource/"+skillId,null, null,"bearer");
  }
  networkListApi(query:any) {
    return this.networkservice.get("api/network?"+query,null, null,"bearer");
  }
  getEmployeeAssmentApi(query:any) {
    return this.networkservice.get("api/userassessment/list?"+query,null, null,"bearer");
  }
  sendApprovalReq(body:any) {
    return this.networkservice.delete("api/userassessment/request/action",body, null,"bearer");
  }
  getEmployeeAssignment(query:any){
    return this.networkservice.get("api/userassessment/list?"+query,null, null,"bearer");
  }
  submitAssessment(body:any) {
    return this.networkservice.post("api/userassessment/submit",body, null,"bearer");
  }
  getskillHistory(skillId:any){
    return this.networkservice.get("api/userassessment/skill/audit/"+skillId,null, null,"bearer");
  }

  uploadPdf(image: any) {
    const formData = new FormData();
    formData.append("image",image);
    return this.networkservice.uploadImages("api/s3upload/image-upload", formData, null, "bearer");
  }

  
}
