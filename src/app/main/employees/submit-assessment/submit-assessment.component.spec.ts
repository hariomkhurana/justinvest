import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubmitAssessmentComponent } from './submit-assessment.component';

describe('SubmitAssessmentComponent', () => {
  let component: SubmitAssessmentComponent;
  let fixture: ComponentFixture<SubmitAssessmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubmitAssessmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmitAssessmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
