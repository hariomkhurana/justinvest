import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalDismissReasons, NgbModal, NgbModalOptions, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import { FullComponent } from '../../../layouts/full/full.component';
import { EmployeeService } from '../employee.service';
import * as moment from 'moment';
import { element } from 'protractor';
@Component({
  selector: 'app-submit-assessment',
  templateUrl: './submit-assessment.component.html',
  styleUrls: ['./submit-assessment.component.scss']
})
export class SubmitAssessmentComponent implements OnInit {
  @ViewChild(FullComponent,null) full: FullComponent;
  isLoading:boolean = false;
  isLoading1:boolean = false
  isSubmit:boolean = false;
  noteText:string = "";
  
  assementList: Array<object> = [];
  attachments:Array<object> = [];
  assementListTemp:Array<object> = [];
  modelOptions: NgbModalOptions = {
    backdrop: "static",
    keyboard: false
  };
  isNewSubmit:boolean = false;
  closeResult: string;
  assessmentId:string
  private modalRef: NgbModalRef;
  Upload = {
    name:"",
    url:""
  }
  activeIndex:any
  ;
  constructor( private modalService: NgbModal,
    private EmployeeService:EmployeeService,
    private _snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private router: Router,
    ) { }

  ngOnInit() {
    this.route.queryParams.subscribe(param => {
      this.assessmentId = param['id'];
      this.getAssessmentDetail();
      //this.getresourceList();
      this.activeIndex = localStorage.getItem('activeIndex')
      // this.fullC();
    });

  }

  assementdetail:any;
  managerNote:any;
  assementHead:any;
  attachmentData:any;
  attachmentDataEmp:any;
  attachmentDataManager:any;
  resourceList:any;
  tempData:any;
  
  getAssessmentDetail() {
    this.isLoading = true;
    let query ="userAssessmentId="+this.assessmentId;
		this.EmployeeService.getEmployeeAssignment(query).subscribe (
		  res => {
			this.isLoading = false;
			if ((res["message"] = "Success")) {
        this.assementdetail = res.data.assessmentList[0];
        this.assementHead = res.data.assessmentList;
        this.assementList =  res.data.assessmentList[0]['skills'];
        
        var i;
        for (i = 0; i < this.assementList.length; i++) {
          this.assementListTemp.push({status:this.assementList[i]['status'],skillLevel:this.assementList[i]['skillLevel']});
        }
        this.assementHead.forEach(element => {
          if(element['track'] == 'clientCare') element['tracklist'] = 'Client Care'
          if(element['track'] == 'patientCare') element['tracklist'] = 'Patient Care'
          if(element['track'] == 'leadership') element['tracklist'] = 'Leadership'
          if(element['track'] == 'hospitalOnboarding') element['tracklist'] = 'Hospital Operations'
         });
        this.noteText =res.data["assessmentList"][0]['notes'];
        if(res.data.assessmentList[0]['managerNotes'] && res.data.assessmentList[0]['managerNotes'].length == 0) {
          this.managerNote = ""
        } else {
          this.managerNote = res.data.assessmentList[0]['managerNotes']
        }
        this.assementList.forEach(element => {
          element["basic"] =false;
          element["intermediate"] =false;
          element['mastered'] = false;
          element["isBasicD"] =false;
          element["isInterD"] =false;
          element['isMasterD'] = false;
        });
        this.setAssessmenData();
        if (res.data.assessmentList[0]["managerAttachments"]) {

          this.attachmentDataManager = res.data.assessmentList[0]["managerAttachments"][0];
        }
      
        if (res.data.assessmentList[0]["attachments"]) {
          this.attachments = res.data.assessmentList[0]["attachments"];
        }
      
			} else {
			  this.assementList = [];
			}      },
		  err => {
			this.isLoading = false;
		  }
		);
  }
  closeModal(){
    this.modalRef.close();
  }

  setAssessmenData() {
    let total = 0;
    this.assementList.forEach((element) => {
      if (element["skillLevel"] == 'NA') {
        total = total +1;
      }
      if (element["skillLevel"] == "basic") {
        if(element['status'] !== 'pending' ) {
          element["basic"] = true;
          element['intermediate'] = false;
          element['isBasicD'] = true;
          element['isMasterD'] = true;
          element['isBasicCL'] = true;
          element['isInterCL'] = false;
          element['isMasterCL'] = false;
        }else {
          element["basic"] = false;
          element['isBasicD'] = false;
          element['isInterD'] = true;
          element['isMasterD'] = true;
          element['intermediate'] = false;
          element['isBasicCL'] = false;
          element['isInterCL'] = false;
          element['isMasterCL'] = false;
         
        }

      }
      if (element["skillLevel"] == "intermediate") {
        if(element['status'] !== 'pending' ) {
          element["basic"] = true;
          element['intermediate'] = true;
          element['isBasicD'] = true;
          element['isInterD'] = true;
          element['isMasterD'] = false;
          element['isBasicCL'] = true;
          element['isInterCL'] = true;
          element['isMasterCL'] = false;

          
        }else {
          element["intermediate"] = false;
          element['isMasterD'] = true;
          element['mastered'] = true;
          element['isBasicCL'] = true;
          element['isInterCL'] = false;
          element['isMasterCL'] = false;
         
        }

      }
      if (element["skillLevel"] == "mastered") {
        if(element['status'] !== 'pending' && element['status'] !== 'rejected' ) {
          element["basic"] = true;
          element['intermediate'] = true;
          element['mastered'] = true;
          element['isBasicD'] = true;
          element['isInterD'] = true;
          element['isMasterD'] = true;
          element['isBasicCL'] = true;
          element['isInterCL'] = true;
          element['isMasterCL'] = true;
        }else {
          element["intermediate"] = true;
          element["basic"] = true;
          element['isMasterD'] = false;
          element['isBasicD'] = true;
          element['isInterD'] = true;
          element['mastered'] = false;
          element['isBasicCL'] = true;
          element['isInterCL'] = true;
          element['isMasterCL'] = false;
         
        }
      }
      if (element["skillLevel"] == "NA" ) {
        if(element['status'] == 'pending') {
          element['skillLevel'] ="basic"
          element['isBasicD'] = false;
          element['isInterD'] = true;
          element['isMasterD'] = true;
          element['isBasicCL'] = false;
          element['isInterCL'] = false;
          element['isMasterCL'] = false;
      }
    }
      
      
    });
    if (this.assementList.length == total ) {
        this.assementList.forEach(element => {
          element['skillLevel'] ="basic"
          element['isBasicD'] = false;
          element['isInterD'] = true;
          element['isMasterD'] = true;
          element['isBasicCL'] = false;
          element['isInterCL'] = false;
          element['isMasterCL'] = false;
          
        });
}
  }

  auditHistory:any;
  skillDetail:any;
  HistoryModalOpen(row,content,btn) {
    this.modelOptions = {
      backdrop: "static",
      keyboard: false,
      centered:true,
      size:'lg',
    };
  this.isLoading = true;
  this.skillDetail = row;
    this.EmployeeService.getskillHistory(row.skillId).subscribe (
		  res => {
			this.isLoading = false;
			if ((res["message"] = "Success")) {
       this.auditHistory = res.data.auditLogList;
       this.auditHistory.forEach(element => {
        if(element.status == 'underObservation') {
          element.status = 'Under Observation'
        }
       });
       btn && btn.parentElement && btn.parentElement.parentElement && btn.parentElement.parentElement.blur();
    this.modalRef = this.modalService.open(content, this.modelOptions);
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );

     

			} else {
			 
			}      },
		  err => {
			this.isLoading = false;
		  }
		);
     }
 ResourceModalOpen(row,content,btn) {
      
      this.modelOptions = {
        backdrop: "static",
        keyboard: false,
        centered:true,
        size:'lg',
      };
    this.resourceList = row;
    this.resourceList.forEach(element => {
      element['fullTitle'] = element['title']
      element['titleLength'] = element['title'].length; 
      element['descriptionLength'] = element['description'].length; 
      if(element['titleLength'] > 20) {
       element["title"] = element["title"].substring(0,15) + '...';
      }
      if(element['descriptionLength'] > 20) {
        element["descriptionDispt"] = element["description"].substring(0,15) + '...';
       }
      
    });
   btn && btn.parentElement && btn.parentElement.parentElement && btn.parentElement.parentElement.blur();
this.modalRef = this.modalService.open(content, this.modelOptions);
this.modalRef.result.then(
  result => {
    this.closeResult = `Closed with: ${result}`;
  },
  reason => {
    this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
  });
       }
     formatTime(epoc) {
      let time =   moment(epoc).format("MMM Do YY");
      return time;
     }
  basicChange(index,value) {
    this.isSubmit  = true;
    if (value == true) {
      this.assementList[index]['basic'] =true;
      this.assementList[index]['skillLevel'] ="basic";
      this.assementList[index]['status'] ="approved";
      this.assementList[index]['intermediate'] =false;
      this.assementList[index]['mastered'] =false;
      this.assementList[index]['isInterD'] =true;
      this.assementList[index]['isMasterD'] =true;

      //color flag
      this.assementList[index]['isBasicCL'] = false;
      this.assementList[index]['isMasterCL'] = false;
    } else {
      this.assementList[index]['basic'] =false;
      this.assementList[index]['skillLevel'] ="NA";
      this.assementList[index]['status'] ="pending";
    }
    if(this.assementListTemp[index]['status'] == 'approved' && this.assementListTemp[index]['skillLevel'] == 'basic' ) {
      this.assementList[index]['status'] ="approved";
    }

  }
  intermediateChange(index,value) {
    this.isSubmit  = true;
    if (value == true) {
      this.assementList[index]['basic'] =true;
      this.assementList[index]['intermediate'] =true;
      this.assementList[index]['mastered'] =false;
      this.assementList[index]['skillLevel'] ="intermediate";
      this.assementList[index]['status'] ="approved";
      this.assementList[index]['isBasicD'] =true;
      this.assementList[index]['isMasterD'] =true;
      
      //color flag
      this.assementList[index]['isInterCL'] = false;
      this.assementList[index]['isMasterCL'] = false;
    } else {
      this.assementList[index]['skillLevel'] ="NA";
      this.assementList[index]['status'] ="pending";
      this.assementList[index]['intermediate'] =false;
      
    }
    if(this.assementListTemp[index]['status'] == 'approved' && this.assementListTemp[index]['skillLevel'] == 'intermediate' ) {
      this.assementList[index]['status'] ="approved";
      
    }
    
    
  }
  masteredChange(index,value) {

    this.isSubmit  = true;


    if (value == true) {
      this.assementList[index]['basic'] =true;
      this.assementList[index]['intermediate'] =true;
      this.assementList[index]['mastered'] =true;
      this.assementList[index]['skillLevel'] ="mastered";
      this.assementList[index]['status'] ="submitted";
      this.assementList[index]['isBasicD'] =true;
      this.assementList[index]['isInterD'] =true;
    } else {
      this.assementList[index]['mastered'] =false;
      this.assementList[index]['skillLevel'] ="NA";
      this.assementList[index]['status'] ="pending";
    }
    if(this.assementListTemp[index]['status'] == 'approved' && this.assementListTemp[index]['skillLevel'] == 'mastered' ) {
      this.assementList[index]['status'] ="approved";
      
    }
    
  }
  ValidateStarred(index,value) {

    this.assementList[index]['isStarred'] = value;
  }
 
 
  fileNamess:any;
  uploadAttachment(event) {
    this.attachments = [];
     this.isLoading1 = true;
     this.selectedFile = event.target.files[0];
     this.fileNamess =event.target.files[0].name;
    // this.Upload =  this.Upload.substr(length-10,length);
       if (this.selectedFile.type !=='application/pdf') {
         this.isLoading1 = false;
         Swal.fire({
           icon: 'error',
           html: 'Unsupported File Type. Only Pdf is allowed!'
       });
           return
       }
    // console.log('selected FIle',this.selectedFile)
     this.EmployeeService.uploadPdf(this.selectedFile).subscribe(res => {

       if (res["message"] == "Success") { 
         this.isLoading1 = false;
         this.attachments.push({name:this.fileNamess,type:'doc',url:res.data.imageUrl});
         this.isSubmit = true;
        //  this.Upload.name = this.fileNamess
        //  this.Upload.url = res.data.imageUrl
        //  console.log(this.Upload)
     //    console.log('attachmentFile',this.attachmentFile)
         this.selectedFile = null;
       } else {
         
       }
     }, err => {
      this.isLoading1 = false
     });
   }

  //  fullC () {
  //   let fullVal =  this.full.getHeader();
  //    console.log(fullVal,"HEADER VAL");
  //  }



   isNote:boolean = false;
   submitData(){
     let skill =[];
     this.assementList.forEach(element => {
       if(element['skillLevel'] =='mastered' && element['status'] == 'submitted') {
         this.isNewSubmit = true;
       }
       skill.push({name:element['name'],skillLevel:element['skillLevel'],status:element['status'],skillId:element['skillId'],originalSkillId:element['originalSkillId'],isStarred:element['isStarred']})
     });
     let request= {
      userAssessmentId:this.assessmentId,
      skills:skill,
      isNewSubmit:this.isNewSubmit
     }
     if(this.attachments.length > 0) {
       request['attachments'] = this.attachments;
     }
     if(this.noteText !== '') {
       request['notes']= this.noteText;
     }
     this.isLoading = true;
    this.EmployeeService.submitAssessment(request).subscribe(res => {
      this.isLoading = false;
      if (res["message"] == "Success") { 
        this.navBack();
        this._snackBar.open("Assessment submitted successfully!!","",{
          duration: 3000,
          horizontalPosition:'right',
          verticalPosition:'top',
          panelClass: ['failure']
        });
       
      } else {
        
      }
    }, err => {
     this.isLoading = false
    });
   }
 
  selectedFile: File;
  //  changeText() {
  //   if(this.noteText == '') {
  //     this.isNote = true;
  //     return;
  //    }else {
  //      this.isNote = false;
  //    }
  //  }

   navBack() {
    if(this.activeIndex == 0) {
      this.router.navigate(["main/employee/dashboard"]);
    } if (this.activeIndex == 2 ) {
      this.router.navigate(["main/employee/list-assessment"]);
    }
   }

   openwebView(value) {
    let index =value.indexOf('http');
    let url  = '';
    if (index == -1) {
         url = 'http://' + value ;
    }
    else 
    {
        url = value ;
    }
    window.open(url, "_blank");
  }

    // END ----- Update user status block
    private getDismissReason(reason: any): string {
      if (reason === ModalDismissReasons.ESC) {
        return "by pressing ESC";
      } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
        return "by clicking on a backdrop";
      } else {
        return `with: ${reason}`;
      }
    }

}

