import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardMain } from '../../auth/auth-guard.service';
import { SubmitAssessmentComponent } from './submit-assessment/submit-assessment.component';
import { AssessmentListComponent } from './assessment-list/assessment-list.component';
import { EmployeeDashboardComponent } from './employee-dashboard/employee-dashboard.component';
import { EmployeeManagementComponent } from './employee-management/employee-management.component';
import { EmployeeResourceListComponent } from './employee-resource-list/employee-resource-list.component';
import { ChangePasswordComponent } from './change-password/change-password.component';

const routes: Routes = [
  {path:"",component: EmployeeManagementComponent,
  canActivate: [AuthGuardMain],
  data: { role: ["employee"] },
  children:[
    { path: '', redirectTo: '/main/employee', pathMatch: 'full' },
    {path:"dashboard",component: EmployeeDashboardComponent},
    {path:"list-resource",component: EmployeeResourceListComponent},
    {path:"list-assessment",component: AssessmentListComponent},
    {path:"submit-assessment",component: SubmitAssessmentComponent},
    
    {path:"change-password",component: ChangePasswordComponent},

  ]
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeeRoutingModule { }
