import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmployeeRoutingModule } from './employee-routing.module';
import { EmployeeDashboardComponent } from './employee-dashboard/employee-dashboard.component';
import { EmployeeResourceListComponent } from './employee-resource-list/employee-resource-list.component';
import { EmployeeManagementComponent } from './employee-management/employee-management.component';
import { SharedModule } from '../../shared/shared.module';
import { AssessmentListComponent } from './assessment-list/assessment-list.component';
import { SubmitAssessmentComponent } from './submit-assessment/submit-assessment.component';
import {ProgressBarModule} from "angular-progress-bar"
import {MatExpansionModule} from '@angular/material/expansion';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { EmployeeService } from './employee.service';
import { ManagerService } from '../manager/manager.service';
@NgModule({
  declarations: [EmployeeDashboardComponent, EmployeeResourceListComponent,EmployeeManagementComponent, AssessmentListComponent, SubmitAssessmentComponent, ChangePasswordComponent],
  imports: [
    CommonModule,
    EmployeeRoutingModule,
    SharedModule,
    ProgressBarModule,
    MatExpansionModule
  ],providers:[EmployeeService,ManagerService]
})
export class EmployeeModule { }
