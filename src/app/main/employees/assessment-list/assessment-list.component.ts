import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import {
  ModalDismissReasons,
  NgbModal,
  NgbModalOptions,
  NgbModalRef,
} from "@ng-bootstrap/ng-bootstrap";
import { EmployeeService } from "../employee.service";
import { MatSnackBar } from "@angular/material";
import { CommonService } from "../../../shared/common.service";
import * as moment from 'moment';
import Swal from "sweetalert2";
@Component({
  selector: 'app-assessment-list',
  templateUrl: './assessment-list.component.html',
  styleUrls: ['./assessment-list.component.scss']
})
export class AssessmentListComponent implements OnInit {
  constructor(
    private modalService: NgbModal,
    private EmployeeService: EmployeeService,
    private _snackBar: MatSnackBar,
    private _common:CommonService
  ) {}
  assessmentList: Array<object> = [];
  isLoading: boolean = false;
  modelOptions: NgbModalOptions = {
    backdrop: "static",
    keyboard: false,
  };
  closeResult: string;
  private modalRef: NgbModalRef;


  isBlue: boolean = true;
  isOrange: boolean = true;
  isGreen: boolean = true;
  isLoading1:boolean = false;
  showlevel1: boolean = false;
  showlevel2: boolean = false;
  showlevel3: boolean = false;
  ngOnInit() {
    this.SetPage();
    this.getAssessmentList();
    // this.pdfTable.nativeElement;
  }
  total: number = 139;
  pagelimit: number = 10;
  totalPage: number;
  currentPage: number = 1;
  SetPage(){
    this.totalPage = this.total/this.pagelimit
    this.totalPage = Math.ceil(this.totalPage);
    if(this.totalPage == 1 && this.total <= 10) {
   //   this.totalPage = 0;
    }
  }
  nextPage() {
    this.currentPage = this.currentPage + 1;
    let query =
      "limit=" +
      this.pagelimit +
      "&offset= " +
      this.pagelimit * this.currentPage;
  }
  prevPage() {
    this.currentPage = this.currentPage - 1;
    let query =
      "limit=" +
      this.pagelimit +
      "&śoffset= " +
      this.pagelimit * this.currentPage;
  }

  openModal(content, btn) {
    this.modelOptions = {
      backdrop: "static",
      keyboard: false,
      size: "xl",
      centered: true,
    };
    btn &&
      btn.parentElement &&
      btn.parentElement.parentElement &&
      btn.parentElement.parentElement.blur();
    this.modalRef = this.modalService.open(content, this.modelOptions);
    this.modalRef.result.then(
      (result) => {
        this.closeResult = `Closed with: ${result}`;
      },
      (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }
skills:any;
  openSkillModal(row,content, btn) {
    this.modelOptions = {
      backdrop: "static",
      keyboard: false,
      size: "md",
      centered: true,
    };
    this.skills = row;
    btn &&
      btn.parentElement &&
      btn.parentElement.parentElement &&
      btn.parentElement.parentElement.blur();
    this.modalRef = this.modalService.open(content, this.modelOptions);
    this.modalRef.result.then(
      (result) => {
        this.closeResult = `Closed with: ${result}`;
      },
      (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  getAssessmentList() {
    this.isLoading = true;
    let userData = this._common.getUser();
    let query = "userId="+userData.userId;
		this.EmployeeService.getEmployeeAssmentApi(query).subscribe (
		  res => {
			this.isLoading = false;
			if ((res["message"] = "Success")) {

       this.assessmentList = res.data.assessmentList;
       this.total = this.assessmentList.length;
       this.assessmentList.forEach(element => {

        
          element["titleLength"] = element["title"].length;
          element["titleOriginal"] = element["title"];
         
          if (element["titleLength"] > 20) {
            element["title"] = element["title"].substring(0, 15) + "...";
          }

        if(element['track'] == 'clientCare') element['tracklist'] = 'Client Care'
        if(element['track'] == 'patientCare') element['tracklist'] = 'Patient Care'
        if(element['track'] == 'leadership') element['tracklist'] = 'Leadership'
        if(element['track'] == 'hospitalOnboarding') element['tracklist'] = 'Hospital Operations'
        element['remaining']=0;
        element['total']=0;
       });
       this.SetPage();

      this.setAssessmentLevel();
      this.validatePercentage();
			} else {
			  this.assessmentList = [];
			}      },
		  err => {
			this.isLoading = false;
		  }
		);
  }
  getText(text,title) {
    
    Swal.fire({
      title:title,
      text:text
    });
  }

  setAssessmentLevel(){
    this.assessmentList.forEach(element => {
      element["level"] 
      element["track"]
      if(element["level"] =='1' && element["track"] =='clientCare') element["displayLevel"] = "C1";
      if(element["level"] =='2' && element["track"] =='clientCare') element["displayLevel"] = "C2";
      if(element["level"] =='3' && element["track"] =='clientCare') element["displayLevel"] = "C3";

      if(element["level"] =='1' && element["track"] =='patientCare') element["displayLevel"] = "P1";
      if(element["level"] =='2' && element["track"] =='patientCare') element["displayLevel"] = "P2";
      if(element["level"] =='3' && element["track"] =='patientCare') element["displayLevel"] = "P3";

      if(element["level"] =='1' && element["track"] =='hospitalOnboarding') element["displayLevel"] = "H1";
      if(element["level"] =='2' && element["track"] =='hospitalOnboarding') element["displayLevel"] = "H2";
      if(element["level"] =='3' && element["track"] =='hospitalOnboarding') element["displayLevel"] = "H3";

      if(element["level"] =='1' && element["track"] =='leadership') element["displayLevel"] = "L1";
      if(element["level"] =='2' && element["track"] =='leadership') element["displayLevel"] = "L2";
      if(element["level"] =='3' && element["track"] =='leadership') element["displayLevel"] = "L3";

      // if(element["level"] =='1' && element["track"] =='basic') element["displayLevel"] = "B1";
      // if(element["level"] =='2' && element["track"] =='basic') element["displayLevel"] = "B2";
      // if(element["level"] =='3' && element["track"] =='basic') element["displayLevel"] = "B3";

      

    });
  }
  validatePercentage(){
    var i;
  
    for (i = 0; i < this.assessmentList.length; i++) {
      this.assessmentList[i]['skills'].forEach(element => {
        this.assessmentList[i]["total"] = this.assessmentList[i]["total"] +1;
        if(element['skillLevel'] !== 'NA') {
          this.assessmentList[i]['skillStatus'] = 'inProgress'
        } else {
          this.assessmentList[i]['skillStatus'] = 'Pending'
        }
        if(element['skillLevel'] !== 'mastered') {
          this.assessmentList[i]["remaining"] = this.assessmentList[i]["remaining"] +1;
        }
        this.assessmentList[i]['percentage']= ((this.assessmentList[i]['remaining']/this.assessmentList[i]['total']) * 100).toFixed(0); 
        this.assessmentList[i]['percentage'] = 100 - parseInt(this.assessmentList[i]['percentage']);
      });

    }

  }

assessmentdetail:any;
  openModalDelete(row,content, btn) {
    this.modelOptions = {
      backdrop: "static",
      keyboard: false,
      size:'sm',
      centered:true
    };
    this.assessmentdetail  = row;
    btn && btn.parentElement && btn.parentElement.parentElement && btn.parentElement.parentElement.blur();
    this.modalRef = this.modalService.open(content, this.modelOptions);
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }
  sendApprovalRequest() {
    let req= {
      "userAssessmentId":this.assessmentdetail['userAssessmentId'],
      "isAssigned": true
    }
    this.EmployeeService.sendApprovalReq(req).subscribe (
		  res => {
			this.isLoading = false;
			if ((res["message"] = "Success")) {
      
          this._snackBar.open("Request send successfully!!", "", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "top",
            panelClass: ["success"],
          });
          this.modalRef.close();
          this.getAssessmentList();
			}      },
		  err => {
			this.isLoading = false;
		  }
		);

  }
  formatTime(epoc) {
    let time = moment(epoc).fromNow(true)
       return time +' ago';
   }


  // END ----- Update user status block
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }
}
