import { Component, OnInit } from '@angular/core';
import { CommonService } from '../shared/common.service';
import { StreamService } from '../shared/stream.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  constructor() { }

  ngOnInit() {
 
  }

}
