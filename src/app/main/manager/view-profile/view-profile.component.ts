import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ManagerService } from '../manager.service';
import Swal from 'sweetalert2'
import { EmployeeService } from '../../employees/employee.service';
import { CommonService } from '../../../shared/common.service';
import * as moment from 'moment';
import { ModalDismissReasons, NgbModal, NgbModalOptions, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-view-profile',
  templateUrl: './view-profile.component.html',
  styleUrls: ['./view-profile.component.scss']
})
export class ViewProfileComponent implements OnInit {
  userId;
  noteCount:number = 0;
  isLoading:boolean =false;
  activeIndex:any
  profileDetail:any;
  assessmentList: Array<object> = [];
  noteList: Array<object> = [];
  attachmentList: Array<object> = [];
  modelOptions: NgbModalOptions = {
    backdrop: "static",
    keyboard: false,
  };
  closeResult: string;
  private modalRef: NgbModalRef;
  assessmenId:string;

  constructor( private router: Router,
    private route:ActivatedRoute,
    private _common:CommonService,
    private managerService:ManagerService,
    private employeeService: EmployeeService,
    private modalService: NgbModal) { }

  ngOnInit() {
    this.activeIndex = localStorage.getItem('activeIndex')
    this.route.queryParams.subscribe(param => {
      this.userId = param['id'];
      if(param['id1']) {
        this.assessmenId = param['id1'];
      }
      this.getEmployee();
      this.getAssessmentList();
    });
  }
  profileData:any;
  getEmployee() {
    this.isLoading = true;
    let query= "userId=" + this.userId;
    this.managerService.getEmployeeProfileApi(query).subscribe (
      res => {
      this.isLoading = false;
      if ((res["message"] = "Success")) {
        this.profileDetail = res["data"]["userList"][0];
         if(this.profileDetail['mobile']) {
          this._mobileFormat("");
         }
       
      } else {
    
      }      },
      err => {
      this.isLoading = false;
      }
    );
  }
  NaviateT() {
    this.router.navigate(["/main/manager/list-employee"]);
  }

  backNavigate () {
    if(this.activeIndex == 0) {
      this.router.navigate(["main/manager/dashboard"]);
    }  if (this.activeIndex == 1 ) {
      this.router.navigate(["main/manager/list-employee"]);
    } 
    if (this.activeIndex == 3 ) {

      this.router.navigate(["/main/manager/employee-assessment"],{ queryParams: { id: this.assessmenId }});
    }
    if (this.activeIndex == 88 ) {
      this.router.navigate(["main/manager/all-assessments"]);
    }
    
}

  _mobileFormat(event: any) {
    var phone = this.profileDetail['mobile']
    phone = phone.replace('(', '').replace(')', '').replace(new RegExp('-', 'g'), '').replace(new RegExp(' ', 'g'), '')
    this.formatMobileNumber(phone)
  }
  mobileNumber:any;
  formatMobileNumber(phone: any) {
    var str = phone.toString();
    var strFormat;
    if (this.mobileNumber !== str) {
      this.mobileNumber = str;
      if (str.length <= 3)
        strFormat = '(' + str.substr(0, 3) + ')'
      if (str.length > 3 && str.length <= 6)
        strFormat = '(' + str.substr(0, 3) + ')' + '' + str.substr(3, 3);
      if (str.length > 6)
        strFormat = '(' + str.substr(0, 3) + ')' + ' ' + str.substr(3, 3) + '-' + str.substr(6, 4);
      this.profileDetail['mobile'] = strFormat
    }
  }
  openwebView(value) {
    let index =value.indexOf('http');
    let url  = '';
    if (index == -1) {
         url = 'http://' + value ;
    }
    else 
    {
        url = value ;
    }
    window.open(url, "_blank");
  }
  swalImg(image) {
    if(image == '') { 
      
      return
    }
    Swal.fire({
      imageUrl: image,
      imageWidth: 400,  
      imageHeight: 400
    });
  }


  getAssessmentList() {
    this.isLoading = true;
    let userData = this._common.getUser();
    let query = "userId="+this.userId;
		this.employeeService.getEmployeeAssmentApi(query).subscribe (
		  res => {
			this.isLoading = false;
			if ((res["message"] = "Success")) {

       this.assessmentList = res.data.assessmentList;

       this.assessmentList.forEach(element => {
         if(element['notes'].length > 0) {
          this.noteList.push(element['notes'])
         }
         element['fullTitle'] = element['title']
         element['titleLength'] = element['title'].length;  
         if(element['titleLength'] > 20) {
          element["title"] = element["title"].substring(0,15) + '...';
         }
         if(element['attachments'] && element['attachments'].length > 0) {
          this.attachmentList.push(element['attachments'][0])
         }
         if(element['track'] == 'clientCare') element['tracklist'] = 'Client Care'
         if(element['track'] == 'patientCare') element['tracklist'] = 'Patient Care'
         if(element['track'] == 'leadership') element['tracklist'] = 'Leadership'
         if(element['track'] == 'hospitalOnboarding') element['tracklist'] = 'Hospital Operations'
        element['remaining']=0;
        element['total']=0;
       });

      this.setAssessmentLevel();
      this.validatePercentage();
			} else {
			  this.assessmentList = [];
			}      },
		  err => {
			this.isLoading = false;
		  }
		);
  }
  openNoteModal(content, btn) {
    this.modelOptions = {
      backdrop: "static",
      keyboard: false,
      size: "md",
      centered: true,
    };
    btn &&
      btn.parentElement &&
      btn.parentElement.parentElement &&
      btn.parentElement.parentElement.blur();
    this.modalRef = this.modalService.open(content, this.modelOptions);
    this.modalRef.result.then(
      (result) => {
        this.closeResult = `Closed with: ${result}`;
      },
      (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }
  openAttachmentModal(content, btn) {
    this.modelOptions = {
      backdrop: "static",
      keyboard: false,
      size: "md",
      centered: true,
    };
    btn &&
      btn.parentElement &&
      btn.parentElement.parentElement &&
      btn.parentElement.parentElement.blur();
    this.modalRef = this.modalService.open(content, this.modelOptions);
    this.modalRef.result.then(
      (result) => {
        this.closeResult = `Closed with: ${result}`;
      },
      (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }
  getText(text,title) {
    
    Swal.fire({
      title:title,
      text:text
    });
  }

  formatTime(epoc) {
    let time = moment(epoc).fromNow(true)
       return time +' ago';
   }


   setAssessmentLevel(){
    this.assessmentList.forEach(element => {
      element["level"] 
      element["track"]
      if(element["level"] =='1' && element["track"] =='clientCare') element["displayLevel"] = "C1";
      if(element["level"] =='2' && element["track"] =='clientCare') element["displayLevel"] = "C2";
      if(element["level"] =='3' && element["track"] =='clientCare') element["displayLevel"] = "C3";

      if(element["level"] =='1' && element["track"] =='patientCare') element["displayLevel"] = "P1";
      if(element["level"] =='2' && element["track"] =='patientCare') element["displayLevel"] = "P2";
      if(element["level"] =='3' && element["track"] =='patientCare') element["displayLevel"] = "P3";

      if(element["level"] =='1' && element["track"] =='hospitalOnboarding') element["displayLevel"] = "H1";
      if(element["level"] =='2' && element["track"] =='hospitalOnboarding') element["displayLevel"] = "H2";
      if(element["level"] =='3' && element["track"] =='hospitalOnboarding') element["displayLevel"] = "H3";

      if(element["level"] =='1' && element["track"] =='leadership') element["displayLevel"] = "L1";
      if(element["level"] =='2' && element["track"] =='leadership') element["displayLevel"] = "L2";
      if(element["level"] =='3' && element["track"] =='leadership') element["displayLevel"] = "L3";

      // if(element["level"] =='1' && element["track"] =='basic') element["displayLevel"] = "B1";
      // if(element["level"] =='2' && element["track"] =='basic') element["displayLevel"] = "B2";
      // if(element["level"] =='3' && element["track"] =='basic') element["displayLevel"] = "B3";

      

    });
  }
  validatePercentage(){
    var i;
  
    for (i = 0; i < this.assessmentList.length; i++) {
      this.assessmentList[i]['skills'].forEach(element => {
        this.assessmentList[i]["total"] = this.assessmentList[i]["total"] +1;
        if(element['skillLevel'] !== 'mastered') {
          this.assessmentList[i]["remaining"] = this.assessmentList[i]["remaining"] +1;
        }
        this.assessmentList[i]['percentage']= ((this.assessmentList[i]['remaining']/this.assessmentList[i]['total']) * 100).toFixed(0); 
        this.assessmentList[i]['percentage'] = 100 - parseInt(this.assessmentList[i]['percentage']);
      });

    }


  }
    // END ----- Update user status block
    private getDismissReason(reason: any): string {
      if (reason === ModalDismissReasons.ESC) {
        return "by pressing ESC";
      } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
        return "by clicking on a backdrop";
      } else {
        return `with: ${reason}`;
      }
    }


}
