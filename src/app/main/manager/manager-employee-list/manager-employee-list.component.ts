
import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { ModalDismissReasons, NgbModal, NgbModalOptions, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ManagerService } from '../manager.service';
import Swal from "sweetalert2"
import { StreamService } from '../../../shared/stream.service';
@Component({
  selector: 'app-manager-employee-list',
  templateUrl: './manager-employee-list.component.html',
  styleUrls: ['./manager-employee-list.component.scss']
})
export class ManagerEmployeeListComponent implements OnInit,OnDestroy {
  isLoading:boolean = false;
  isLoading1:boolean = false
  managerList: Array<object> = [];
  message:string = "";
  subscribedPage:any;
  modelOptions: NgbModalOptions = {
    backdrop: "static",
    keyboard: false
  };
  closeResult: string;
  private modalRef: NgbModalRef;
  constructor( private modalService: NgbModal,
    private adminService:ManagerService,
    private _snackBar: MatSnackBar,
    private streamService: StreamService
    ) { }

  ngOnInit() {
    this.SetPage();
    this.getEmployeeList();
    this.subscribedPage =  this.streamService.getFilterSubscription().subscribe((data: string) => {
      this.message = data;
      this.getEmployeeList();
      })

  }
  ngOnDestroy(): void {
    this.subscribedPage.unsubscribe();
}
  
 
  mobileNumber:any;
  _mobileFormat(mobile: any) {
    var phone = mobile;
    phone = phone.replace('(', '').replace(')', '').replace(new RegExp('-', 'g'), '').replace(new RegExp(' ', 'g'), '')
    var str = phone.toString();
    var strFormat;
    if (this.mobileNumber !== str) {
      this.mobileNumber = str;
      if (str.length <= 3)
        strFormat = '(' + str.substr(0, 3) + ')'
      if (str.length > 3 && str.length <= 6)
        strFormat = '(' + str.substr(0, 3) + ')' + '' + str.substr(3, 3);
      if (str.length > 6)
        strFormat = '(' + str.substr(0, 3) + ')' + ' ' + str.substr(3, 3) + '-' + str.substr(6, 4);
      return strFormat;
    }
  }
  getText(text,title) {
    
    Swal.fire({
      title:title,
      text:text
    });
  }
  

  assessmenList:any;
  openModal(row,content, btn) {
    this.modelOptions = {
      backdrop: "static",
      keyboard: false,
      size:'md',
      centered:true
    };
    let query ="userId="+row;
    this.isLoading = true;
    this.adminService.getEmployeeAssignment(query).subscribe (
		  res => {
			this.isLoading = false;
			if ((res["message"] = "Success")) {
        
       this.assessmenList = res.data.assessmentList;
       this.assessmenList.forEach(element => {
        if(element['track'] == 'clientCare') element['tracklist'] = 'Client Care'
        if(element['track'] == 'patientCare') element['tracklist'] = 'Patient Care'
        if(element['track'] == 'leadership') element['tracklist'] = 'Leadership'
        if(element['track'] == 'hospitalOnboarding') element['tracklist'] = 'Hospital Operations'
       });
       
       btn && btn.parentElement && btn.parentElement.parentElement && btn.parentElement.parentElement.blur();
    this.modalRef = this.modalService.open(content, this.modelOptions);
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );

			} else {
			  this.managerList = [];
			}      },
		  err => {
			this.isLoading = false;
		  }
		);
    
  }
  
  openModalDelete(row,content, btn) {
    this.modelOptions = {
      backdrop: "static",
      keyboard: false,
      size:'sm',
      centered:true
    };

    btn && btn.parentElement && btn.parentElement.parentElement && btn.parentElement.parentElement.blur();
    this.modalRef = this.modalService.open(content, this.modelOptions);
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }
  openModalReset(row,content, btn) {
    this.modelOptions = {
      backdrop: "static",
      keyboard: false,
      size:'sm',
      centered:true
    };
    btn && btn.parentElement && btn.parentElement.parentElement && btn.parentElement.parentElement.blur();
    this.modalRef = this.modalService.open(content, this.modelOptions);
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  changePageLimit() {
    this.currentPage = 0;
    this.getEmployeeList();
  }
  getEmployeeList() {
    this.isLoading = true;
    let  query = "limit="+this.pagelimit +"&offset="+this.pagelimit * this.currentPage;
    let queryFilter = "limit="+this.pagelimit +"&offset=0"
    if(this.message !== '') {
      let query1 = "&name="+this.message;
      query = queryFilter +query1;
      this.currentPage = 0;
    }

    query=query.trim();
		this.adminService.getEmployeListApi(query).subscribe (
		  res => {
			this.isLoading = false;
			if ((res["message"] = "Success")) {
        
       this.managerList = res.data.userList;
       this.total = res.data.totalUsers;

       this.SetPage();

       this.managerList.forEach(element => {
        if(element['track'] == 'clientCare') element['tracklist'] = 'Client Care'
        if(element['track'] == 'patientCare') element['tracklist'] = 'Patient Care'
        if(element['track'] == 'leadership') element['tracklist'] = 'Leadership'
        if(element['track'] == 'hospitalOnboarding') element['tracklist'] = 'Hospital Operations'
       });
       this.managerList.forEach(element => {
        if(element['clientCareLevel'] == 0) element['levelDisplayC'] = 'C0'
        if(element['clientCareLevel'] == 1) element['levelDisplayC'] = 'C1'
        if(element['clientCareLevel'] == 2) element['levelDisplayC'] = 'C2'
        if(element['clientCareLevel'] == 3) element['levelDisplayC'] = 'C3'

        if(element['patientCareLevel'] == 0) element['levelDisplayT'] = 'P0'
        if(element['patientCareLevel'] == 1) element['levelDisplayT'] = 'P1'
        if(element['patientCareLevel'] == 2) element['levelDisplayT'] = 'P2'
        if(element['patientCareLevel'] == 3) element['levelDisplayT'] = 'P3'

        if(element['leadershipLevel'] == 0) element['levelDisplayL'] = 'L0'
        if(element['leadershipLevel'] == 1) element['levelDisplayL'] = 'L1'
        if(element['leadershipLevel'] == 2) element['levelDisplayL'] = 'L2'
        if(element['leadershipLevel'] == 3) element['levelDisplayL'] = 'L3'


       });
       this.managerList.forEach(element => {
        if(element['department'] == 'other') element['department'] = 'Other'
       });

  

       this.managerList.forEach(element => {
         element['fullTitle'] = element['title']

          element['titleLength'] = element['title'].length; 
        element['facilityLength'] = element['facility'].length; 
        if(element['titleLength'] > 20) {
         element["title"] = element["title"].substring(0,15) + '...';
        }
        if(element['facilityLength'] > 40) {
          element["facility"] = element["facility"].substring(0,20) + '...';
         }
       
        
      });


			} else {
			  this.managerList = [];
			}      },
		  err => {
			this.isLoading = false;
		  }
		);
  }

  total:number = 0;
  pagelimit :number = 10;
  totalPage:number
  currentPage:number =0;
 SetPage(){
   this.totalPage = 0
   this.totalPage = this.total/this.pagelimit
   this.totalPage = Math.ceil(this.totalPage);
   if(this.totalPage == 1 || this.total <= this.pagelimit) {
    this.totalPage = 1;
   }
 }
 nextPage(){
   this.currentPage = this.currentPage +1;
   this.getEmployeeList();
 }
 prevPage(){
   this.currentPage = this.currentPage -1;
   this.getEmployeeList();
 }


    // END ----- Update user status block
    private getDismissReason(reason: any): string {
      if (reason === ModalDismissReasons.ESC) {
        return "by pressing ESC";
      } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
        return "by clicking on a backdrop";
      } else {
        return `with: ${reason}`;
      }
    }

}
