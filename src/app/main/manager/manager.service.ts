import { Injectable } from '@angular/core';
import { NetworkService } from '../../shared/network.service';

@Injectable({
  providedIn: 'root'
})
export class ManagerService {


  constructor(private networkservice:NetworkService) { }


  getResourceApi(body:any) {
    return this.networkservice.get("api/resource?",body, null,"bearer");
  }
  getResourceApiBySkill(skillId:any) {
    return this.networkservice.get("api/skill/resource/"+skillId,null, null,"bearer");
  }
  getEmployeListApi(query:any) {
    return this.networkservice.get("api/user/employee/list?"+query,null, null,"bearer");
  }
  getEmployeeProfileApi(query:any) {
    return this.networkservice.get("api/user/employee/list?"+query,null, null,"bearer");
  }
  getEmployeeProfileAdminApi(query:any) {
    return this.networkservice.get("api/user/userList?"+query,null, null,"bearer");
  }
  networkListApi(query:any) {
    return this.networkservice.get("api/network?"+query,null, null,"bearer");
  }
  AddnetworkApi(body:any) {
    return this.networkservice.post("api/network",body, null,"bearer");
  }

  getAssessmentListApi (query:any) {
    return this.networkservice.get("api/assessment?",null, null,"bearer");
  }
  geteligibleEmployee (assessmentId:any) {
    return this.networkservice.get("api/userassessment/employee/"+assessmentId,null, null,"bearer");
  }
  assignEmployee(body:any) {
    return this.networkservice.post("api/userassessment/assign",body, null,"bearer");
  }
  getEmployeeAssignment(query:any){
    return this.networkservice.get("api/userassessment/list?"+query,null, null,"bearer");
  }
  markedAssessment(body:any) {
    return this.networkservice.post("api/userassessment/marked",body, null,"bearer");
  }
  markSkill(body:any) {
    return this.networkservice.post("api/userassessment/skill/mark",body, null,"bearer");
  }
  getskillHistory(skillId:any){
    return this.networkservice.get("api/userassessment/skill/audit/"+skillId,null, null,"bearer");
  }

  uploadPdf(image: any) {
    const formData = new FormData();
    formData.append("image",image);
    return this.networkservice.uploadImages("api/s3upload/image-upload", formData, null, "bearer");
  }

  changePassword (body:any) {
    return this.networkservice.post("api/user/password/change",body, null,"bearer");
  }
  
}
