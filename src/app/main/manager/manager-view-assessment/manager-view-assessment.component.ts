import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { ModalDismissReasons, NgbModal, NgbModalOptions, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ManagerService } from '../manager.service';
import Swal from "sweetalert2"
@Component({
  selector: 'app-manager-view-assessment',
  templateUrl: './manager-view-assessment.component.html',
  styleUrls: ['./manager-view-assessment.component.scss']
})
export class ManagerViewAssessmentComponent implements OnInit {

  isLoading:boolean = false
  assessmentList: Array<object> = [];
  modelOptions: NgbModalOptions = {
    backdrop: "static",
    keyboard: false
  };
  closeResult: string;
  private modalRef: NgbModalRef;
  constructor( private modalService: NgbModal,
    private managerService:ManagerService,
    private _snackBar: MatSnackBar,
    ) { }

  ngOnInit() {
    this.SetPage();
    this.getEmployeeList();
  }
  total:number = 0;
  pagelimit :number = 10;
  totalPage:number
  currentPage:number =0;
  SetPage(){
    this.totalPage = this.total/this.pagelimit
    this.totalPage = Math.ceil(this.totalPage);
    if(this.totalPage == 1 && this.total <= 10) {
     // this.totalPage = 0;
    }
  }
  nextPage(){
    this.currentPage = this.currentPage +1;
   this.getEmployeeList()
  }
  prevPage(){
    this.currentPage = this.currentPage -1;
    this.getEmployeeList();
  }
  openModal(row,content, btn) {
    this.modelOptions = {
      backdrop: "static",
      keyboard: false,
      size:'sm',
      centered:true
    };
    btn && btn.parentElement && btn.parentElement.parentElement && btn.parentElement.parentElement.blur();
    this.modalRef = this.modalService.open(content, this.modelOptions);
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  assessmentData:any
  viewAssessmentModal(row,content, btn) {
    this.modelOptions = {
      backdrop: "static",
      keyboard: false,
      size:'md',
      centered:true
    };
    this.assessmentData = row;
    btn && btn.parentElement && btn.parentElement.parentElement && btn.parentElement.parentElement.blur();
    this.modalRef = this.modalService.open(content, this.modelOptions);
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }
  getText(text,title) {
    
    Swal.fire({
      title:title,
      text:text
    });
  }

  getEmployeeList() {
    this.isLoading = true;
    let  query = "limit="+this.pagelimit+"&offset="+this.pagelimit * this.currentPage;
    query = query.trim();
		this.managerService.getAssessmentListApi(query).subscribe (
		  res => {
			this.isLoading = false;
			if ((res["message"] = "Success")) {
       this.assessmentList = res.data.assessmentList;
       this.assessmentList.forEach(element => {
        if(element['track'] == 'clientCare') element['tracklist'] = 'Client Care'
        if(element['track'] == 'patientCare') element['tracklist'] = 'Patient Care'
        if(element['track'] == 'leadership') element['tracklist'] = 'Leadership'
        if(element['track'] == 'hospitalOnboarding') element['tracklist'] = 'Hospital Operations'
       });
       this.SetPage();

        this.assessmentList.forEach(element => {
          // element['descriptionLength'] = element['description'].length; 
          if(element['title'].length > 40) {
           element["titleDisp"] = element["title"].substring(0,20) + '...';
          }
          if(element['description'].length > 20) {
            element["descriptionDisp"] = element["description"].substring(0,15) + '...';
           }
        });

        this.setAssessmentLevel();
			} else {
			  this.assessmentList = [];
			}      },
		  err => {
			this.isLoading = false;
		  }
		);
  }

  setAssessmentLevel(){
    this.assessmentList.forEach(element => {
      element["level"] 
      element["track"]

      if(element["level"] ==1 && element["track"] =='clientCare') element["displayLevel"] = "C1";
      if(element["level"] ==2 && element["track"] =='clientCare') element["displayLevel"] = "C2";
      if(element["level"] ==3 && element["track"] =='clientCare') element["displayLevel"] = "C3";

      if(element["level"] ==1 && element["track"] =='patientCare') element["displayLevel"] = "P1";
      if(element["level"] ==2 && element["track"] =='patientCare') element["displayLevel"] = "P2";
      if(element["level"] ==3 && element["track"] =='patientCare') element["displayLevel"] = "P3";

      if(element["level"] =='1' && element["track"] =='leadership') element["displayLevel"] = "L1";
      if(element["level"] =='2' && element["track"] =='leadership') element["displayLevel"] = "L2";
      if(element["level"] =='3' && element["track"] =='leadership') element["displayLevel"] = "L3";

      if(element["level"] ==1 && element["track"] =='hospitalOnboarding') element["displayLevel"] = "H1";
      if(element["level"] ==2 && element["track"] =='hospitalOnboarding') element["displayLevel"] = "H2";
      if(element["level"] ==3 && element["track"] =='hospitalOnboarding') element["displayLevel"] = "H3";

    });
  }

    // END ----- Update user status block
    private getDismissReason(reason: any): string {
      if (reason === ModalDismissReasons.ESC) {
        return "by pressing ESC";
      } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
        return "by clicking on a backdrop";
      } else {
        return `with: ${reason}`;
      }
    }


}
