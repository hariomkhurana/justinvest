import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { ModalDismissReasons, NgbModal, NgbModalOptions, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import { StreamService } from '../../../shared/stream.service';
import { EmployeeService } from '../../employees/employee.service';
import { ManagerService } from '../manager.service';

@Component({
  selector: 'app-manager-list-resource',
  templateUrl: './manager-list-resource.component.html',
  styleUrls: ['./manager-list-resource.component.scss']
})
export class ManagerListResourceComponent implements OnInit {
 
  isLoading:boolean = false
  resourceList: Array<object> = [];
  tempData : Array<object> = [];
  message:string = "";
  subscribedPage:any;
  modelOptions: NgbModalOptions = {
    backdrop: "static",
    keyboard: false
  };
  closeResult: string;
  private modalRef: NgbModalRef;
  constructor( private modalService: NgbModal,
    private employeeService:EmployeeService,
    private _snackBar: MatSnackBar,
    private streamService:StreamService
    ) { }

  ngOnInit() {

    this.getresourceList();
   this.subscribedPage =  this.streamService.getFilterSubscription().subscribe((data: string) => {
      this.message = data;
      this.getresourceList();
      })
  }
  ngOnDestroy(): void {
    this.subscribedPage.unsubscribe();
}

  total:number = 139;
  pagelimit :number = 1000;
  totalPage:number
  currentPage:number =0;
  SetPage(){
    this.totalPage = this.total/this.pagelimit
    this.totalPage = Math.ceil(this.totalPage);
    if(this.totalPage == 1 && this.total <= 10) {
    //  this.totalPage = 0;
    }
  }
  getText(text,title) {
    
    Swal.fire({
      title:title,
      text:text
    });
  }
  nextPage(){
    this.currentPage = this.currentPage +1;
    this.getresourceList();
  }
  prevPage(){
    this.currentPage = this.currentPage -1;
    this.getresourceList();
  }
  openModal(row,content, btn) {
    this.modelOptions = {
      backdrop: "static",
      keyboard: false,
      size:'sm',
      centered:true
    };
    btn && btn.parentElement && btn.parentElement.parentElement && btn.parentElement.parentElement.blur();
    this.modalRef = this.modalService.open(content, this.modelOptions);
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }
  
  openModalDelete(row,content, btn) {
    this.modelOptions = {
      backdrop: "static",
      keyboard: false,
      size:'sm',
      centered:true
    };
    this.UserId= row;
    btn && btn.parentElement && btn.parentElement.parentElement && btn.parentElement.parentElement.blur();
    this.modalRef = this.modalService.open(content, this.modelOptions);
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }
  openModalReset(row,content, btn) {
    this.modelOptions = {
      backdrop: "static",
      keyboard: false,
      size:'sm',
      centered:true
    };
    btn && btn.parentElement && btn.parentElement.parentElement && btn.parentElement.parentElement.blur();
    this.modalRef = this.modalService.open(content, this.modelOptions);
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  groupBy(objectArray, property) {
    return objectArray.reduce((acc, obj) => {
       const key = obj[property];
       if (!acc[key]) {
          acc[key] = [];
       }
       // Add object to list for given key's value
       acc[key].push(obj);
       return acc;
    }, {});
 }

  categorySelect(data){
    this.tempData = [];
    this.tempData = data;
    this.tempData.forEach(element => {
     element['fullTitle'] = element['title']
   
    element['titleLength'] = element['title'].length; 
    element['descriptionLength'] = element['description'].length; 
    if(element['titleLength'] > 20) {
     element["title"] = element["title"].substring(0,15) + '...';
    }
    if(element['descriptionLength'] > 20) {
      element["descriptionDispt"] = element["description"].substring(0,15) + '...';
     }
  });
  this.tempData = this.tempData.reverse();
  }
   
   getresourceList() {
     this.isLoading = true;
     let  query = "limit="+this.pagelimit +"&offset="+this.pagelimit * this.currentPage;
     if(this.message !== '') {
       let query1 = "&title="+this.message;
       query = query +query1
     }
     // query = query.trim();
 
     this.employeeService.getResourceApi(query).subscribe (
       res => {
       this.isLoading = false;
       this.isLoading = false;
       if ((res["message"] = "Success")) {
        this.resourceList = res.data.resourceList;
       //  this.resourceList.forEach(element => {
       //    element["category"]= element["category"][0]
       //  });
 
      
  this.tempData = this.groupBy(this.resourceList, 'category');
  this.resourceList = []
  for (var key of Object.keys(this.tempData)) {
   this.resourceList.push({category:key,data:this.tempData[key]});
 }
 
       } else {
         this.resourceList = [];
       }      },
       err => {
       this.isLoading = false;
       }
     );
   }
 

  UserId:any
  resourceId:any
  openwebView(value) {
    let index =value.indexOf('http');
    let url  = '';
    if (index == -1) {
         url = 'http://' + value ;
    }
    else 
    {
        url = value ;
    }
    window.open(url, "_blank");
  }


    // END ----- Update user status block
    private getDismissReason(reason: any): string {
      if (reason === ModalDismissReasons.ESC) {
        return "by pressing ESC";
      } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
        return "by clicking on a backdrop";
      } else {
        return `with: ${reason}`;
      }
    }

}
