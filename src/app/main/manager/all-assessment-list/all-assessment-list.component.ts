import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { ModalDismissReasons, NgbModal, NgbModalOptions, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2'
import { ManagerService } from '../manager.service';
import { CommonService } from '../../../shared/common.service';
import { FormControl } from '@angular/forms';
import {Observable} from 'rxjs';
import { Router } from '@angular/router';
import * as moment from 'moment';
export interface User {
  name: string;
}
@Component({
  selector: 'app-all-assessment-list',
  templateUrl: './all-assessment-list.component.html',
  styleUrls: ['./all-assessment-list.component.scss']
})
export class AllAssessmentListComponent implements OnInit {
  myControl = new FormControl();
  options: User[] = [
    {name: 'Mary'},
    {name: 'Shelley'},
    {name: 'Igor'}
  ];
  filteredOptions: Observable<User[]>;

  total: number = 0;
  pagelimit: number = 10;
  totalPage: number;
  currentPage: number = 0;
 

allAssessment:any
  assessmentList:any;
  isLoading:boolean = false;
  isLoading1:boolean = false
  managerList: Array<object> = [];
  message:string = "";
  subscribedPage:any;
  selectedItem:Array<object> = [];
  modelOptions: NgbModalOptions = {
    backdrop: "static",
    keyboard: false
  };
  closeResult: string;
  private modalRef: NgbModalRef;
  constructor( private modalService: NgbModal,
    private ManagerService:ManagerService,
    private _snackBar: MatSnackBar,
    private _common: CommonService,
    private router:Router    ) { }

  ngOnInit() {
    this.getRequestedAssesment();
    this.getAssessmentList()
    this.getEmployeeList();
    // this.filteredOptions = this.myControl.valueChanges
    //   .pipe(
    //     startWith(''),
    //     map(value => typeof value === 'string' ? value : value.name),
    //     map(name => name ? this._filter(name) : this.options.slice())
    //   );
    // this.getManagerList();
    //  this.subscribedPage =  this.streamService.getFilterSubscription().subscribe((data: string) => {
    //     console.log(data,"DATA")
    //     this.message = data;
    //     console.log(this.message)
    //     this.getManagerList();
    //     })


        // console.log("MESSAGE")
      }

  //     ngOnDestroy(): void {
  //       console.log("Destroy")
  //       this.subscribedPage.unsubscribe();
  //     console.log("Destroyed")
  //  }

  displayFn(user: User): string {
    return user && user.name ? user.name : '';
  }

  private _filter(name: string): User[] {
    const filterValue = name.toLowerCase();

    return this.options.filter(option => option.name.toLowerCase().indexOf(filterValue) === 0);
  }
 

  SetPage() {
    this.totalPage = this.total / this.pagelimit;
    this.totalPage =Math.ceil(this.totalPage);
    if (this.totalPage == 1 && this.total <= 10) {
    //  this.totalPage = 0;
    }
  }
  nextPage() {
    this.currentPage = this.currentPage + 1;
    let query =
      "limit=" +
      this.pagelimit +
      "&offset=" +
      this.pagelimit * this.currentPage;
    this.getRequestedAssesment();
  }
  prevPage() {
    this.currentPage = this.currentPage - 1;
    let query =
      "limit=" +
      this.pagelimit +
      "&śoffset= " +
      this.pagelimit * this.currentPage;
    this.getRequestedAssesment();
  }




  status:string;
  rowdata:any;
  openModal(row,content, btn,status) {
    this.modelOptions = {
      backdrop: "static",
      keyboard: false,
      size:'sm',
      centered:true
    };
    this.rowdata = row;
    this.UserId = row.userId;
    this.status = status;
    if(this.status == 'active') {
      this.status = 'activate'
    }

    btn && btn.parentElement && btn.parentElement.parentElement && btn.parentElement.parentElement.blur();
    this.modalRef = this.modalService.open(content, this.modelOptions);
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  onReset() {
    this.isLoading = true;
    setTimeout(() => {
      // //     /** spinner ends after 5 seconds */
      this.isLoading = false;
    }, 2000);
    (this.searchUserStatus = "all"),
      (this.searchText = ""),
      (this.searchBy = ""),
      (this.searchTitle = ""),
      (this.searchEmpName = ""),
      (this.selectedItem = [])
      this.currentPage = 0;
    this.getRequestedAssesment();
  }

  skills:any;
  openSkillModal(row,content, btn) {
    this.modelOptions = {
      backdrop: "static",
      keyboard: false,
      size: "md",
      centered: true,
    };
    this.skills = row;
    btn &&
      btn.parentElement &&
      btn.parentElement.parentElement &&
      btn.parentElement.parentElement.blur();
    this.modalRef = this.modalService.open(content, this.modelOptions);
    this.modalRef.result.then(
      (result) => {
        this.closeResult = `Closed with: ${result}`;
      },
      (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  filterValue() {
  }
  
  openModalDelete(row,content, btn) {
    this.modelOptions = {
      backdrop: "static",
      keyboard: false,
      size:'sm',
      centered:true
    };
    this.UserId= row;
    btn && btn.parentElement && btn.parentElement.parentElement && btn.parentElement.parentElement.blur();
    this.modalRef = this.modalService.open(content, this.modelOptions);
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }
  openModalReset(row,content, btn) {
    this.modelOptions = {
      backdrop: "static",
      keyboard: false,
      size:'sm',
      centered:true
    };
    this.UserId = row;

    btn && btn.parentElement && btn.parentElement.parentElement && btn.parentElement.parentElement.blur();
    this.modalRef = this.modalService.open(content, this.modelOptions);
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }
 


 resourceDropDownFinal: Array<object> = [];
  searchText:any;
  settings1 = {};
  searchBy:any
  searchUserStatus='pending';
  searchTitle:any;
  searchEmpName:any
  emailVal:[]
  queryParams() {
    let query = "";
    if (this.searchUserStatus && this.searchUserStatus != "all") {
      query = query + "status=" + this.searchUserStatus + "&";
    }

    
    if (this.searchTitle && this.searchTitle != "all") {
      query = query + "assessmentId=" + this.searchTitle + "&";
    }

    if (this.searchEmpName && this.searchEmpName != "all") {
      query = query + "userId=" + this.searchEmpName + "&";
    }

    if (this.searchText !== "" && this.searchText !== undefined) {
      query = query + "email=" + this.searchText+"&";
    }
    if (this.selectedItem && this.selectedItem.length > 0 ) {
      query = query + "userId=" + this.selectedItem[0]['userId'] + "&" ;
    }
    // if (this.searchBy && this.searchBy == "email" && this.searchText && this.searchText !== "") {
    //   query = query + "email=" + this.searchText.toLowerCase().trim()+"&";
    // }
    return query;
  }


  // getAssessmentList() {
  //   this.isLoading = true;
  //   let query = "";
	// 	this.adminService.getAssessmentListApi(query).subscribe (
	// 	  res => {
	// 		this.isLoading = false;
	// 		if ((res["message"] = "Success")) {
  //      this.allAssessment = res.data.assessmentList;
	// 		} else {
	// 		  this.allAssessment = [];
	// 		}      },
	// 	  err => {
	// 		this.isLoading = false;
	// 	  }
	// 	);
  // }



  // getEmployeeList() {
  //   this.isLoading = true;

  //   let query = "";
  //   this.adminService.getEmployeListApi(query).subscribe(
  //     (res) => {
  //       this.isLoading = false;
  //       if ((res["message"] = "Success")) {
  //         this.allEmployee = res.data.userList;
  //         this.total = res.data.totalUsers;
  //         // this.allEmployee.forEach((element) => {
  //         //   if (element["department"][0] == "other")
  //         //     element["department"] = "Other";
  //         // });
  //         console.log("final", this.allEmployee);
  //       } else {
  //         this.allEmployee = [];
  //       }
  //     },
  //     (err) => {
  //       this.isLoading = false;
  //     }
  //   );
  // }


  // getRequestedAssesment() {
  //   let userData = this._common.getUser();
  //   let query = "&managerId=" + userData.userId +"&"+"limit="+this.pagelimit +"&offset="+this.pagelimit * this.currentPage;
  //   this.ManagerService.getEmployeeAssignment(query).subscribe(
  //     res => {
  //       this.isLoading = false;
  //       if ((res["message"] = "Success")) {
  //         this.assessmentList = res.data.assessmentList;
  //         this.total = res.data.totalCount;
  //       }
  //     },
  //     err => {
  //       this.isLoading = false;
  //     }
  //   );
  // }

  getRequestedAssesment() { 
    this.isLoading = true;
    let userData = this._common.getUser();
    let finalquery = this.queryParams();
    // let  query = "&managerId=" + userData.userId +"&"+"limit="+this.pagelimit +"&offset="+this.pagelimit * this.currentPage;
    let query = "userType=employee&managerId="+userData.userId +"&"+"limit="+this.pagelimit +"&offset="+this.pagelimit * this.currentPage;
    query  = finalquery + query;
   this.ManagerService.getEmployeeAssignment(query).subscribe (
     res => {
       this.isLoading = false;
       if ((res["message"] = "Success")) {
         this.assessmentList = res.data.assessmentList;
         this.assessmentList.forEach(element => {
          if(element['track'] == 'clientCare') element['tracklist'] = 'Client Care'
          if(element['track'] == 'patientCare') element['tracklist'] = 'Patient Care'
          if(element['track'] == 'leadership') element['tracklist'] = 'Leadership'
          if(element['track'] == 'hospitalOnboarding') element['tracklist'] = 'Hospital Operations'
          });

          this.assessmentList.forEach((element) => {
           element["titleLength"] = element["title"].length;
           element["titleOriginal"] = element["title"];
          
           if (element["titleLength"] > 20) {
             element["title"] = element["title"].substring(0, 15) + "...";
           }
         });
         this.total = res.data.totalCount;
         this.SetPage();
         this.assessmentList.forEach(element => {
           element['remaining'] = 0;
           element['total'] = element['skills'].length;
           
         });
         this.validatePercentage();
       }
     },
     err => {
       this.isLoading = false;
     }
   );
 }


 getAssessmentList() {
  this.isLoading = true;
  let query = "";
  this.ManagerService.getAssessmentListApi(query).subscribe (
    res => {
    this.isLoading = false;
    if ((res["message"] = "Success")) {
     this.allAssessment = res.data.assessmentList;
    //  this.total = this.assessmentList.length;
    //  this.assessmentList.forEach(element => {
    //   if(element['track'] == 'customerService') element['tracklist'] = 'Customer service'
    //   if(element['track'] == 'leadership') element['tracklist'] = 'Leadership'
    //   if(element['track'] == 'technical') element['tracklist'] = 'Technical'
    //  });
    } else {
      this.allAssessment = [];
    }      },
    err => {
    this.isLoading = false;
    }
  );
}
formatTime(epoc) {
  let time = moment(epoc).fromNow(true)
  return time + ' ago';
}
naviageAssessment(data) {
  this.router.navigate(["/main/manager/review-assessment"],{ queryParams: { id: data }});
}

allEmployee:any;
getEmployeeList() {
  this.isLoading = true;
  // let  query = "limit="+this.pagelimit +"&offset="+this.pagelimit * this.currentPage;
  // if(this.message !== '') {
  //   console.log("IN")
  //   let query1 = "&name="+this.message;
  //   query = query +query1
  //   console.log(query,"FINAL QUERY")
  // }

  // query = query.trim();
  let query = "";
  this.ManagerService.getEmployeListApi(query).subscribe(
    (res) => {
      this.isLoading = false;
      if ((res["message"] = "Success")) {
        this.allEmployee = res.data.userList;
        this.total = res.data.totalUsers;
        this.allEmployee.forEach(element => {
          element['fullName'] = element['firstName'] +" " +element['lastName'];
        });

        for (let i = 1; i < this.allEmployee.length; i++) {
          this.allEmployee[i]['id'] = i
      }
        this.settings1= {
          singleSelection: true,
          text: "Select Employee",
          labelKey: 'fullName',
          idField: 'userId',
          enableSearchFilter: true,
          badgeShowLimit: 1,
        scrollableHeight: '10px',
        scrollable: true,
        };
        // this.allEmployee.forEach((element) => {
        //   if (element["department"][0] == "other")
        //     element["department"] = "Other";
        // });
      } else {
        this.allEmployee = [];
      }
    },
    (err) => {
      this.isLoading = false;
    }
  );
}



 filterFun() {
  this.currentPage = 0
   this.getRequestedAssesment();
 }
 changePageLimit() {
   this.currentPage = 0
   this.getRequestedAssesment();
}


onItemSelect(item: any) {
  this.getRequestedAssesment();
}
  onDropDownClose1(){}

  OnItemDeSelect1(item:any){

    // this.updateResource();
    
  }

  onItemDeSelect(item:any) {
  }

 validatePercentage() {
  var i;
  for (i = 0; i < this.assessmentList.length; i++) {
    let totalRequest = 0;
    let totalUnderObs = 0;
    this.assessmentList[i]['skills'].forEach(element => {
      if(element['status'] == 'submitted') {
        totalRequest =  totalRequest +1;
        this.assessmentList[i]['totalRequest'] =  totalRequest;
      }
      if(element['status'] == 'underObservation') {
        totalUnderObs =  totalUnderObs +1;
        this.assessmentList[i]['totalUnderObs'] =  totalUnderObs;
      }
      if (element['skillLevel'] !== 'mastered') {
        this.assessmentList[i]["remaining"] = this.assessmentList[i]["remaining"] + 1;
      }
      this.assessmentList[i]['percentage'] = ((this.assessmentList[i]['remaining'] / this.assessmentList[i]['total']) * 100).toFixed(0);
      this.assessmentList[i]['percentage'] = 100 - parseInt(this.assessmentList[i]['percentage']);
    });
  }


}

//   getManagerList() {
//     this.isLoading = true;
//     let  query = "limit="+this.pagelimit +"&offset="+this.pagelimit * this.currentPage;
//     if(this.message !== '') {
//       console.log("IN")
//       let query1 = "&name="+this.message;
//       query = query +query1
//       console.log(query,"FINAL QUERY")
//     }

//     query = query.trim();
// 		this.adminService.getManagerListApi(query).subscribe (
// 		  res => {
// 			this.isLoading = false;
// 			if ((res["message"] = "Success")) {
// this.message = ""
//        this.managerList = res.data.userList;
//        this.managerList.forEach(element => {
//         if(element['department'][0] == 'other') element['department'] = 'Other'
      
//        });
//        this.total = res.data.totalUsers;
//        this.SetPage();

//        this.managerList.forEach(element => {
//          element['fullTitle'] = element['title']
//          element['fullFacility'] = element['facility']
//           element['titleLength'] = element['title'].length; 
//         element['facilityLength'] = element['facility'].length; 
//         if(element['titleLength'] > 40) {
//          element["title"] = element["title"].substring(0,20) + '...';
//         }
//         if(element['facilityLength'] > 10) {
//           element["facility"] = element["facility"].substring(0,8) + '...';
//          }
       
        
//       });


//         console.log('final',this.managerList)
// 			} else {
// 			  this.managerList = [];
// 			}      },
// 		  err => {
// 			this.isLoading = false;
// 		  }
// 		);
//   }
  UserId:any
  // deleteUser() {
  //   this.isLoading1 = true;
  //   this.adminService.deleteUser(this.UserId).subscribe((res) => {
  //     this.isLoading = false;
  //      if(res['status'] =='Success') {
  //      this.isLoading1 = false;
  //      this.modalRef.close();
  //      this._snackBar.open("Manager deleted successfully!!","",{
  //       duration: 3000,
  //       horizontalPosition:'right',
  //       verticalPosition:'top',
  //       panelClass: ['failure']
  //     });
  //    this.getManagerList();
  //      } 
  //    }, err => {
  //      this.isLoading1 = false;
  //    });
  // }
  getText(text,title) {
    
    Swal.fire({
      title:title,
      text:text
    });
  }

  _mobileFormat(mobile: any) {
    var phone = mobile
    phone = phone.replace('(', '').replace(')', '').replace(new RegExp('-', 'g'), '').replace(new RegExp(' ', 'g'), '')
    var str = phone.toString();
    var strFormat;
    if (this.mobileNumber !== str) {
      this.mobileNumber = str;
      if (str.length <= 3)
        strFormat = '(' + str.substr(0, 3) + ')'
      if (str.length > 3 && str.length <= 6)
        strFormat = '(' + str.substr(0, 3) + ')' + '' + str.substr(3, 3);
      if (str.length > 6)
        strFormat = '(' + str.substr(0, 3) + ')' + ' ' + str.substr(3, 3) + '-' + str.substr(6, 4);
      return strFormat;
    }
  }
  mobileNumber:any;
  formatMobileNumber(phone: any) {
    
  }

    // END ----- Update user status block
    private getDismissReason(reason: any): string {
      if (reason === ModalDismissReasons.ESC) {
        return "by pressing ESC";
      } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
        return "by clicking on a backdrop";
      } else {
        return `with: ${reason}`;
      }
    }


}
