import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubmitManagerAssessmentComponent } from './submit-manager-assessment.component';

describe('SubmitManagerAssessmentComponent', () => {
  let component: SubmitManagerAssessmentComponent;
  let fixture: ComponentFixture<SubmitManagerAssessmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubmitManagerAssessmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmitManagerAssessmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
