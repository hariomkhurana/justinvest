import { Component, OnInit } from "@angular/core";
import { MatSnackBar } from "@angular/material";
import { ActivatedRoute, Router } from "@angular/router";
import {
  ModalDismissReasons,
  NgbModal,
  NgbModalOptions,
  NgbModalRef,
} from "@ng-bootstrap/ng-bootstrap";
import Swal from "sweetalert2";
import { ManagerService } from "../manager.service";
import * as moment from 'moment';
@Component({
  selector: "app-submit-manager-assessment",
  templateUrl: "./submit-manager-assessment.component.html",
  styleUrls: ["./submit-manager-assessment.component.scss"],
})
export class SubmitManagerAssessmentComponent implements OnInit {
  isLoading: boolean = false;
  isLoading1: boolean = false;
  noteText: any;
  remarkText:any;
  assementList: Array<object> = [];
  attachments: Array<object> = [];
  resourceList: Array<object> = [];
  activeIndex:any
  skillDetail:any;
  Upload = {
    name:"",
    url:""
  }
  modelOptions: NgbModalOptions = {
    backdrop: "static",
    keyboard: false,
  };
  closeResult: string;
  assessmentId: string;
  employeeAttachment:any
  attachmentData: any;
  assementdetail: any;
  assessdata:any;
  private modalRef: NgbModalRef;
  constructor(
    private modalService: NgbModal,
    private ManagerService: ManagerService,
    private _snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.route.queryParams.subscribe((param) => {
  
      this.assessmentId = param["id"];
      this.getAssessmentDetail();
      this.activeIndex = localStorage.getItem('activeIndex')
    });
  }

  getAssessmentDetail() {
    this.isLoading = true;
    let query = "userAssessmentId=" + this.assessmentId;
    this.ManagerService.getEmployeeAssignment(query).subscribe(
      (res) => {
        this.isLoading = false;
        if ((res["message"] = "Success")) {
          this.assementdetail = res.data.assessmentList[0];
          this.assessdata = res.data.assessmentList
          this.assessdata.forEach(element => {
            if(element['track'] == 'clientCare') element['tracklist'] = 'Client Care'
      if(element['track'] == 'patientCare') element['tracklist'] = 'Patient Care'
      if(element['track'] == 'leadership') element['tracklist'] = 'Leadership'
      if(element['track'] == 'hospitalOnboarding') element['tracklist'] = 'Hospital Operations'
           });
          this.assementList = res.data["assessmentList"][0]["skills"];
          this.assementList.forEach((element) => {
            element["baisc"] = false;
            element["intermediate"] = false;
            element["mastered"] = false;
          });
          if (res.data.assessmentList[0]["managerAttachments"]) {
            this.attachments = res.data.assessmentList[0]["managerAttachments"];
          }
          this.noteText = res.data.assessmentList[0]['managerNotes']
          this.setAssessmenData();
          if (res.data.assessmentList[0]["attachments"]) {
            this.attachmentData = res.data.assessmentList[0]["attachments"][0];
          }
          
        } else {
          this.assementList = [];
        }
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }

  AcceptModalOpen(row,content,btn) {
    this.modelOptions = {
      backdrop: "static",
      keyboard: false,
      centered:true,
      size:'md',
    };
    this.skillDetail = row;
    this.remarkText = "";
    btn && btn.parentElement && btn.parentElement.parentElement && btn.parentElement.parentElement.blur();
    this.modalRef = this.modalService.open(content, this.modelOptions);
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  RejectModalOpen(row,content,btn) {
    this.modelOptions = {
      backdrop: "static",
      keyboard: false,
      centered:true,
      size:'md',
    };
    this.skillDetail = row;
    this.remarkText = "";
    btn && btn.parentElement && btn.parentElement.parentElement && btn.parentElement.parentElement.blur();
    this.modalRef = this.modalService.open(content, this.modelOptions);
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  auditHistory:any;
  HistoryModalOpen(row,content,btn) {
    this.modelOptions = {
      backdrop: "static",
      keyboard: false,
      centered:true,
      size:'lg',
    };
  this.isLoading = true;
  this.skillDetail = row;
    this.ManagerService.getskillHistory(row.skillId).subscribe (
		  res => {
			this.isLoading = false;
			if ((res["message"] = "Success")) {
       this.auditHistory = res.data.auditLogList;
       this.auditHistory.forEach(element => {
        if(element.status == 'underObservation') {
          element.status = 'Under Observation'
        }
       });
       btn && btn.parentElement && btn.parentElement.parentElement && btn.parentElement.parentElement.blur();
    this.modalRef = this.modalService.open(content, this.modelOptions);
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );

     

			} else {
			 
			}      },
		  err => {
			this.isLoading = false;
		  }
		);
     }
     ResourceModalOpen(row,content,btn) {
      
      this.modelOptions = {
        backdrop: "static",
        keyboard: false,
        centered:true,
        size:'lg',
      };
    this.resourceList = row;
    this.resourceList.forEach(element => {
      element['fullTitle'] = element['title']
      element['titleLength'] = element['title'].length; 
      element['descriptionLength'] = element['description'].length; 
      if(element['titleLength'] > 20) {
       element["title"] = element["title"].substring(0,15) + '...';
      }
      if(element['descriptionLength'] > 20) {
        element["descriptionDispt"] = element["description"].substring(0,15) + '...';
       }
      
    });
   btn && btn.parentElement && btn.parentElement.parentElement && btn.parentElement.parentElement.blur();
this.modalRef = this.modalService.open(content, this.modelOptions);
this.modalRef.result.then(
  result => {
    this.closeResult = `Closed with: ${result}`;
  },
  reason => {
    this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
  }
);
    
     }
  markedskillApprove(){
    let request = {
        "userAssessmentId":this.assementdetail['userAssessmentId'],
        "skillId":this.skillDetail['skillId'],
        "status": "approved",
        "remarks":this.remarkText,
        "isStarred":this.skillDetail['isStarred']
    
    }
    this.isLoading1 = true;
    if(request['remarks'] == '') delete request['remarks']
    this.ManagerService.markSkill(request).subscribe(
      (res) => {
        this.isLoading1 = false;
        if (res["message"] == "Success") {
          this.getAssessmentDetail();
          this.modalRef.close()
          this.noteText = "";
          this._snackBar.open("Skill approved successfully!!!!", "", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "top",
            panelClass: ["failure"],
          });
        } else {
        }
      },
      (err) => {
        this.isLoading1 = false;
      }
    );
    
  }
  markedskillReject(){
    let request = {
        "userAssessmentId":this.assementdetail['userAssessmentId'],
        "skillId":this.skillDetail['skillId'],
        "status": "rejected",
        "remarks":this.remarkText,
        "isStarred":this.skillDetail['isStarred']
    
    }
    if(request['remarks'] == '') delete request['remarks']
    this.isLoading1 = true;
    
    this.ManagerService.markSkill(request).subscribe(
      (res) => {
        this.isLoading1 = false;
        if (res["message"] == "Success") {
          this.getAssessmentDetail();
          this.modalRef.close();
          this.noteText = "";
          this._snackBar.open("Skill rejected successfully!!", "", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "top",
            panelClass: ["failure"],
          });
        } else {
        }
      },
      (err) => {
        this.isLoading1 = false;
      }
    );
  }
  formatTime(epoc) {
   
    let time =   moment(epoc).format("MMM Do YY");
    return time;
   }

 
  
   setAssessmenData() {
    let total = 0;
    this.assementList.forEach((element) => {
      if (element["skillLevel"] == 'NA') {
        total = total +1;
      }
      if (element["skillLevel"] == "basic") {
        if(element['status'] !== 'pending' ) {
          element["basic"] = true;
          element['intermediate'] = false;
          element['isBasicD'] = true;
          element['isMasterD'] = true;
          element['isBasicCL'] = true;
          element['isInterCL'] = false;
          element['isMasterCL'] = false;
        }else {
          element["basic"] = false;
          element['isBasicD'] = false;
          element['isInterD'] = true;
          element['isMasterD'] = true;
          element['intermediate'] = false;
          element['isBasicCL'] = false;
          element['isInterCL'] = false;
          element['isMasterCL'] = false;
         
        }

      }
      if (element["skillLevel"] == "intermediate") {
        if(element['status'] !== 'pending' ) {
          element["basic"] = true;
          element['intermediate'] = true;
          element['isBasicD'] = true;
          element['isInterD'] = true;
          element['isMasterD'] = false;
          element['isBasicCL'] = true;
          element['isInterCL'] = true;
          element['isMasterCL'] = false;

          
        }else {
          element["intermediate"] = false;
          element['isMasterD'] = true;
          element['mastered'] = true;
          element['isBasicCL'] = true;
          element['isInterCL'] = false;
          element['isMasterCL'] = false;
         
        }

      }
      if (element["skillLevel"] == "mastered") {
        if(element['status'] !== 'pending' && element['status'] !== 'rejected' ) {
          element["basic"] = true;
          element['intermediate'] = true;
          element['mastered'] = true;
          element['isBasicD'] = true;
          element['isInterD'] = true;
          element['isMasterD'] = true;
          element['isBasicCL'] = true;
          element['isInterCL'] = true;
          element['isMasterCL'] = true;
        }else {
          element["intermediate"] = true;
          element["basic"] = true;
          element['isMasterD'] = false;
          element['isBasicD'] = true;
          element['isInterD'] = true;
          element['mastered'] = false;
          element['isBasicCL'] = true;
          element['isInterCL'] = true;
          element['isMasterCL'] = false;
         
        }
      }
      if (element["skillLevel"] == "NA" ) {
        if(element['status'] == 'pending') {
          element['skillLevel'] ="basic"
          element['isBasicD'] = false;
          element['isInterD'] = true;
          element['isMasterD'] = true;
          element['isBasicCL'] = false;
          element['isInterCL'] = false;
          element['isMasterCL'] = false;
      }
    }
      
      
    });
    if (this.assementList.length == total ) {
        this.assementList.forEach(element => {
          element['skillLevel'] ="basic"
          element['isBasicD'] = false;
          element['isInterD'] = true;
          element['isMasterD'] = true;
          element['isBasicCL'] = false;
          element['isInterCL'] = false;
          element['isMasterCL'] = false;
          
        });
}
  }
  basicChange(index, value) {
    if (value == true) {
      this.assementList[index]["baisc"] = true;
      this.assementList[index]["skillLevel"] = "basic";
      this.assementList[index]["intermediate"] = false;
      this.assementList[index]["intermediate"] = false;
    } else {
      this.assementList[index]["baisc"] = false;
    }
  }
  intermediateChange(index, value) {
    if (value == true) {
      this.assementList[index]["baisc"] = false;
      this.assementList[index]["intermediate"] = true;
      this.assementList[index]["mastered"] = false;
      this.assementList[index]["skillLevel"] = "intermediate";
    } else {
      this.assementList[index]["baisc"] = false;
    }
  }
  masteredChange(index, value) {
    if (value == true) {
      this.assementList[index]["baisc"] = false;
      this.assementList[index]["intermediate"] = false;
      this.assementList[index]["mastered"] = true;
      this.assementList[index]["skillLevel"] = "mastered";
    } else {
      this.assementList[index]["mastered"] = false;
    }
  }
  ValidateStarred(index, value) {
    this.assementList[index]["isStarred"] = value;
  }
  attachmentFile = {
    name: "",
    type: "doc",
    url: "",
  };
  selectedFile: File;

 
  fileNamess:any;
  uploadAttachment(event) {
    this.attachments = [];
     this.isLoading1 = true;
     this.selectedFile = event.target.files[0];
     this.fileNamess =event.target.files[0].name;
    // this.Upload =  this.Upload.substr(length-10,length);
       if (this.selectedFile.type !=='application/pdf') {
         this.isLoading = false;
         Swal.fire({
           icon: 'error',
           html: 'Unsupported File Type. Only Pdf is allowed!'
       });
           return
       }
      this.isLoading =true;
     this.ManagerService.uploadPdf(this.selectedFile).subscribe(res => {
       this.isLoading = false;
       if (res["message"] == "Success") { 
         this.isLoading = false;
         this.attachments.push({name:this.fileNamess,type:'doc',url:res.data.imageUrl});
        //  this.Upload.name = this.fileNamess
        //  this.Upload.url = res.data.imageUrl
         this.selectedFile = null;
       } else {
         
       }
     }, err => {
      this.isLoading = false
     });
   }

  navBack() {
    if(this.activeIndex == 0) {
      this.router.navigate(["main/manager/dashboard"]);
    } 
    if (this.activeIndex == 1 ) {
      this.router.navigate(["main/manager/list-employee"]);
    } 
    if (this.activeIndex == 3 ) {
      this.router.navigate(["main/manager/list-assessment"]);
    }
    if (this.activeIndex == 88 ) {
      this.router.navigate(["main/manager/all-assessments"]);
    }
   }
   
  submitData() {
    this.isLoading =true 
    let request = {};
    if(this.attachments.length > 0) {
      request['managerAttachments'] = this.attachments;
    }
    if (this.noteText !== "") {
      request["managerNotes"] = this.noteText;
    }
   
    request['userAssessmentId']  = this.assementdetail['userAssessmentId']
    
    this.ManagerService.markedAssessment(request).subscribe (
      (res) => {
        this.isLoading = false;
        if (res["message"] == "Success") {

          if(this.activeIndex == 0) {
            this.router.navigate(["main/manager/dashboard"]);
          } 
          if (this.activeIndex == 1 ) {
            this.router.navigate(["main/manager/list-employee"]);
          }
          if (this.activeIndex == 3 ) {
            this.router.navigate(["main/manager/list-assessment"]);
          }
          if (this.activeIndex == 88 ) {
            this.router.navigate(["main/manager/all-assessments"]);
          }
          this._snackBar.open("Assessment updated successfully!!", "", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "top",
            panelClass: ["failure"],
          });
        } else {
        }
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }
  isEdit: boolean = false;
  editAssessment() {
    this.isEdit = true;
  }

  openwebView(value) {
    let index =value.indexOf('http');
    let url  = '';
    if (index == -1) {
         url = 'http://' + value ;
    }
    else 
    {
        url = value ;
    }
    window.open(url, "_blank");
  }

  backNav() {
    this.router.navigate(["main/manager/list-employee"]);
  }
  closeModal(){
    this.modalRef.close();
    this.remarkText = "";
  }
isUnderObservation:boolean = false;
  underObservation(row) {
   
    let request = {
      "userAssessmentId":this.assementdetail['userAssessmentId'],
      "skillId":row['skillId'],
      "status": "underObservation",
      "isStarred":row['isStarred']
  
  }
    this.ManagerService.markSkill(request).subscribe (
      (res) => {
        this.isLoading = false;
        if (res["message"] == "Success") {
          this.isUnderObservation = true
          this._snackBar.open("Status updated successfully!!", "", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "top",
            panelClass: ["failure"],
          });
          this.getAssessmentDetail();
        } else {
        }
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }

  // END ----- Update user status block
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }
}
