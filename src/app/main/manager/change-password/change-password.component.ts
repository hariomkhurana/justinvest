import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { CommonService } from '../../../shared/common.service';
import { StreamService } from '../../../shared/stream.service';
import { ManagerService } from '../manager.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  ChangePasswordForm:FormGroup;
  isLoading:boolean = false;

  constructor(    private fb: FormBuilder,
    private commonService: CommonService,
    private managerService:ManagerService,
    private router:Router,
    private _snackBar: MatSnackBar,) { }

    ngOnInit() {
      this.initialiseForms();
    }

    initialiseForms() {
      this.ChangePasswordForm = this.fb.group({
        oldPassword: ["", [Validators.required,Validators.minLength(6)]],
        newPassword: ["", [Validators.required,Validators.minLength(6)]],
      });
    }

    submitForm() {
      let req = this.ChangePasswordForm.value;
      this.isLoading = true;
      this.managerService.changePassword(req).subscribe((res) => {
        this.isLoading = false;
         if(res['message'] =='Success') {
         this.isLoading = false;
         this.ChangePasswordForm.reset();
         this._snackBar.open("Password changed successfully!!","",{
          duration: 3000,
          horizontalPosition:'right',
          verticalPosition:'top',
          panelClass: ['failure']
        });
         } 
       }, err => {
         this.isLoading = false;
       });
    }

}
