import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EligableEmployeeComponent } from './eligable-employee.component';

describe('EligableEmployeeComponent', () => {
  let component: EligableEmployeeComponent;
  let fixture: ComponentFixture<EligableEmployeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EligableEmployeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EligableEmployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
