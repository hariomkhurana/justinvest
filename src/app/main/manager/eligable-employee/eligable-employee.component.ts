import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalDismissReasons, NgbModal, NgbModalOptions, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ManagerService } from '../manager.service';
import Swal from "sweetalert2"
@Component({
  selector: 'app-eligable-employee',
  templateUrl: './eligable-employee.component.html',
  styleUrls: ['./eligable-employee.component.scss']
})
export class EligableEmployeeComponent implements OnInit {
  isLoading:boolean = false;
  isLoading1:boolean = false
  managerList: Array<object> = [];
  listValue:boolean =false;
  modelOptions: NgbModalOptions = {
    backdrop: "static",
    keyboard: false
  };
  closeResult: string;
  assessmentId:string
  private modalRef: NgbModalRef;
  constructor( private modalService: NgbModal,
    private adminService:ManagerService,
    private _snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private router: Router,
    ) { }

  ngOnInit() {
    this.route.queryParams.subscribe(param => {
      this.assessmentId = param['id'];
      this.getEmployeeList();
    });

  }
  total:number = 0;
  pagelimit :number = 10;
  totalPage:number
  currentPage:number =0;
  SetPage(){
    this.totalPage = this.total/this.pagelimit
    this.totalPage = Math.ceil(this.totalPage);
    if(this.totalPage == 1 && this.total <= 10) {
      //this.totalPage = 0;
    }
  }
  mobileNumber:any;
  employeeList:any

  nextPage(){
    this.currentPage = this.currentPage +1;
    this.getEmployeeList();
  }
  prevPage(){
    this.currentPage = this.currentPage -1;
    this.getEmployeeList();
  }
  isSubmit:boolean = false;
  changeEmployee(index,value) {
    let count=0;
    this.managerList.forEach(element => {
      if(element['assessmentStatus'] == true) {
        count = count +1;
      }
    });
 if(count > 0) {
   this.isSubmit = true;
 }else {
   this.isSubmit = false;
 }
 // for selected 
 if (this.managerList.length == count ) {
   this.listValue  = true;
 } else {
  this.listValue =false;
 }
 

  }
  assignEmployee(){
    let temp=[]
    this.managerList.forEach(element => {
      if(element['assessmentStatus'] == true) {
      temp.push(element['userId']);
      }

    }); 
    let request =
      {
        "assessmentId": this.assessmentId,
        "userIdList":temp
      }
      this.isLoading = true;
      this.adminService.assignEmployee(request).subscribe (
        res => {
        this.isLoading = false;
        if ((res["message"] = "Success")) {

          this._snackBar.open("Assessment assigned successfully!!","",{
            duration: 3000,
            horizontalPosition:'right',
            verticalPosition:'top',
            panelClass: ['failure']
          });
          this.router.navigate(["/main/manager/list-assessment"]);
        } else {
          this.managerList = [];
        }      },
        err => {
        this.isLoading = false;
        }
      );
    
  }
  
  _mobileFormat(mobile: any) {
    var phone = mobile;
    phone = phone
      .replace("(", "")
      .replace(")", "")
      .replace(new RegExp("-", "g"), "")
      .replace(new RegExp(" ", "g"), "");
    var str = phone.toString();
    var strFormat;
    if (this.mobileNumber !== str) {
      this.mobileNumber = str;
      if (str.length <= 3) strFormat = "(" + str.substr(0, 3) + ")";
      if (str.length > 3 && str.length <= 6)
        strFormat = "(" + str.substr(0, 3) + ")" + "" + str.substr(3, 3);
      if (str.length > 6)
        strFormat =
          "(" +
          str.substr(0, 3) +
          ")" +
          " " +
          str.substr(3, 3) +
          "-" +
          str.substr(6, 4);
      return strFormat;
    }
  }
  profileDetail:any;
  openModal(row,content, btn) {
    this.modelOptions = {
      backdrop: "static",
      keyboard: false,
      size:'lg',
      centered:true
    };
    this.profileDetail= row;
    btn && btn.parentElement && btn.parentElement.parentElement && btn.parentElement.parentElement.blur();
    this.modalRef = this.modalService.open(content, this.modelOptions);
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  swalImg(image) {
    if(image == '') { 
      
      return
    }
    Swal.fire({
      imageUrl: image,
      imageWidth: 400,  
      imageHeight: 400
    });
  }
  
  openModalDelete(row,content, btn) {
    this.modelOptions = {
      backdrop: "static",
      keyboard: false,
      size:'sm',
      centered:true
    };

    btn && btn.parentElement && btn.parentElement.parentElement && btn.parentElement.parentElement.blur();
    this.modalRef = this.modalService.open(content, this.modelOptions);
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }
  openModalReset(row,content, btn) {
    this.modelOptions = {
      backdrop: "static",
      keyboard: false,
      size:'sm',
      centered:true
    };
    btn && btn.parentElement && btn.parentElement.parentElement && btn.parentElement.parentElement.blur();
    this.modalRef = this.modalService.open(content, this.modelOptions);
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }


  getEmployeeList() {
    this.isLoading = true;
    let  query = "limit="+this.pagelimit+"&offset="+this.pagelimit * this.currentPage ;
    query=query.trim();
  
		this.adminService.geteligibleEmployee(this.assessmentId).subscribe (
		  res => {
			this.isLoading = false;
			if ((res["message"] = "Success")) {
        
       this.managerList = res.data.employeeList;
       this.total = res.data.employeeList.length;
       this.SetPage();
       this.managerList.forEach(element => {
        element['fullDesc'] = element['description']
        
        element['fullTitle'] = element['title'];
        if (element["title"].length > 20) {
         element["title"] = element["title"].substring(0, 15) + "...";
       }
     });

      

       this.managerList.forEach(element => {
        element['assessmentStatus'] = false;
      });


			} else {
			  this.managerList = [];
			}      },
		  err => {
			this.isLoading = false;
		  }
		);
  }
  getText(text) {
    
    Swal.fire({
      title:'Title',
      text:text
    });
  }
  selectAll () {

    if(this.listValue == false) {
       this.managerList.forEach(element => {
     element['assessmentStatus'] = true
    });
    this.isSubmit = true;
    }
    if (this.listValue == true) {
      this.managerList.forEach(element => {
        element['assessmentStatus'] = false
       });
       this.isSubmit = false;
    }
    else {
      
    }
  }


    // END ----- Update user status block
    private getDismissReason(reason: any): string {
      if (reason === ModalDismissReasons.ESC) {
        return "by pressing ESC";
      } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
        return "by clicking on a backdrop";
      } else {
        return `with: ${reason}`;
      }
    }

}
