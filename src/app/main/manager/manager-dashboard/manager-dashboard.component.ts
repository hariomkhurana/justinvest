import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { NgbModalOptions, NgbModalRef, NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ManagerService } from '../manager.service';
import * as moment from 'moment';
import { CommonService } from '../../../shared/common.service';
import Swal from 'sweetalert2';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-manager-dashboard',
  templateUrl: './manager-dashboard.component.html',
  styleUrls: ['./manager-dashboard.component.scss']
})
export class ManagerDashboardComponent implements OnInit {
  total: number = 0;
  totalPending:number = 0;
  pagelimit: number = 5;
  totalPage: number
  currentPage: number = 0;
  list = [1, 2, 3];
  networkList: Array<object> = [];
  dashboardData: any;
  isLoading1:boolean = false;
  modelOptions: NgbModalOptions = {
    backdrop: "static",
    keyboard: false
  };
  closeResult: string;
  private modalRef: NgbModalRef;
  isLoading: boolean = false;
  messageText: string = '';
  assessmentList: any;
  constructor(
    private modalService: NgbModal,
    private ManagerService: ManagerService,
    private _snackBar: MatSnackBar,
    private _common: CommonService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.getNetworkList();
     this.getRequestedAssesment();

  }

  getText(text,title) {
    
    Swal.fire({
      title:title,
      text:text
    });
  }

  getNetworkList() {
    this.isLoading = true;
    let query = ""
    this.ManagerService.networkListApi(query).subscribe(
      res => {
        this.isLoading = false;
        if ((res["message"] = "Success")) {
          this.networkList = res.data.networkList;
          this.networkList.forEach(element => {
            element['humanDate'] = moment.unix(element['insertDate']).format('MMMM Do, YYYY');
          });
        } else {
          this.networkList = [];
        }
      },
      err => {
        this.isLoading = false;
      }
    );
  }

  addMessage() {
    this.isLoading = true;
    let request = {
      message: this.messageText
    }
    this.ManagerService.AddnetworkApi(request).subscribe(
      res => {
        this.isLoading = false;
        this._snackBar.open("Message added successfully!!", "", {
          duration: 3000,
          horizontalPosition: 'right',
          verticalPosition: 'top',
          panelClass: ['failure']
        });
        this.modalRef.close();
        if ((res["message"] = "Success")) {
          this.getNetworkList();
        } else {
        }
      },
      err => {
        this.isLoading = false;
      }
    );
  }
  naviageAssessment(data) {
   
    this.router.navigate(["/main/manager/review-assessment"],{ queryParams: { id: data }});
  }
  SetPage(){
    this.totalPage = this.total/this.pagelimit
    this.totalPage =  Math.ceil(this.totalPage);
  }
  nextPage() {
    this.currentPage = this.currentPage + 1;
    this.getRequestedAssesment();
  }
  prevPage() {
    this.currentPage = this.currentPage - 1;
   this.getRequestedAssesment()
  }

  getRequestedAssesment() {
    let userData = this._common.getUser();
    let query = "status=pending&userType=employee&managerId=" + userData.userId +"&"+"limit="+this.pagelimit +"&offset="+this.pagelimit * this.currentPage;
    this.ManagerService.getEmployeeAssignment(query).subscribe(
      res => {
        this.isLoading = false;
        if ((res["message"] = "Success")) {
          this.assessmentList = res.data.assessmentList;
          this.total = res.data.totalCount;
          this.SetPage();
          this.assessmentList.forEach(element => {
            if(element['track'] == 'clientCare') element['tracklist'] = 'Client Care'
          if(element['track'] == 'patientCare') element['tracklist'] = 'Patient Care'
          if(element['track'] == 'leadership') element['tracklist'] = 'Leadership'
          if(element['track'] == 'hospitalOnboarding') element['tracklist'] = 'Hospital Operations'
          
          if(element['status'] == 'submitted') {
            this.totalPending = this.totalPending + 1;
          }
           });
        
          this.assessmentList.forEach(element => {
            element['remaining'] = 0;
            element['total'] = element['skills'].length;
          });
          this.validatePercentage();


        }
      },
      err => {
        this.isLoading = false;
      }
    );
  }
  validatePercentage() {
    var i;

    for (i = 0; i < this.assessmentList.length; i++) {
      let totalRequest = 0;
      this.assessmentList[i]['skills'].forEach(element => {
        if(element['status'] == 'submitted') {
          totalRequest =  totalRequest +1;
          this.assessmentList[i]['totalRequest'] =  totalRequest;
        }
        if (element['skillLevel'] !== 'mastered') {
          this.assessmentList[i]["remaining"] = this.assessmentList[i]["remaining"] + 1;
        }
          if(this.assessmentList[i])
        this.assessmentList[i]['percentage'] = ((this.assessmentList[i]['remaining'] / this.assessmentList[i]['total']) * 100).toFixed(0);
        this.assessmentList[i]['percentage'] = 100 - parseInt(this.assessmentList[i]['percentage']);
      });


    }


  }
  formatTime(epoc) {
    let time = moment(epoc).fromNow(true)
    return time + ' ago';
  }
  status: string = "";
  reasonText: string = "";
  submitRequest(value) {
    this.status = value;
  }
  hitSubmitApi() {
this.isLoading1 = true;
    let req = {
      userAssessmentId:this.rowData['userAssessmentId'],
        status:this.status
    }
    if (this.reasonText !== '') {
      req['managerNotes'] = this.reasonText;
    }
    this.ManagerService.markedAssessment(req).subscribe(
      res => {
        this.isLoading1= false;
        if ((res["message"] = "Success")) {
          this._snackBar.open("Assessment status updated successfully!!", "", {
            duration: 3000,
            horizontalPosition: 'right',
            verticalPosition: 'top',
            panelClass: ['failure']
          });
          this.getRequestedAssesment();
          this.modalRef.close();
        }
      },
      err => {
        this.isLoading1 = false;
      }
    );

  }

  profileDetail: any;
  openModal(row, content, btn) {
    this.modelOptions = {
      backdrop: "static",
      keyboard: false,
      size: 'md',
    };
    this.isLoading = true;
    let query= "userId=" +row.userId;
    this.profileDetail =row;
    this.ManagerService.getEmployeeProfileApi(query).subscribe (
      res => {
      this.isLoading = false;
      if ((res["message"] = "Success")) {
        this.profileDetail = res["data"]["userList"][0];
        this.modalRef = this.modalService.open(content, this.modelOptions);
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
        this._mobileFormat("");
      } else {
    
      }      },
      err => {
      this.isLoading = false;
      }
    );
    btn && btn.parentElement && btn.parentElement.parentElement && btn.parentElement.parentElement.blur();
    
  }

  _mobileFormat(event: any) {
    var phone = this.profileDetail['mobile']
    phone = phone.replace('(', '').replace(')', '').replace(new RegExp('-', 'g'), '').replace(new RegExp(' ', 'g'), '')
    this.formatMobileNumber(phone)
  }
  mobileNumber:any;
  formatMobileNumber(phone: any) {
    var str = phone.toString();
    var strFormat;
    if (this.mobileNumber !== str) {
      this.mobileNumber = str;
      if (str.length <= 3)
        strFormat = '(' + str.substr(0, 3) + ')'
      if (str.length > 3 && str.length <= 6)
        strFormat = '(' + str.substr(0, 3) + ')' + '' + str.substr(3, 3);
      if (str.length > 6)
        strFormat = '(' + str.substr(0, 3) + ')' + ' ' + str.substr(3, 3) + '-' + str.substr(6, 4);
      this.profileDetail['mobile'] = strFormat
    }
  }
  rowData: any
  openRequestModal(row, content, btn) {
    this.modelOptions = {
      backdrop: "static",
      keyboard: false,
      size: 'sm',
    };
    this.rowData = row;
    btn && btn.parentElement && btn.parentElement.parentElement && btn.parentElement.parentElement.blur();
    this.modalRef = this.modalService.open(content, this.modelOptions);
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  // END ----- Update user status block
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }

}

