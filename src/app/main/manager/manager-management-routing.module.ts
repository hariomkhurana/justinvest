import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ManagerListResourceComponent } from './manager-list-resource/manager-list-resource.component';
import { ManagerDashboardComponent } from './manager-dashboard/manager-dashboard.component';
import { ManagerManagementComponent } from './manager-management/manager-management.component';
import { ManagerEmployeeListComponent } from './manager-employee-list/manager-employee-list.component';
import { ViewProfileComponent } from './view-profile/view-profile.component';
import { AuthGuardMain } from '../../auth/auth-guard.service';
import { ManagerViewAssessmentComponent } from './manager-view-assessment/manager-view-assessment.component';
import { EligableEmployeeComponent } from './eligable-employee/eligable-employee.component';
import { SubmitManagerAssessmentComponent } from './submit-manager-assessment/submit-manager-assessment.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { AllAssessmentListComponent } from './all-assessment-list/all-assessment-list.component';

const routes: Routes = [
  {path:"",component: ManagerManagementComponent,
  canActivate: [AuthGuardMain],
  data: { role: ["manager"] },
  children:[
    { path: '', redirectTo: '/main/manager', pathMatch: 'full' },
    {path:"dashboard",component: ManagerDashboardComponent},
    {path:"list-resource",component:ManagerListResourceComponent},
    {path:"list-employee",component:ManagerEmployeeListComponent},
    {path:"view-profile",component:ViewProfileComponent},
    {path:"list-assessment",component:ManagerViewAssessmentComponent},
    {path:"employee-assessment",component:EligableEmployeeComponent},
    {path:"review-assessment",component:SubmitManagerAssessmentComponent},
    {path:"change-password",component:ChangePasswordComponent},
    {path:"all-assessments",component:AllAssessmentListComponent},
  ]
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManagerManagementRoutingModule { }
