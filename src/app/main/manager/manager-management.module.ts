import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManagerManagementRoutingModule } from './manager-management-routing.module';
import { ManagerManagementComponent } from './manager-management/manager-management.component';
import { ManagerDashboardComponent } from './manager-dashboard/manager-dashboard.component';
import { ManagerEmployeeListComponent } from './manager-employee-list/manager-employee-list.component';
import { ManagerListResourceComponent } from './manager-list-resource/manager-list-resource.component';
import { SharedModule } from '../../shared/shared.module';
import { ManagerService } from './manager.service';
import { ViewProfileComponent } from './view-profile/view-profile.component';
import { ManagerViewAssessmentComponent } from './manager-view-assessment/manager-view-assessment.component';
import { EligableEmployeeComponent } from './eligable-employee/eligable-employee.component';
import { SubmitManagerAssessmentComponent } from './submit-manager-assessment/submit-manager-assessment.component';
import {ProgressBarModule} from "angular-progress-bar"
import {MatExpansionModule} from '@angular/material/expansion';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { AllAssessmentListComponent } from './all-assessment-list/all-assessment-list.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
@NgModule({
  declarations: [ManagerManagementComponent, ManagerDashboardComponent,ManagerEmployeeListComponent,ManagerListResourceComponent, ViewProfileComponent, ManagerViewAssessmentComponent, EligableEmployeeComponent, SubmitManagerAssessmentComponent, ChangePasswordComponent,AllAssessmentListComponent ],
  imports: [
    CommonModule,
    ManagerManagementRoutingModule,
    SharedModule,
    ProgressBarModule,
    MatExpansionModule,
    NgMultiSelectDropDownModule.forRoot(),
    AngularMultiSelectModule,
  ],
  providers:[ManagerService]
})
export class ManagerManagementModule { }
