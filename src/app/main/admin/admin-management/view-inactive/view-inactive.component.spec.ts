import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewInactiveComponent } from './view-inactive.component';

describe('ViewInactiveComponent', () => {
  let component: ViewInactiveComponent;
  let fixture: ComponentFixture<ViewInactiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewInactiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewInactiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
