import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewPrejectedComponent } from './view-prejected.component';

describe('ViewPrejectedComponent', () => {
  let component: ViewPrejectedComponent;
  let fixture: ComponentFixture<ViewPrejectedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewPrejectedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewPrejectedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
