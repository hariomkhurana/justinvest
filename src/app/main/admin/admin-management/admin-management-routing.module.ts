import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AddManagerComponent } from "./add-manager/add-manager.component";
import { AdminDashboardComponent } from "./admin-dashboard/admin-dashboard.component";
import { AdminManagementComponent } from "./admin-management/admin-management.component";
import { ManagerListComponent } from "./manager-list/manager-list.component";
import { EditManagerComponent } from "./edit-manager/edit-manager.component";
import { ViewProfileComponent } from "./view-profile/view-profile.component";
import { AuthGuardMain } from "../../../auth/auth-guard.service";
import { ViewMasterComponent } from "./view-master/view-master.component";
import { SubmitAdminAssessmentComponent } from "./submit-admin-assessment/submit-admin-assessment.component";
import { ViewUserComponent } from "./view-user/view-user.component";
import { ViewCompanyComponent } from "./view-company/view-company.component";
import { AddUserComponent } from "./add-user/add-user.component";
import { EditUserComponent } from "./edit-user/edit-user.component";
import { AddCompanyComponent } from "./add-company/add-company.component";
import { EditCompanyComponent } from "./edit-company/edit-company.component";
import { ViewIndustryComponent } from "./view-industry/view-industry.component";
import { AddIndustryComponent } from "./add-industry/add-industry.component";
import { EditIndustryComponent } from "./edit-industry/edit-industry.component";
import { EditInvestmentComponent } from "./edit-investment/edit-investment.component";
import { AddInvestmentComponent } from "./add-investment/add-investment.component";
import { ViewInvestmentComponent } from "./view-investment/view-investment.component";
import { ViewActiveComponent } from "./view-active/view-active.component";
import { ViewInactiveComponent } from "./view-inactive/view-inactive.component";
import { ViewRejectedComponent } from "./view-rejected/view-rejected.component";
import { ViewPostComponent } from "./view-post/view-post.component";
import { ViewPactiveComponent } from "./view-pactive/view-pactive.component";
import { ViewPinactiveComponent } from "./view-pinactive/view-pinactive.component";
import { ViewPrejectedComponent } from "./view-prejected/view-prejected.component";
import { AddPostComponent } from "./add-post/add-post.component";
import { EditPostComponent } from "./edit-post/edit-post.component";
import { ViewSettingComponent } from "./view-setting/view-setting.component";

const routes: Routes = [
  {
    path: "",
    component: AdminManagementComponent,
    canActivate: [AuthGuardMain],
    data: { role: ["admin"] },
    children: [
      { path: "", redirectTo: "/main/admin", pathMatch: "full" },
      { path: "dashboard", component: AdminDashboardComponent },
      { path: "add-manager", component: AddManagerComponent },
      { path: "all-manager", component: ManagerListComponent },
      { path: "edit-manager", component: EditManagerComponent },
      // {path:"view-candidate",component:ListCandidateComponent},

      { path: "view-profile", component: ViewProfileComponent },

      {
        path: "view-master",
        component: ViewMasterComponent,
        children: [{ path: "", redirectTo: "skill", pathMatch: "full" }]
      },

      { path: "review-assessment", component: SubmitAdminAssessmentComponent }
    ]
  },
  { path: "view-user", component: ViewUserComponent },
  { path: "add-user", component: AddUserComponent },
  { path: "edit-user", component: EditUserComponent },
  //{ path: "view-post", component: ViewPostComponent },
  { path: "add-company", component: AddCompanyComponent },
  { path: "edit-company", component: EditCompanyComponent },
  { path: "view-industry", component: ViewIndustryComponent },
  { path: "add-industry", component: AddIndustryComponent },
  { path: "edit-industry", component: EditIndustryComponent },
  { path: "view-investment", component: ViewInvestmentComponent },
  { path: "edit-investment", component: EditInvestmentComponent },
  { path: "add-investment", component: AddInvestmentComponent },
  { path: "view-setting", component: ViewSettingComponent },
  { path: "add-post", component: AddPostComponent },
  { path: "edit-post", component: EditPostComponent },
  {
    path: "view-company",
    component: ViewCompanyComponent,
    children: [
      { path: "", redirectTo: "active", pathMatch: "full" },
      {
        path: "active", // child route path
        component: ViewActiveComponent // child route component that the router renders
      },
      {
        path: "inactive",
        component: ViewInactiveComponent // another child route component that the router renders
      },
      {
        path: "rejected",
        component: ViewRejectedComponent // another child route component that the router renders
      }
    ]
  },

  {
    path: "view-post",
    component: ViewPostComponent,
    children: [
      { path: "", redirectTo: "active", pathMatch: "full" },
      {
        path: "active", // child route path
        component: ViewPactiveComponent // child route component that the router renders
      },
      {
        path: "inactive",
        component: ViewPinactiveComponent // another child route component that the router renders
      },
      {
        path: "rejected",
        component: ViewPrejectedComponent // another child route component that the router renders
      }
    ]
  }
];

// const routes: Routes = [
//   {path:"",component:UserManagementComponent,
// children:[
//   { path: '', redirectTo: '/main/user-management/view-user', pathMatch: 'full' },
//   {path:"view-user",component:ViewUserComponent},
//   {path:"table",component:ViewReportedComponent},
//   {path:"admin-dashboard",component:AdminDashboardComponent},
//   {path:"add-manager",component:AddUserComponent}
// ]
// }
// ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminManagementRoutingModule {}
