import { Injectable } from "@angular/core";
import { NetworkService } from "../../../shared/network.service";

@Injectable({
  providedIn: "root"
})
export class AdminService {
  constructor(private networkservice: NetworkService) {}

  //  MANAGER  //
  getManagerListApi(query: any) {
    return this.networkservice.get("api/user/userList?role=manager&" + query, null, null, "bearer");
  }
  getResourceApiBySkill(skillId: any) {
    return this.networkservice.get("api/skill/resource/" + skillId, null, null, "bearer");
  }
  getAllManagerListApi(query: any) {
    return this.networkservice.get("api/user/userList?role=manager&limit=1000&" + query, null, null, "bearer");
  }
  getEmployeListApi(query: any) {
    return this.networkservice.get("api/user/userList?role=employee&" + query, null, null, "bearer");
  }

  getUserListApi(query) {
    return this.networkservice.get("api/users/userList?" + query, null, null, "bearer");
  }

  postUserApi(body: any) {
    return this.networkservice.post("api/users", body, null, "bearer");
  }

  updateUserStatus(user: any) {
    return this.networkservice.put("api/users", user, null, "bearer");
  }

  getCompanyListApi(query) {
    return this.networkservice.get("api/company/companyList?status=active&" + query, null, null, "bearer");
  }

  getPendingListApi(query) {
    return this.networkservice.get("api/company/companyList?status=pending&" + query, null, null, "bearer");
  }

  getRejectedListApi(query) {
    return this.networkservice.get("api/company/companyList?status=rejected&" + query, null, null, "bearer");
  }

  postCompanyApi(body: any) {
    return this.networkservice.post("api/company", body, null, "bearer");
  }

  updateCompanyStatus(user: any) {
    return this.networkservice.put("api/company", user, null, "bearer");
  }

  getIndustryListApi(query) {
    return this.networkservice.get("api/industryTypes?" + query, null, null, "bearer");
  }

  postIndustryApi(body: any) {
    return this.networkservice.post("api/industryTypes", body, null, "bearer");
  }

  updateIndustryStatus(user: any) {
    return this.networkservice.put("api/industryTypes", user, null, "bearer");
  }

  deleteIndustryApi(industry: any) {
    return this.networkservice.delete("api/industryTypes/" + industry, null, null, "bearer");
  }

  getInvestmentListApi(query) {
    return this.networkservice.get("api/investmentTypes?" + query, null, null, "bearer");
  }

  postInvestmentApi(body: any) {
    return this.networkservice.post("api/investmentTypes", body, null, "bearer");
  }

  updateInvestmentStatus(user: any) {
    return this.networkservice.put("api/investmentTypes", user, null, "bearer");
  }

  deleteInvestmentApi(investment: any) {
    return this.networkservice.delete("api/investmentTypes/" + investment, null, null, "bearer");
  }

  getActivePostListApi(query) {
    return this.networkservice.get("api/posts?status=approved&" + query, null, null, "bearer");
  }

  getPendingPostListApi(query) {
    return this.networkservice.get("api/posts?status=pending&" + query, null, null, "bearer");
  }

  getRejectedPostListApi(query) {
    return this.networkservice.get("api/posts?status=rejected&" + query, null, null, "bearer");
  }

  deletePostApi(post: any) {
    return this.networkservice.delete("api/posts/" + post, null, null, "bearer");
  }

  updatePostStatus(user: any) {
    return this.networkservice.put("api/posts", user, null, "bearer");
  }

  getCandidateListApi(query: any) {
    return this.networkservice.get("api/user/userList?role=candidate&" + query, null, null, "bearer");
  }
  postManagerApi(body: any) {
    return this.networkservice.post("api/user?", body, null, "bearer");
  }
  addResourceApi(body: any) {
    return this.networkservice.post("api/resource?", body, null, "bearer");
  }
  updateResourceApi(body: any) {
    return this.networkservice.put("api/resource?", body, null, "bearer");
  }
  getResourceApi(query: any) {
    return this.networkservice.get("api/resource?" + query, null, null, "bearer");
  }
  putManagerApi(body: any) {
    return this.networkservice.put("api/user?", body, null, "bearer");
  }
  verifyEmail(body: any) {
    return this.networkservice.post("api/user/verify", body, null, "bearer");
  }
  postimageApi(body: any) {
    return this.networkservice.post("api/webview", body, null, "bearer");
  }

  getDocumentApi() {
    return this.networkservice.get("api/webview", null, null, "bearer");
  }

  // MANAGER END //
  uploadImage(image: any) {
    const formData = new FormData();

    formData.append("image", image, "image.jpg");
    return this.networkservice.uploadImages("api/s3upload/image-upload", formData, null, "bearer");
  }
  uploadPdf(image: any) {
    const formData = new FormData();
    formData.append("image", image);
    return this.networkservice.uploadImages("api/s3upload/image-upload", formData, null, "bearer");
  }

  departmentLIst(query: any) {
    return this.networkservice.get("api/general/department" + query, null, null, "bearer");
  }
  facility(query: any) {
    return this.networkservice.get("api/general/facility" + query, null, null, "bearer");
  }
  category(query: any) {
    return this.networkservice.get("api/general/category" + query, null, null, "bearer");
  }

  deleteUser(userId) {
    return this.networkservice.delete("api/user/delete/" + userId, null, null, "bearer");
  }
  deleteUserassessmentApi(userId) {
    return this.networkservice.delete("api/userassessment/" + userId, null, null, "bearer");
  }
  deleteResource(userId) {
    return this.networkservice.delete("api/resource/" + userId, null, null, "bearer");
  }
  networkListApi(query: any) {
    return this.networkservice.get("api/network" + query, null, null, "bearer");
  }
  AddnetworkApi(body: any) {
    return this.networkservice.post("api/network", body, null, "bearer");
  }
  deleteMessageApi(body: any) {
    return this.networkservice.delete("api/network/" + body, null, null, "bearer");
  }
  adminDashboard() {
    return this.networkservice.get("api/dashboard", null, null, "bearer");
  }

  postAssessmentApi(body: any) {
    return this.networkservice.post("api/assessment", body, null, "bearer");
  }
  getAssessmentListApi(query: any) {
    return this.networkservice.get("api/assessment?" + query, null, null, "bearer");
  }
  getcandidateListApi(query: any) {
    return this.networkservice.get("api/user/userList?role=candidate&" + query, null, null, "bearer");
  }

  getCandidateAssessmentApi(query: any) {
    return this.networkservice.get("api/userassessment/candidate/list?" + query, null, null, "bearer");
  }

  assignAssemtCandidate(body: any) {
    return this.networkservice.post("api/userassessment/candidate/assign", body, null, "bearer");
  }

  putAssessmentApi(body: any) {
    return this.networkservice.put("api/assessment", body, null, "bearer");
  }
  deleteAssessmentApi(assessmentId: any) {
    return this.networkservice.delete("api/assessment/" + assessmentId, null, null, "bearer");
  }
  getSkillList() {
    return this.networkservice.get("api/skill", null, null, "bearer");
  }
  getSkill() {
    return this.networkservice.get("api/skill", null, null, "bearer");
  }

  addSkill(body: any) {
    return this.networkservice.post("api/skill", body, null, "bearer");
  }
  updateskill(body: any) {
    return this.networkservice.put("api/skill", body, null, "bearer");
  }
  deleteskill(id) {
    return this.networkservice.delete("api/skill/" + id, null, null, "bearer");
  }
  addCategory(body: any) {
    return this.networkservice.post("api/general/category", body, null, "bearer");
  }
  updateCateogry(body: any) {
    return this.networkservice.put("api/general/category", body, null, "bearer");
  }
  deleteCategory(id) {
    return this.networkservice.delete("api/general/category/" + id, null, null, "bearer");
  }

  addDepartment(body: any) {
    return this.networkservice.post("api/general/department", body, null, "bearer");
  }
  updateDepartment(body: any) {
    return this.networkservice.put("api/general/department", body, null, "bearer");
  }
  deleteDepartment(id) {
    return this.networkservice.delete("api/general/department/" + id, null, null, "bearer");
  }

  addFacility(body: any) {
    return this.networkservice.post("api/general/facility", body, null, "bearer");
  }
  updateFacility(body: any) {
    return this.networkservice.put("api/general/facility", body, null, "bearer");
  }
  deleteFacilty(id) {
    return this.networkservice.delete("api/general/facility/" + id, null, null, "bearer");
  }
  assignEmployee(body: any) {
    return this.networkservice.post("api/userassessment/assign", body, null, "bearer");
  }
  getEmployeeAssignment(query: any) {
    return this.networkservice.get("api/userassessment/list?" + query, null, null, "bearer");
  }
  markedAssessment(body: any) {
    return this.networkservice.post("api/userassessment/marked", body, null, "bearer");
  }
  markSkill(body: any) {
    return this.networkservice.post("api/userassessment/skill/mark", body, null, "bearer");
  }
  getskillHistory(skillId: any) {
    return this.networkservice.get("api/userassessment/skill/audit/" + skillId, null, null, "bearer");
  }
  getEmployeeProfileApi(query: any) {
    return this.networkservice.get("api/user/userList?" + query, null, null, "bearer");
  }
  resetPassword(body: any) {
    return this.networkservice.post("api/user/resetPassword", body, null, "bearer");
  }

  getTrackAndLevelApi() {
    return this.networkservice.post("api/api/assessmentTrack", null, null, "bearer");
  }
}
