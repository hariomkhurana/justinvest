import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { AdminManagementRoutingModule } from "./admin-management-routing.module";
import { AdminManagementComponent } from "./admin-management/admin-management.component";
import { AdminDashboardComponent } from "./admin-dashboard/admin-dashboard.component";
import { AddManagerComponent } from "./add-manager/add-manager.component";
import { ManagerListComponent } from "./manager-list/manager-list.component";
import { EditManagerComponent } from "./edit-manager/edit-manager.component";
import { SharedModule } from "../../../shared/shared.module";
import { AdminService } from "./admin.service";
import { NgMultiSelectDropDownModule } from "ng-multiselect-dropdown";
import { AngularMultiSelectModule } from "angular2-multiselect-dropdown";
import { ViewMasterComponent } from "./view-master/view-master.component";
import { MatExpansionModule } from "@angular/material/expansion";
import { ManagerService } from "../../manager/manager.service";
import { ViewProfileComponent } from "./view-profile/view-profile.component";
import { EmployeeService } from "../../employees/employee.service";
import { ProgressBarModule } from "angular-progress-bar";
import { SubmitAdminAssessmentComponent } from "./submit-admin-assessment/submit-admin-assessment.component";
import { MatAutocompleteModule } from "@angular/material/autocomplete";
import { Ng2SearchPipeModule } from "ng2-search-filter";
import { ViewUserComponent } from "./view-user/view-user.component";
import { ViewCompanyComponent } from "./view-company/view-company.component";
import { AddUserComponent } from "./add-user/add-user.component";
import { EditUserComponent } from "./edit-user/edit-user.component";
import { AddCompanyComponent } from "./add-company/add-company.component";
import { EditCompanyComponent } from "./edit-company/edit-company.component";
import { ViewIndustryComponent } from "./view-industry/view-industry.component";
import { EditIndustryComponent } from "./edit-industry/edit-industry.component";
import { AddIndustryComponent } from "./add-industry/add-industry.component";
import { AddInvestmentComponent } from "./add-investment/add-investment.component";
import { EditInvestmentComponent } from "./edit-investment/edit-investment.component";
import { ViewInvestmentComponent } from "./view-investment/view-investment.component";
import { ViewActiveComponent } from "./view-active/view-active.component";
import { ViewInactiveComponent } from "./view-inactive/view-inactive.component";
import { ViewRejectedComponent } from "./view-rejected/view-rejected.component";
import { ViewPostComponent } from "./view-post/view-post.component";
import { ViewPactiveComponent } from "./view-pactive/view-pactive.component";
import { ViewPinactiveComponent } from "./view-pinactive/view-pinactive.component";
import { ViewPrejectedComponent } from "./view-prejected/view-prejected.component";
import { AddPostComponent } from "./add-post/add-post.component";
import { EditPostComponent } from "./edit-post/edit-post.component";
import { ViewSettingComponent } from "./view-setting/view-setting.component";
import { ViewTermsComponent } from "./view-terms/view-terms.component";
import { ViewPrivacyComponent } from "./view-privacy/view-privacy.component";

@NgModule({
  declarations: [
    AdminManagementComponent,
    AdminDashboardComponent,
    AddManagerComponent,
    ManagerListComponent,
    EditManagerComponent,
    ViewProfileComponent,
    ViewMasterComponent,
    SubmitAdminAssessmentComponent,
    ViewProfileComponent,
    ViewUserComponent,
    ViewCompanyComponent,
    ViewIndustryComponent,
    EditIndustryComponent,
    AddIndustryComponent,
    AddUserComponent,
    EditUserComponent,
    AddCompanyComponent,
    EditCompanyComponent,
    AddInvestmentComponent,
    EditInvestmentComponent,
    ViewInvestmentComponent,
    ViewActiveComponent,
    ViewInactiveComponent,
    ViewRejectedComponent,
    ViewPostComponent,
    ViewPactiveComponent,
    ViewPinactiveComponent,
    ViewPrejectedComponent,
    AddPostComponent,
    EditPostComponent,
    ViewSettingComponent,
    ViewTermsComponent,
    ViewPrivacyComponent
  ],
  imports: [
    CommonModule,
    MatAutocompleteModule,
    AdminManagementRoutingModule,
    SharedModule,
    NgMultiSelectDropDownModule.forRoot(),
    AngularMultiSelectModule,
    MatExpansionModule,
    ProgressBarModule,
    Ng2SearchPipeModule
  ],
  providers: [AdminService, ManagerService, EmployeeService]
})
export class AdminManagementModule {}
