import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewPinactiveComponent } from './view-pinactive.component';

describe('ViewPinactiveComponent', () => {
  let component: ViewPinactiveComponent;
  let fixture: ComponentFixture<ViewPinactiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewPinactiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewPinactiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
