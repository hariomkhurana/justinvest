import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { MatSnackBar } from "@angular/material";
import { NgbModalOptions, NgbModalRef, NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { AdminService } from "../admin.service";
import * as moment from "moment";
import { Router } from "@angular/router";
import { StreamService } from "../../../../shared/stream.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
@Component({
  selector: "app-admin-dashboard",
  templateUrl: "./admin-dashboard.component.html",
  styleUrls: ["./admin-dashboard.component.scss"]
})
export class AdminDashboardComponent implements OnInit {
  @ViewChild("scrollMe", null) private myScrollContainer: ElementRef;

  total: number = 139;
  pagelimit: number = 5;
  isLoading1: boolean = false;
  totalPage: number;
  currentPage: number = 0;
  list = [1, 2, 3];
  candidateList: Array<object> = [];
  networkList: Array<object> = [];
  dashboardData: any;
  modelOptions: NgbModalOptions = {
    backdrop: "static",
    keyboard: false
  };
  closeResult: string;
  private modalRef: NgbModalRef;
  isLoading: boolean = false;
  messageText: string = "";
  modalForm: FormGroup;
  assessmentList: any;
  constructor(
    private modalService: NgbModal,
    private adminService: AdminService,
    private _snackBar: MatSnackBar,
    private router: Router,
    private streamService: StreamService,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.getNetworkList();
    this.dashboardCount();
    // this.getRequestedAssesment();
  }

  naviageAssessment(data) {
    this.router.navigate(["/main/admin/review-assessment"], { queryParams: { id: data } });
  }
  getRequestedAssesment() {
    let query =
      "status=pending&userType=employee&limit=" + this.pagelimit + "&offset=" + this.pagelimit * this.currentPage;

    this.adminService.getEmployeeAssignment(query).subscribe(
      res => {
        this.isLoading = false;
        if ((res["message"] = "Success")) {
          this.assessmentList = res.data.assessmentList;
          this.assessmentList.forEach(element => {
            if (element["track"] == "clientCare") element["tracklist"] = "Client Care";
            if (element["track"] == "patientCare") element["tracklist"] = "Patient Care";
            if (element["track"] == "leadership") element["tracklist"] = "Leadership";
            if (element["track"] == "hospitalOnboarding") element["tracklist"] = "Hospital Operations";
          });

          this.assessmentList.forEach(element => {
            element["titleLength"] = element["title"].length;
            element["titleOriginal"] = element["title"];

            if (element["titleLength"] > 20) {
              element["title"] = element["title"].substring(0, 15) + "...";
            }
          });

          this.total = res.data.totalCount;
          this.SetPage();
          this.assessmentList.forEach(element => {
            element["remaining"] = 0;
            element["total"] = element["skills"].length;
          });
          this.validatePercentage();
        }
      },
      err => {
        this.isLoading = false;
      }
    );
  }
  validatePercentage() {
    var i;

    for (i = 0; i < this.assessmentList.length; i++) {
      let totalRequest = 0;
      this.assessmentList[i]["skills"].forEach(element => {
        if (element["status"] == "submitted") {
          totalRequest = totalRequest + 1;
          this.assessmentList[i]["totalRequest"] = totalRequest;
        }
        if (element["skillLevel"] !== "mastered") {
          this.assessmentList[i]["remaining"] = this.assessmentList[i]["remaining"] + 1;
        }
        this.assessmentList[i]["percentage"] = (
          (this.assessmentList[i]["remaining"] / this.assessmentList[i]["total"]) *
          100
        ).toFixed(0);
        this.assessmentList[i]["percentage"] = 100 - parseInt(this.assessmentList[i]["percentage"]);
      });
    }
  }
  formatTime(epoc) {
    let time = moment(epoc).fromNow(true);
    return time + " ago";
  }
  initialForm() {
    this.modalForm = this.fb.group({
      message: ["", [Validators.required]]
      // type: ["", [Validators.required]],
    });
  }
  scrollToTop(): void {
    this.myScrollContainer.nativeElement.scrollTo(0, 0);
    this.myScrollContainer.nativeElement.scrollIntoView({ behavior: "smooth" });
  }
  onScroll() {}
  getNetworkList() {
    this.isLoading = true;
    let query = "";
    this.adminService.networkListApi(query).subscribe(
      res => {
        this.isLoading = false;
        if ((res["message"] = "Success")) {
          this.networkList = res.data.networkList;
          this.networkList.forEach(element => {
            element["humanDate"] = moment.unix(element["insertDate"]).format("MMMM Do, YYYY");
          });
          this.myScrollContainer.nativeElement.scrollTo(0, 0);
        } else {
          this.networkList = [];
        }
      },
      err => {
        this.isLoading = false;
      }
    );
  }

  dashboardCount() {
    this.isLoading = true;
    let query = "";
    this.adminService.adminDashboard().subscribe(
      res => {
        this.isLoading = false;
        if ((res["message"] = "Success")) {
          this.dashboardData = res.data;
        } else {
          this.networkList = [];
        }
      },
      err => {
        this.isLoading = false;
      }
    );
  }

  addMessage() {
    this.isLoading1 = true;
    let request = this.modalForm.value;
    this.adminService.AddnetworkApi(request).subscribe(
      res => {
        this.isLoading1 = false;
        this.messageText = "";
        window.scroll(0, 0);
        this._snackBar.open("Message added successfully!!", "", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "top",
          panelClass: ["failure"]
        });
        this.modalRef.close();
        if ((res["message"] = "Success")) {
          this.getNetworkList();
        } else {
        }
      },
      err => {
        this.isLoading1 = false;
        this.messageText = "";
      }
    );
  }

  deleteMessage() {
    this.isLoading1 = true;

    this.adminService.deleteMessageApi(this.networkId).subscribe(
      res => {
        this.isLoading1 = false;
        this.messageText = "";
        this._snackBar.open("Message deleted successfully!!", "", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "top",
          panelClass: ["success"]
        });
        this.modalRef.close();

        this.getNetworkList();
      },
      err => {
        this.isLoading1 = false;
        this.messageText = "";
      }
    );
  }
  networkId: any;
  deleteModalOpen(row, content, btn) {
    this.modelOptions = {
      backdrop: "static",
      keyboard: false,
      centered: true,
      size: "sm"
    };
    this.networkId = row.networkId;
    btn && btn.parentElement && btn.parentElement.parentElement && btn.parentElement.parentElement.blur();
    this.modalRef = this.modalService.open(content, this.modelOptions);
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  naviget(value) {
    if (value == "P") {
      localStorage.setItem("activeIndex", "5");
    }
    if (value == "U") {
      localStorage.setItem("activeIndex", "1");
    }
    if (value == "C") {
      localStorage.setItem("activeIndex", "2");
    }
    if (value == "I") {
      localStorage.setItem("activeIndex", "3");
    }
    if (value == "J") {
      localStorage.setItem("activeIndex", "4");
    }
  }

  Naviagte() {
    this.router.navigate(["main/admin/add-candidate"]);
    localStorage.setItem("activeIndex", "5");
    this.sendRouteVal(5);
  }
  NaviagteAssessment() {
    this.router.navigate(["main/admin/assessment-detail"]);
  }
  textChange() {
    this.messageText = this.messageText.trim();
  }
  sendRouteVal(index) {
    this.streamService.streamMessage(index);
    // this.messageEvent.emit(index);
  }
  SetPage() {
    this.totalPage = this.total / this.pagelimit;
    this.totalPage = Math.ceil(this.totalPage);
    if (this.totalPage == 1 && this.total <= 10) {
      //  this.totalPage = 0;
    }
  }
  nextPage() {
    this.currentPage = this.currentPage + 1;
    this.getRequestedAssesment();
  }
  prevPage() {
    this.currentPage = this.currentPage - 1;
    this.getRequestedAssesment();
  }

  openModal(row, content, btn) {
    this.initialForm();
    this.modelOptions = {
      backdrop: "static",
      keyboard: false,
      size: "md"
    };
    btn && btn.parentElement && btn.parentElement.parentElement && btn.parentElement.parentElement.blur();
    this.modalRef = this.modalService.open(content, this.modelOptions);
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }
  profileDetail: any;
  openModal1(row, content, btn) {
    this.modelOptions = {
      backdrop: "static",
      keyboard: false,
      size: "md"
    };
    this.isLoading = true;
    let query = "userId=" + row.userId;
    this.profileDetail = row;
    this.adminService.getEmployeeProfileApi(query).subscribe(
      res => {
        this.isLoading = false;
        if ((res["message"] = "Success")) {
          this.profileDetail = res["data"]["userList"][0];
          this.modalRef = this.modalService.open(content, this.modelOptions);
          this.modalRef.result.then(
            result => {
              this.closeResult = `Closed with: ${result}`;
            },
            reason => {
              this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
            }
          );
          this._mobileFormat("");
        } else {
        }
      },
      err => {
        this.isLoading = false;
      }
    );
    btn && btn.parentElement && btn.parentElement.parentElement && btn.parentElement.parentElement.blur();
  }
  _mobileFormat(event: any) {
    var phone = this.profileDetail["mobile"];
    phone = phone
      .replace("(", "")
      .replace(")", "")
      .replace(new RegExp("-", "g"), "")
      .replace(new RegExp(" ", "g"), "");
    this.formatMobileNumber(phone);
  }
  mobileNumber: any;
  formatMobileNumber(phone: any) {
    var str = phone.toString();
    var strFormat;
    if (this.mobileNumber !== str) {
      this.mobileNumber = str;
      if (str.length <= 3) strFormat = "(" + str.substr(0, 3) + ")";
      if (str.length > 3 && str.length <= 6) strFormat = "(" + str.substr(0, 3) + ")" + "" + str.substr(3, 3);
      if (str.length > 6) strFormat = "(" + str.substr(0, 3) + ")" + " " + str.substr(3, 3) + "-" + str.substr(6, 4);
      this.profileDetail["mobile"] = strFormat;
    }
  }

  getCandidateList(query) {
    let queryParam = query.trim();
    this.isLoading = true;
    this.adminService.getCandidateListApi(queryParam).subscribe(
      res => {
        this.isLoading = false;
        if ((res["message"] = "Success")) {
          this.candidateList = res.data.userList;
          this.total = res.data.totalUsers;
          this.SetPage();
        } else {
          this.candidateList = [];
        }
      },
      err => {
        this.isLoading = false;
      }
    );
  }

  // END ----- Update user status block
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }
}
