import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewPactiveComponent } from './view-pactive.component';

describe('ViewPactiveComponent', () => {
  let component: ViewPactiveComponent;
  let fixture: ComponentFixture<ViewPactiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewPactiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewPactiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
