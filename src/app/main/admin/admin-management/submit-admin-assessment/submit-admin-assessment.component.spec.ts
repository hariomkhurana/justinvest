import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubmitAdminAssessmentComponent } from './submit-admin-assessment.component';

describe('SubmitAdminAssessmentComponent', () => {
  let component: SubmitAdminAssessmentComponent;
  let fixture: ComponentFixture<SubmitAdminAssessmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubmitAdminAssessmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmitAdminAssessmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
