import { Component, OnInit, Output } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import Swal from "sweetalert2";
import { ImageCroppedEvent } from "ngx-image-cropper";
import { EventEmitter } from "events";
import { MatSnackBar } from "@angular/material";
import { AdminService } from "../admin.service";
import { Router } from "@angular/router";
@Component({
  selector: "app-add-investment",
  templateUrl: "./add-investment.component.html",
  styleUrls: ["./add-investment.component.scss"]
})
export class AddInvestmentComponent implements OnInit {
  @Output() imageUploaded = new EventEmitter();
  @Output() imageError = new EventEmitter();
  @Output() imageLoadedToContainer = new EventEmitter();
  @Output() croppingCanceled = new EventEmitter();
  managerForm: FormGroup;
  profilePic: string;
  isVerified: boolean;
  chooseVal: string = "Choose ";
  isLoading: boolean = false;
  isUploading: boolean = true;
  departmentList: Array<object> = [{ name: "department 1" }, { name: "department 2" }, { name: "department 3" }];
  facelityList: Array<object> = [{ name: "facility 1" }, { name: "facility 2" }, { name: "facility 3" }];
  emailPattern = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)+$/;
  constructor(
    private fb: FormBuilder,
    private adminService: AdminService,
    private _snackBar: MatSnackBar,
    private router: Router
  ) {}

  ngOnInit() {
    this.initialiseForms();
    this.getDepartment();
    this.getFacility();
  }
  initialiseForms() {
    this.managerForm = this.fb.group({
      investmentType: ["", [Validators.required]]
    });
  }

  getDepartment() {
    this.isLoading = true;
    let query = "";
    this.adminService.departmentLIst(query).subscribe(
      res => {
        this.isLoading = false;
        if ((res["message"] = "Success")) {
          this.departmentList = res.data.departmentList;
        } else {
          this.departmentList = [];
        }
      },
      err => {
        this.isLoading = false;
      }
    );
  }
  getFacility() {
    this.isLoading = true;
    let query = "";
    this.adminService.facility(query).subscribe(
      res => {
        this.isLoading = false;
        if ((res["message"] = "Success")) {
          this.facelityList = res.data.facilityList;
        } else {
          this.facelityList = [];
        }
      },
      err => {
        this.isLoading = false;
      }
    );
  }

  imageChangedEvent: any = "";
  imageChangedEvent1: any = "";
  croppedImage: any = "";
  fileChangeEvent(event: any): void {
    this.isLoading = true;
    let file, img;
    if ((file = event.target.files[0]) && (file.type === "image/png" || file.type === "image/jpeg")) {
      img = new Image();
      this.imageChangedEvent = event;
    } else {
      Swal.fire({
        icon: "error",
        html: "Unsupported File Type. Only jpeg and png is allowed!"
      });
    }
    this.isLoading = false;
  }

  b64toBlob(dataURI) {
    var byteString = atob(dataURI.split(",")[1]);
    var ab = new ArrayBuffer(byteString.length);
    var ia = new Uint8Array(ab);
    for (var i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ab], { type: "image/jpeg" });
  }

  save() {
    const blob = this.b64toBlob(this.croppedImage);
    this.imageChangedEvent = null;
    this.isLoading = true;
    this.adminService.uploadImage(blob).subscribe(res => {
      this.isLoading = false;

      if (res["message"] === "Success") {
        this.profilePic = res["data"].imageUrl;
        this.managerForm.controls["profilePic"].setValue(this.profilePic);
        //  console.log('Image url',this.profilePic)
      }
    });
  }
  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
    // console.log(this.croppedImage,'CropImage')
  }

  imageLoaded() {
    // show cropper
  }
  cropperReady() {
    // cropper ready
  }

  validateEmail() {
    if (this.managerForm.value.email == null || this.managerForm.value.email == "") {
      return null;
    }
    let request = {
      email: this.managerForm.value.email
    };
    this.adminService.verifyEmail(request).subscribe(
      res => {
        this.isLoading = false;
        if (res["message"] == "Success") {
          this.isVerified = true;
        }
      },
      err => {
        this.isLoading = false;
        this.isVerified = false;
      }
    );
  }
  generatePassword() {
    var length = 8,
      charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
      retVal = "";
    for (var i = 0, n = charset.length; i < length; ++i) {
      retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    this.managerForm.controls["password"].setValue(retVal);
  }

  submitForm() {
    let req = this.managerForm.value;
    //req["userType"] = "app";
    //req["deviceType"] = "ios";
    //req["deviceToken"] = "abc";
    //req["otpToken"] = "166633";
    this.isLoading = true;
    this.adminService.postInvestmentApi(req).subscribe(
      res => {
        this.isLoading = false;
        if (res["message"] == "Success") {
          this.isLoading = false;
          this._snackBar.open("Investment added successfully!!", "", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "top",
            panelClass: ["failure"]
          });
          this.router.navigate(["/main/admin/view-investment"]);
        }
      },
      err => {
        this.isLoading = false;
      }
    );
  }
  userId: any;

  _mobileFormat(event: any) {
    var phone = this.managerForm.value.mobile_format.toString();
    phone = phone
      .replace("(", "")
      .replace(")", "")
      .replace(new RegExp("-", "g"), "")
      .replace(new RegExp(" ", "g"), "");
    this.formatMobileNumber(phone);
  }
  mobileNumber: any;
  formatMobileNumber(phone: any) {
    var str = phone.toString();
    var strFormat;
    if (this.mobileNumber !== str) {
      this.mobileNumber = str;
      if (str.length <= 3) strFormat = "(" + str.substr(0, 3) + ")";
      if (str.length > 3 && str.length <= 6) strFormat = "(" + str.substr(0, 3) + ")" + "" + str.substr(3, 3);
      if (str.length > 6) strFormat = "(" + str.substr(0, 3) + ")" + " " + str.substr(3, 3) + "-" + str.substr(6, 4);
      this.managerForm.controls["mobile"].setValue(str);
      this.managerForm.controls["mobile_format"].setValue(strFormat);
    }
  }
  onlyNumberKey(event) {
    return event.charCode == 8 || event.charCode == 0 ? null : event.charCode >= 48 && event.charCode <= 57;
  }
  cancel(): void {
    this.croppedImage = null;
    this.imageChangedEvent = null;
    this.isUploading = false;
    setTimeout(() => {
      /** spinner ends after 1 seconds */
      this.isUploading = true;
    }, 1000);
  }
}
