import { Component, OnInit } from "@angular/core";
import { AdminService } from "../admin.service";
import { MatSnackBar } from "@angular/material";
@Component({
  selector: "app-view-setting",
  templateUrl: "./view-setting.component.html",
  styleUrls: ["./view-setting.component.scss"]
})
export class ViewSettingComponent implements OnInit {
  isLoading: boolean = false;
  isUpload: boolean = false;
  constructor(private adminService: AdminService, private _snackBar: MatSnackBar) {}

  ngOnInit() {
    this.getDocument();
  }
  back() {
    this.isUpload = false;
  }
  selectedFile: File;
  attachmentUrl: any = "";
  onFileChanged(event) {
    this.isLoading = true;
    this.selectedFile = event.target.files[0];
    if (this.selectedFile.type !== "application/pdf") {
      this.isLoading = false;
      // this.selectedFile = null;
      this._snackBar.open("Unsupported file type", "", {
        duration: 3000,
        horizontalPosition: "right",
        verticalPosition: "top",
        panelClass: ["success"]
      });
      return;
    }
    console.log("selected FIle", this.selectedFile);
    this.adminService.uploadImage(this.selectedFile).subscribe(
      res => {
        this.isLoading = false;
        if (res["message"] == "Success") {
          this.attachmentUrl = res.data.url;
          console.log(this.attachmentUrl);
          this.postImage();
        } else {
        }
      },
      err => {
        this.isLoading = false;
      }
    );
  }
  imageUploadedUrl: any;
  postImage() {
    let finalObj = {};
    finalObj["url"] = this.attachmentUrl;
    finalObj["status"] = this.searchBy;
    finalObj["docType"] = "pdf";
    console.log(finalObj);
    this.isLoading = true;
    this.adminService.postimageApi(finalObj).subscribe(
      res => {
        this.isLoading = false;
        this.searchBy = "none";
        if (res["message"] == "Success") {
          this._snackBar.open("file uploaded successfully", "", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "top",
            panelClass: ["success"]
          });
          finalObj = "";
          this.getDocument();
          this.isUpload = false;
        } else {
        }
      },
      err => {
        this.isLoading = false;
      }
    );
  }
  queryParams() {
    let query = "";

    return query;
  }

  isUploadFn() {
    if (this.isUpload == false) {
      this.isUpload = true;
      console.log(this.isUpload, "1");
    } else {
      this.isUpload = false;
      console.log(this.isUpload, "2");
    }
  }
  searchBy: string = "none";
  getResponse = [];
  termsNcondition: any;
  privacyPolicy: any;
  consultantTermsNConditions: string = "";
  consultantPrivacyPolicy: string = "";
  getDocument() {
    this.isLoading = true;
    this.adminService.getDocumentApi().subscribe(
      res => {
        this.isLoading = false;
        if (res["message"] == "Success") {
          this.getResponse = res.data.webviewList;
          this.getResponse.forEach(element => {
            if (element.status == "termsNConditions") this.termsNcondition = element["url"];
            if (element.status == "privacyPolicy") this.privacyPolicy = element["url"];
            if (element.status == "consultantTermsNConditions") this.consultantTermsNConditions = element["url"];
            if (element.status == "consultantPrivacyPolicy") this.consultantPrivacyPolicy = element["url"];
            console.log(this.termsNcondition, "1");
            console.log(this.privacyPolicy, "2");
            console.log(this.consultantTermsNConditions, "3");
            console.log(this.consultantPrivacyPolicy, "4");
          });
          // console.log("res", this.getResponse);
        } else {
        }
      },
      err => {
        this.isLoading = false;
      }
    );
  }
  download(value) {
    console.log(this.termsNcondition, "termsInit");
    console.log(this.privacyPolicy, "privacy");
    console.log(value);
    if (value == "termsNCondition") {
      console.log(this.termsNcondition, "TERMS");
      window.open(this.termsNcondition, "_blank");
    }
    if (value == "privacyPolicy") {
      console.log(this.privacyPolicy, "privacy");
      window.open(this.privacyPolicy, "_blank");
    }
    if (value == "consultantTermsNConditions") window.open(this.consultantTermsNConditions, "_blank");
    if (value == "consultantPrivacyPolicy") window.open(this.consultantPrivacyPolicy, "_blank");
    // setTimeout(function() {

    // }, 1000);
  }
}
