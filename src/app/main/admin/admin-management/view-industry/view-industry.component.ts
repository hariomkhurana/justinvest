import { Component, OnInit } from "@angular/core";
import { NgbModal, NgbModalRef, NgbModalOptions, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";
//import { NgbModal, NgbModalRef, NgbModalOptions, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import Swal from "sweetalert2";
import { AdminService } from "../admin.service";
import { MatSnackBar } from "@angular/material";

@Component({
  selector: "app-view-industry",
  templateUrl: "./view-industry.component.html",
  styleUrls: ["./view-industry.component.scss"]
})
export class ViewIndustryComponent implements OnInit {
  userDetails;
  rowDetail: any;
  rowImages: any;
  query: string;
  statusFormSubmitted: boolean = false;
  tierFormSubmitted: boolean = false;

  statusForm: FormGroup;
  tierForm: FormGroup;

  // statusList: Array<object> = [
  //   { id: "active", name: "Active" },
  //   { id: "inactive", name: "Inactive" },
  //   { id: "deleted", name: "Delete" }
  // ];

  // tierList: Array<object> = [
  //   { tno: "0", tname: "Tier0" },
  //   { tno: "1", tname: "Tier1" },
  //   { tno: "2", tname: "Tier2" },
  //   { tno: "3", tname: "Tier3" },
  //   { tno: "4", tname: "Tier4" }
  // ];

  // END - Update user variable
  // filters
  searchStartDate: string;
  searchEndDate: string;
  searchUserStatus: string = "all";
  searchBy: string = "all";
  searchText: string;
  searchName: string;

  // filters end

  userDetail = [];
  loadingIndicator: boolean = true;
  reorderable: boolean = true;
  userSearchType: number = 0;
  minDate: any;
  maxDate: any;
  isLoading: boolean = false;
  totalCount: any;
  offset: number = 0;
  pageNumber: number = 0;

  modelOptions: NgbModalOptions = {
    backdrop: "static",
    keyboard: false
  };
  closeResult: string;
  private modalRef: NgbModalRef;

  constructor(
    private adminService: AdminService,
    private modalService: NgbModal,
    private _snackBar: MatSnackBar,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.getUserList();
  }

  queryParams() {
    let query = "limit=" + this.pagelimit + "&offset=" + this.pagelimit * this.currentPage;

    if (this.searchName && this.searchName !== "") {
      query = query + "&industryType=" + this.searchName.toLowerCase().trim() + "&";
    }

    return query;
  }
  // GET USERS (By Default ALL)
  getUserList() {
    // let queryParam = "";
    //  let  query = "limit="+this.pagelimit +"&offset="+this.pagelimit * this.currentPage;
    //let  queryFilter = "limit="+this.pagelimit +"&offset=0"
    var queryValue = this.queryParams();
    //debugger;
    this.isLoading = true;
    this.adminService.getIndustryListApi(queryValue).subscribe(
      res => {
        this.isLoading = false;
        if ((res["message"] = "Success")) {
          this.userDetail = res["data"]["industryTypeList"];
          //this.total = res.data.totalCount;
          this.SetPage();

          console.log(this.userDetail);
        } else {
          this.userDetail = [];
        }
        this.isLoading = false;
      },
      err => {
        this.isLoading = false;
      }
    );
  }

  onReset() {
    this.isLoading = true;
    setTimeout(() => {
      //     /** spinner ends after 5 seconds */
      this.isLoading = false;
    }, 2000);
    (this.searchName = ""), this.getUserList();
  }

  searchFiltersBtn() {
    this.getUserList();
  }

  total: number = 1;
  pagelimit: number = 10;
  totalPage: number;
  currentPage: number = 0;
  SetPage() {
    this.totalPage = 0;
    this.totalPage = this.total / this.pagelimit;
    this.totalPage = Math.ceil(this.totalPage);
    if (this.totalPage == 1 || this.total <= this.pagelimit) {
      this.totalPage = 1;
    }
  }

  nextPage() {
    this.currentPage = this.currentPage + 1;
    this.getUserList();
  }
  prevPage() {
    this.currentPage = this.currentPage - 1;
    this.getUserList();
  }

  removeIndustry(row) {
    this.isLoading = false;
    Swal.fire({
      title: "Are you sure?",
      text: "You want to delete this industry!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!"
    }).then(result => {
      if (result.isConfirmed == true) {
        this.adminService.deleteIndustryApi(row).subscribe(
          res => {
            this.isLoading = false;
            if (res["message"] == "success") {
              this._snackBar.open("Industry deleted successfully !", "", {
                duration: 5000,
                horizontalPosition: "right",
                verticalPosition: "top",
                panelClass: ["success"]
              });
              this.getUserList();
            } else {
            }
          },
          err => {
            this.isLoading = false;
          }
        );
      }
    });
  }

  swalImage(image) {
    Swal.fire({
      imageUrl: image,
      imageWidth: 400,
      imageHeight: 400
    });
  }
  //// END ----- Update user status block
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }
}
