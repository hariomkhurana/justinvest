import { Component, OnInit } from "@angular/core";
import { NgbModal, NgbModalRef, NgbModalOptions, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";
//import { NgbModal, NgbModalRef, NgbModalOptions, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import Swal from "sweetalert2";
import { AdminService } from "../admin.service";
import { MatSnackBar } from "@angular/material";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "app-view-rejected",
  templateUrl: "./view-rejected.component.html",
  styleUrls: ["./view-rejected.component.scss"]
})
export class ViewRejectedComponent implements OnInit {
  userDetails;
  companyDetail = [];
  rowDetail: any;
  rowImages: any;
  query: string;
  statusFormSubmitted: boolean = false;
  tierFormSubmitted: boolean = false;

  statusForm: FormGroup;
  tierForm: FormGroup;

  statusList: Array<object> = [
    { id: "active", name: "Approved" }
    // { id: "rejected", name: "Rejected" }
  ];

  tierList: Array<object> = [
    { tno: "0", tname: "Tier0" },
    { tno: "1", tname: "Tier1" },
    { tno: "2", tname: "Tier2" },
    { tno: "3", tname: "Tier3" },
    { tno: "4", tname: "Tier4" }
  ];

  // END - Update user variable
  // filters
  searchStartDate: string;
  searchEndDate: string;
  searchUserStatus: string = "all";
  searchBy: string = "all";
  searchText: string;
  searchName: string;
  searchEmail: string;
  // filters end

  userDetail = [];
  loadingIndicator: boolean = true;
  reorderable: boolean = true;
  userSearchType: number = 0;
  minDate: any;
  maxDate: any;
  isLoading: boolean = false;
  isLoading1: boolean = false;
  totalCount: any;
  pageLimit: number = 10;
  offset: number = 0;
  pageNumber: number = 0;

  modelOptions: NgbModalOptions = {
    backdrop: "static",
    keyboard: false
  };
  closeResult: string;
  private modalRef: NgbModalRef;

  constructor(
    private adminService: AdminService,
    private modalService: NgbModal,
    private _snackBar: MatSnackBar,
    private router: Router,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.getCompanyList();
  }

  queryParams() {
    let query = "limit=" + this.pagelimit + "&offset=" + this.pagelimit * this.currentPage;

    if (this.searchName && this.searchName !== "") {
      query = query + "&companyName=" + this.searchName.toLowerCase().trim() + "&";
    }

    if (this.searchEmail && this.searchEmail !== "") {
      query = query + "&email=" + this.searchEmail.toLowerCase().trim() + "&";
    }

    return query;
  }
  // GET USERS (By Default ALL)
  getCompanyList() {
    let queryParam = "";
    var queryValue = this.queryParams();
    //debugger;
    this.isLoading = true;
    this.adminService.getRejectedListApi(queryValue).subscribe(
      res => {
        this.isLoading = false;
        if ((res["message"] = "Success")) {
          this.companyDetail = res["data"]["companyList"];
          this.total = res.data.totalCount;
          console.log(this.totalCount);
          this.SetPage();

          console.log(this.companyDetail);
        } else {
          this.companyDetail = [];
        }
        this.isLoading = false;
      },
      err => {
        this.isLoading = false;
      }
    );
  }

  total: number = 0;
  pagelimit: number = 10;
  totalPage: number;
  currentPage: number = 0;
  SetPage() {
    this.totalPage = 0;
    this.totalPage = this.total / this.pagelimit;
    this.totalPage = Math.ceil(this.totalPage);
    if (this.totalPage == 1 || this.total <= this.pagelimit) {
      this.totalPage = 1;
    }
    console.log(this.total);
  }

  nextPage() {
    this.currentPage = this.currentPage + 1;
    this.getCompanyList();
  }
  prevPage() {
    this.currentPage = this.currentPage - 1;
    this.getCompanyList();
  }

  onReset() {
    this.isLoading = true;
    setTimeout(() => {
      //     /** spinner ends after 5 seconds */
      this.isLoading = false;
    }, 2000);
    (this.searchName = ""), (this.searchEmail = ""), this.getCompanyList();
  }

  searchFiltersBtn() {
    this.getCompanyList();
  }

  prepareUpdateStatusForm() {
    this.statusForm = this.formBuilder.group({
      status: ["", [Validators.required]],
      reasonForRejection: [""]
    });
  }

  prepareTierStatusForm() {
    this.tierForm = this.formBuilder.group({
      tier: ["", [Validators.required]]
    });
  }

  updateUserStatusModal(row, content, btn) {
    this.modelOptions = {
      backdrop: "static",
      keyboard: false,
      size: "md"
    };
    btn && btn.parentElement && btn.parentElement.parentElement && btn.parentElement.parentElement.blur();

    this.rowDetail = row;
    this.statusFormSubmitted = false;
    this.prepareUpdateStatusForm();
    this.modalRef = this.modalService.open(content, this.modelOptions);
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  updateUserStatus(test) {
    this.statusFormSubmitted = true;
    if (this.statusForm.valid) {
      this.isLoading1 = true;
      let finalObj = {
        companyId: this.rowDetail["companyId"],

        status: this.statusForm.controls.status.value
      };
      if (finalObj["status"] == "rejected") {
        finalObj["reasonForRejection"] = this.statusForm.controls.reasonForRejection.value;
      }
      this.adminService.updateCompanyStatus(finalObj).subscribe(
        res => {
          this.isLoading1 = false;
          if (res["message"] == "Success") {
            this._snackBar.open("Company status updated successfully!", "", {
              duration: 5000,
              horizontalPosition: "right",
              verticalPosition: "top",
              panelClass: ["success"]
            });
            // Swal.fire("Success", "Officer updated successfully!", "success");
            this.getCompanyList();
            this.router.navigate(["main/admin/view-active"]);
            this.modalRef.close();
            this.statusForm.reset();
          } else {
          }
        },
        err => {
          this.isLoading1 = false;
        }
      );
    }
  }

  swalImage(image) {
    Swal.fire({
      imageUrl: image,
      imageWidth: 400,
      imageHeight: 400
    });
  }
  //// END ----- Update user status block
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }
}
