import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewPrivacyComponent } from './view-privacy.component';

describe('ViewPrivacyComponent', () => {
  let component: ViewPrivacyComponent;
  let fixture: ComponentFixture<ViewPrivacyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewPrivacyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewPrivacyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
