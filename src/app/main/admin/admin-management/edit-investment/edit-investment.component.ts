import { Component, OnInit, Output } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import Swal from "sweetalert2";
import { ImageCroppedEvent } from "ngx-image-cropper";
import { EventEmitter } from "events";
import { MatSnackBar } from "@angular/material";
import { AdminService } from "../admin.service";
import { ActivatedRoute, Router } from "@angular/router";
@Component({
  selector: "app-edit-investment",
  templateUrl: "./edit-investment.component.html",
  styleUrls: ["./edit-investment.component.scss"]
})
export class EditInvestmentComponent implements OnInit {
  @Output() imageUploaded = new EventEmitter();
  @Output() imageError = new EventEmitter();
  @Output() imageLoadedToContainer = new EventEmitter();
  @Output() croppingCanceled = new EventEmitter();
  managerForm: FormGroup;
  industryDetail = [];
  profilePic: string;
  isVerified: boolean;
  chooseVal: string = "Choose ";
  isLoading: boolean = false;
  isUploading: boolean = true;
  managerdetail: any;
  investmentId: any;
  emailPattern = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)+$/;
  departmentList: Array<object> = [{ name: "department 1" }, { name: "department 2" }, { name: "department 3" }];
  facelityList: Array<object> = [{ name: "facility 1" }, { name: "facility 2" }, { name: "facility 3" }];

  constructor(
    private fb: FormBuilder,
    private adminService: AdminService,
    private _snackBar: MatSnackBar,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.route.queryParams.subscribe(param => {
      console.log("pramm", param);
      this.investmentId = param["id"];
      this.getUserList();
    });
    this.initialiseForms();
    this.getUserList();
  }

  patchValue(data) {
    this.managerForm.patchValue({
      investmentType: data["investmentType"]
    });
  }

  getUserList() {
    this.isLoading = true;
    let query = "investmentTypeId	=" + this.investmentId;

    this.adminService.getInvestmentListApi(query).subscribe(
      res => {
        this.isLoading = false;
        if ((res["message"] = "Success")) {
          console.log(res);
          this.industryDetail = res["data"]["investmentTypeList"][0];
          console.log(this.industryDetail);

          this.patchValue(this.industryDetail);
        } else {
        }
      },
      err => {
        this.isLoading = false;
      }
    );
  }

  updateUser(form: any) {
    let req = this.managerForm.value;
    req["investmentTypeId"] = this.investmentId;
    this.adminService.updateInvestmentStatus(req).subscribe(
      res => {
        this.isLoading = false;
        if (res["message"] == "Success") {
          this._snackBar.open("Investment updated successfully!", "", {
            duration: 5000,
            horizontalPosition: "right",
            verticalPosition: "top",
            panelClass: ["success"]
          });
          this.getUserList();
          this.router.navigate(["main/admin/view-investment"]);
        } else {
          //Swal.fire("Error", res.data, "error");
        }
        this.isLoading = false;
      },
      err => {
        this.isLoading = false;
      }
    );
  }

  blogdetail: any;

  initialiseForms() {
    this.managerForm = this.fb.group({
      investmentType: ["", [Validators.required]]
    });
  }

  imageChangedEvent: any = "";
  imageChangedEvent1: any = "";
  croppedImage: any = "";

  cancel(): void {
    this.croppedImage = null;
    this.imageChangedEvent = null;
    this.isUploading = false;
    setTimeout(() => {
      /** spinner ends after 1 seconds */
      this.isUploading = true;
    }, 1000);
  }
}
