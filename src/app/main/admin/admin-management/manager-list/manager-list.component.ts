import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { ModalDismissReasons, NgbModal, NgbModalOptions, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AdminService } from '../admin.service';
import Swal from 'sweetalert2'
import { StreamService } from '../../../../shared/stream.service';
@Component({
  selector: 'app-manager-list',
  templateUrl: './manager-list.component.html',
  styleUrls: ['./manager-list.component.scss']
})
export class ManagerListComponent implements OnInit,OnDestroy {
  isLoading:boolean = false;
  isLoading1:boolean = false
  managerList: Array<object> = [];
  message:string = "";
  subscribedPage:any;
  modelOptions: NgbModalOptions = {
    backdrop: "static",
    keyboard: false
  };
  closeResult: string;
  private modalRef: NgbModalRef;
  constructor( 
    private modalService: NgbModal,
    private adminService:AdminService,
    private _snackBar: MatSnackBar,
   private streamService: StreamService
    ) { }

  ngOnInit() {
    this.getManagerList();
     this.subscribedPage =  this.streamService.getFilterSubscription().subscribe((data: string) => {
        this.message = data;
        this.getManagerList();
        })
      }

      ngOnDestroy(): void {

        this.subscribedPage.unsubscribe();

   }

   total:number = 0;
   pagelimit :number = 10;
   totalPage:number
   currentPage:number =0;
  SetPage(){
    this.totalPage = 0
    this.totalPage = this.total/this.pagelimit
    this.totalPage = Math.ceil(this.totalPage);
    if(this.totalPage == 1 || this.total <= this.pagelimit) {
     this.totalPage = 1;
    }
  }
  nextPage(){
    this.currentPage = this.currentPage +1;
    this.getManagerList();
  }
  prevPage(){
    this.currentPage = this.currentPage -1;
    this.getManagerList();
  }
  status:string;
  rowdata:any;
  openModal(row,content, btn,status) {
    this.modelOptions = {
      backdrop: "static",
      keyboard: false,
      size:'sm',
      centered:true
    };
    this.rowdata = row;
    this.UserId = row.userId;
    this.status = status;
    if(this.status == 'active') {
      this.status = 'activate'
    }

    btn && btn.parentElement && btn.parentElement.parentElement && btn.parentElement.parentElement.blur();
    this.modalRef = this.modalService.open(content, this.modelOptions);
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  filterValue() {
  }
  
  openModalDelete(row,content, btn) {
    this.modelOptions = {
      backdrop: "static",
      keyboard: false,
      size:'sm',
      centered:true
    };
    this.UserId= row;
    btn && btn.parentElement && btn.parentElement.parentElement && btn.parentElement.parentElement.blur();
    this.modalRef = this.modalService.open(content, this.modelOptions);
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }
  openModalReset(row,content, btn) {
    this.modelOptions = {
      backdrop: "static",
      keyboard: false,
      size:'sm',
      centered:true
    };
    this.UserId = row;

    btn && btn.parentElement && btn.parentElement.parentElement && btn.parentElement.parentElement.blur();
    this.modalRef = this.modalService.open(content, this.modelOptions);
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }
  suspendAccount(){
    this.isLoading1 = true;
    if(this.status =='suspend') {
      this.status = 'suspended'
    }
  let request ={
    userId:this.UserId,
    status :this.status
  }
  if(this.status == "activate") {
    request['status'] = 'active'
  }
  this.adminService.putManagerApi(request).subscribe((res) => {
    this.isLoading1 = false;
     if(res['message'] =='Success') {
     this.isLoading1 = false;
     this.modalRef.close();
     this._snackBar.open("Status changed successfully!!","",{
      duration: 3000,
      horizontalPosition:'right',
      verticalPosition:'top',
      panelClass: ['failure']
    });
   this.getManagerList();
     } 
   }, err => {
     this.isLoading1 = false;
   });

  }
 resetAccount(){
   this.isLoading1 = true;
   var length = 8,
        charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
        retVal = "";
    for (var i = 0, n = charset.length; i < length; ++i) {
        retVal += charset.charAt(Math.floor(Math.random() * n));
    }
  let request ={
    userId:this.UserId,
    newPassword:retVal
  }
  this.adminService.resetPassword(request).subscribe((res) => {
    this.isLoading1 = false;
     if(res['message'] =='Success') {
     this.isLoading1 = false;
     this.modalRef.close();
     this._snackBar.open("Password reset successfully!!","",{
       duration: 3000,
       horizontalPosition:'right',
       verticalPosition:'top',
       panelClass: ['failure']
     });
     } 
   }, err => {
     this.isLoading1 = false;
   });

 
  this.isLoading1= false;
    
  }
  changePageLimit() {
    this.currentPage = 0;
    this.getManagerList();
  }
  getManagerList() {
    this.isLoading = true;
    let  query = "limit="+this.pagelimit +"&offset="+this.pagelimit * this.currentPage;
    let  queryFilter = "limit="+this.pagelimit +"&offset=0"
    if(this.message !== '') {
      // this.pagelimit = 0;
      let query1 = "&name="+this.message;
      query = queryFilter +query1
      this.currentPage = 0;
    }

    query = query.trim();
		this.adminService.getManagerListApi(query).subscribe (
		  res => {
			this.isLoading = false;
			if ((res["message"] = "Success")) {
      this.message = ""
       this.managerList = res.data.userList;
       this.managerList.forEach(element => {
        if(element['department'][0] == 'other') element['department'] = 'Other'
      
       });
       this.total = res.data.totalUsers;
       this.SetPage();

       this.managerList.forEach(element => {
         element['fullTitle'] = element['title']
         element['fullFacility'] = element['facility']
          element['titleLength'] = element['title'].length; 
        element['facilityLength'] = element['facility'].length; 
        if(element['titleLength'] > 20) {
         element["title"] = element["title"].substring(0,15) + '...';
        }
        if(element['facilityLength'] > 10) {
          element["facility"] = element["facility"].substring(0,8) + '...';
         }
       
        
      });


			} else {
			  this.managerList = [];
			}      },
		  err => {
			this.isLoading = false;
		  }
		);
  }
  UserId:any
  deleteUser() {
    this.isLoading1 = true;
    this.adminService.deleteUser(this.UserId).subscribe((res) => {
      this.isLoading = false;
       if(res['status'] =='Success') {
       this.isLoading1 = false;
       this.modalRef.close();
       this._snackBar.open("Manager deleted successfully!!","",{
        duration: 3000,
        horizontalPosition:'right',
        verticalPosition:'top',
        panelClass: ['failure']
      });
     this.getManagerList();
       } 
     }, err => {
       this.isLoading1 = false;
     });
  }
  getText(text,title) {
    
    Swal.fire({
      title:title,
      text:text
    });
  }

  _mobileFormat(mobile: any) {
    var phone = mobile
    phone = phone.replace('(', '').replace(')', '').replace(new RegExp('-', 'g'), '').replace(new RegExp(' ', 'g'), '')
    var str = phone.toString();
    var strFormat;
    if (this.mobileNumber !== str) {
      this.mobileNumber = str;
      if (str.length <= 3)
        strFormat = '(' + str.substr(0, 3) + ')'
      if (str.length > 3 && str.length <= 6)
        strFormat = '(' + str.substr(0, 3) + ')' + '' + str.substr(3, 3);
      if (str.length > 6)
        strFormat = '(' + str.substr(0, 3) + ')' + ' ' + str.substr(3, 3) + '-' + str.substr(6, 4);
      return strFormat;
    }
  }
  mobileNumber:any;
  formatMobileNumber(phone: any) {
    
  }

    // END ----- Update user status block
    private getDismissReason(reason: any): string {
      if (reason === ModalDismissReasons.ESC) {
        return "by pressing ESC";
      } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
        return "by clicking on a backdrop";
      } else {
        return `with: ${reason}`;
      }
    }

}
