import { Component, OnInit, Output } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import Swal from "sweetalert2";
import { ImageCroppedEvent } from "ngx-image-cropper";
import { EventEmitter } from "events";
import { MatSnackBar } from "@angular/material";
import { AdminService } from "../admin.service";
import { ActivatedRoute, Router } from "@angular/router";
@Component({
  selector: "app-edit-company",
  templateUrl: "./edit-company.component.html",
  styleUrls: ["./edit-company.component.scss"]
})
export class EditCompanyComponent implements OnInit {
  @Output() imageUploaded = new EventEmitter();
  @Output() imageError = new EventEmitter();
  @Output() imageLoadedToContainer = new EventEmitter();
  @Output() croppingCanceled = new EventEmitter();
  managerForm: FormGroup;
  companyDetail = [];
  industryDetail = [];
  profilePic: string;
  isVerified: boolean;
  chooseVal: string = "Choose ";
  isLoading: boolean = false;
  isUploading: boolean = true;
  managerdetail: any;
  userId: any;
  companyId: any;
  emailPattern = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)+$/;
  departmentList: Array<object> = [{ name: "department 1" }, { name: "department 2" }, { name: "department 3" }];
  facelityList: Array<object> = [{ name: "facility 1" }, { name: "facility 2" }, { name: "facility 3" }];
  indId: any;
  industryTypeId: any;

  constructor(
    private fb: FormBuilder,
    private adminService: AdminService,
    private _snackBar: MatSnackBar,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.route.queryParams.subscribe(param => {
      console.log("pramm", param);
      this.companyId = param["id"];
      this.industryTypeId = param["industryid"];
      this.getCompanyList();
    });
    this.initialiseForms();
    this.getCompanyList();
    this.getIndustryList();
  }
  getDepartment() {
    this.isLoading = true;
    let query = "";
    this.adminService.departmentLIst(query).subscribe(
      res => {
        this.isLoading = false;
        if ((res["message"] = "Success")) {
          this.departmentList = res.data.departmentList;
        } else {
          this.departmentList = [];
        }
      },
      err => {
        this.isLoading = false;
      }
    );
  }

  patchValue(data) {
    this.managerForm.patchValue({
      firstName: data["firstName"],
      lastName: data["lastName"],
      email: data["email"],
      address: data["address"],
      revenue: data["revenue"],
      industryTypeId: data["industryType"][0],
      companyName: data["companyName"],
      aboutCompany: data["aboutCompany"]
    });
  }

  getCompanyList() {
    this.isLoading = true;
    //debugger;
    let query = "companyId=" + this.companyId;

    this.adminService.getCompanyListApi(query).subscribe(
      res => {
        this.isLoading = false;
        if ((res["message"] = "Success")) {
          console.log(res);
          this.companyDetail = res["data"]["companyList"][0];
          console.log(this.companyDetail);

          this.patchValue(this.companyDetail);
        } else {
        }
      },
      err => {
        this.isLoading = false;
      }
    );
  }

  updateCompany(form: any) {
    let req = this.managerForm.value;
    req["companyId"] = this.companyId;
    req["industryTypeId"] = this.indId;
    this.adminService.updateCompanyStatus(req).subscribe(
      res => {
        this.isLoading = false;
        if (res["message"] == "Success") {
          this._snackBar.open("Company updated successfully!", "", {
            duration: 5000,
            horizontalPosition: "right",
            verticalPosition: "top",
            panelClass: ["success"]
          });
          this.getCompanyList();
          this.router.navigate(["main/admin/view-company"]);
        } else {
          //Swal.fire("Error", res.data, "error");
        }
        this.isLoading = false;
      },
      err => {
        this.isLoading = false;
      }
    );
  }

  queryParam() {
    let query = "";

    return query;
  }
  // GET USERS (By Default ALL)
  getIndustryList() {
    //debugger;
    let query = "";
    //var queryValue = this.queryParam();
    this.isLoading = true;
    // let query = "industryTypeId=" + this.industryTypeId;
    this.adminService.getIndustryListApi(query).subscribe(
      res => {
        this.isLoading = false;
        if ((res["message"] = "Success")) {
          this.industryDetail = res["data"]["industryTypeList"];

          console.log(this.industryDetail);
        } else {
          this.industryDetail = [];
        }
        this.isLoading = false;
      },
      err => {
        this.isLoading = false;
      }
    );
  }

  getFacility() {
    this.isLoading = true;
    let query = "";
    this.adminService.facility(query).subscribe(
      res => {
        this.isLoading = false;
        if ((res["message"] = "Success")) {
          this.facelityList = res.data.facilityList;
        } else {
          this.departmentList = [];
        }
      },
      err => {
        this.isLoading = false;
      }
    );
  }
  blogdetail: any;
  getManager() {
    this.isLoading = true;
    let query = "userId=" + this.userId;
    this.adminService.getAllManagerListApi(query).subscribe(
      res => {
        this.isLoading = false;
        if ((res["message"] = "Success")) {
          this.managerdetail = res["data"]["userList"][0];
          this.PatchManager();
        } else {
        }
      },
      err => {
        this.isLoading = false;
      }
    );
  }
  PatchManager() {
    this.managerForm.patchValue({
      firstName: this.managerdetail["firstName"],
      lastName: this.managerdetail["lastName"],
      email: this.managerdetail["email"],
      title: this.managerdetail["title"],
      profilePic: this.managerdetail["profilePic"],
      department: this.managerdetail["department"][0],
      salary: this.managerdetail["salary"],
      facility: this.managerdetail["facility"]
    });
    if (this.managerdetail["mobile"] == "") {
    } else {
      this.managerForm.controls.mobile_format.setValue(this.managerdetail["mobile"]);
      this._mobileFormat("TEST");
    }
    this.profilePic = this.managerdetail["profilePic"];
    this.isVerified = true;
  }

  initialiseForms() {
    this.managerForm = this.fb.group({
      firstName: ["", [Validators.required]],
      lastName: ["", [Validators.required]],
      email: ["", [Validators.required, Validators.pattern(this.emailPattern)]],
      address: ["", [Validators.required]],
      revenue: ["", [Validators.required]],
      companyName: ["", [Validators.required]],
      aboutCompany: ["", [Validators.required]],
      industryTypeId: ["", [Validators.required]]
    });
  }

  imageChangedEvent: any = "";
  imageChangedEvent1: any = "";
  croppedImage: any = "";
  fileChangeEvent(event: any): void {
    this.isLoading = true;
    let file, img;
    if ((file = event.target.files[0]) && (file.type === "image/png" || file.type === "image/jpeg")) {
      img = new Image();
      this.imageChangedEvent = event;
    } else {
      Swal.fire({
        icon: "error",
        html: "Unsupported File Type. Only jpeg and png is allowed!"
      });
    }
    this.isLoading = false;
  }

  b64toBlob(dataURI) {
    var byteString = atob(dataURI.split(",")[1]);
    var ab = new ArrayBuffer(byteString.length);
    var ia = new Uint8Array(ab);
    for (var i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ab], { type: "image/jpeg" });
  }

  save() {
    const blob = this.b64toBlob(this.croppedImage);
    this.imageChangedEvent = null;
    this.isLoading = true;
    this.adminService.uploadImage(blob).subscribe(res => {
      this.isLoading = false;

      if (res["message"] === "Success") {
        this.profilePic = res["data"].imageUrl;
        this.managerForm.controls["profilePic"].setValue(this.profilePic);
        //  console.log('Image url',this.profilePic)
      }
    });
  }
  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
    // console.log(this.croppedImage,'CropImage')
  }

  imageLoaded() {
    // show cropper
  }
  cropperReady() {
    // cropper ready
  }

  validateEmail() {
    if (this.managerForm.value.email == null || this.managerForm.value.email == "") {
      return null;
    }
    if (this.managerForm.value.email == this.managerdetail["email"]) {
      return null;
    }
    let request = {
      email: this.managerForm.value.email
    };
    this.adminService.verifyEmail(request).subscribe(
      res => {
        this.isLoading = false;
        if (res["message"] == "Success") {
          this.isVerified = true;
        }
      },
      err => {
        this.isLoading = false;
        this.isVerified = false;
      }
    );
  }
  generatePassword() {
    var length = 8,
      charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
      retVal = "";
    for (var i = 0, n = charset.length; i < length; ++i) {
      retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    this.managerForm.controls["password"].setValue(retVal);
  }

  submitForm() {
    let req = this.managerForm.value;
    if (this.managerForm.value.salary == "" || this.managerForm.value.salary == null) delete req["salary"];
    req["userId"] = this.managerdetail["userId"];
    delete req["mobile_format"];

    let dept = [];
    dept.push(req["department"]);
    req["department"] = dept;
    this.isLoading = true;
    this.adminService.putManagerApi(req).subscribe(
      res => {
        this.isLoading = false;
        if (res["message"] == "Success") {
          this.isLoading = false;
          this._snackBar.open("Manager updated successfully!!", "", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "top",
            panelClass: ["failure"]
          });
          this.router.navigate(["/main/admin/all-manager"]);
        }
      },
      err => {
        this.isLoading = false;
      }
    );
  }
  _mobileFormat(event: any) {
    var phone = this.managerForm.value.mobile_format.toString();
    phone = phone
      .replace("(", "")
      .replace(")", "")
      .replace(new RegExp("-", "g"), "")
      .replace(new RegExp(" ", "g"), "");
    this.formatMobileNumber(phone);
  }
  mobileNumber: any;
  formatMobileNumber(phone: any) {
    var str = phone.toString();
    var strFormat;
    if (this.mobileNumber !== str) {
      this.mobileNumber = str;
      if (str.length <= 3) strFormat = "(" + str.substr(0, 3) + ")";
      if (str.length > 3 && str.length <= 6) strFormat = "(" + str.substr(0, 3) + ")" + "" + str.substr(3, 3);
      if (str.length > 6) strFormat = "(" + str.substr(0, 3) + ")" + " " + str.substr(3, 3) + "-" + str.substr(6, 4);
      this.managerForm.controls["mobile"].setValue(str);
      this.managerForm.controls["mobile_format"].setValue(strFormat);
    }
  }
  cancel(): void {
    this.croppedImage = null;
    this.imageChangedEvent = null;
    this.isUploading = false;
    setTimeout(() => {
      /** spinner ends after 1 seconds */
      this.isUploading = true;
    }, 1000);
  }
  onlyNumberKey(event) {
    return event.charCode == 8 || event.charCode == 0 ? null : event.charCode >= 48 && event.charCode <= 57;
  }
}
