import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalDismissReasons, NgbModal, NgbModalOptions, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import { FullComponent } from '../../../layouts/full/full.component';
import { AuthServiceLocal } from '../../auth.service';
import * as moment from 'moment';
@Component({
  selector: 'app-submit-assessment',
  templateUrl: './submit-assessment.component.html',
  styleUrls: ['./submit-assessment.component.scss']
})
export class SubmitAssessmentComponent implements OnInit {
  @ViewChild(FullComponent,null) full: FullComponent;
  isLoading:boolean = false;
  isLoading1:boolean = false
  isSubmit:boolean = false;

  noteText:string = "";
  
  assementList: Array<object> = [];
  attachments:Array<object> = [];
  modelOptions: NgbModalOptions = {
    backdrop: "static",
    keyboard: false
  };
  closeResult: string;
  assessmentId:string
  private modalRef: NgbModalRef;
  Upload:any;
  activeIndex:any
  ;
  constructor( private modalService: NgbModal,
    private EmployeeService:AuthServiceLocal,
    private _snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private router: Router,
    ) { }

  ngOnInit() {
    this.assessmentId= this.route.snapshot.params["id"];
    this.getAssessmentDetail();

  }

  assementdetail:any;
  managerNote:any;
  assementHead:any;
  attachmentData:any;
  attachmentDataManager:any;
  getAssessmentDetail() {
    this.isLoading = true;
		this.EmployeeService.candidateAssement(this.assessmentId).subscribe (
		  res => {
			this.isLoading = false;
			if ((res["message"] = "Success")) {
        this.assementdetail = res.data.assessmentList[0];
        this.assementHead = res.data.assessmentList;
        this.assementList =  res.data.assessmentList[0]['skills']
        this.assementHead.forEach(element => {
          if(element['track'] == 'clientCare') element['tracklist'] = 'Client Care'
          if(element['track'] == 'patientCare') element['tracklist'] = 'Patient Care'
          if(element['track'] == 'leadership') element['tracklist'] = 'Leadership'
          if(element['track'] == 'hospitalOnboarding') element['tracklist'] = 'Hospital Operations'
         });
        this.noteText =res.data["assessmentList"][0]['notes'];
        this.managerNote  =res.data.assessmentList[0]['managerNotes']
        
        this.assementList.forEach(element => {
          element["basic"] =false;
          element["intermediate"] =false;
          element['mastered'] = false;
          
        });
        this.setAssessmenData();
        if (res.data.assessmentList[0]["attachments"]) {
          this.attachmentData = res.data.assessmentList[0]["attachments"][0];
        }
        if (res.data.assessmentList[0]["attachments"]) {
          this.attachmentDataManager = res.data.assessmentList[0]["attachments"][0];
        }

			} else {
			  this.assementList = [];
			}      },
		  err => {
			this.isLoading = false;
		  }
		);
  }
  closeModal(){
    this.modalRef.close();

  }
  setAssessmenData() {
    this.assementList.forEach((element) => {
      if (element["skillLevel"] == "basic") {

        if(element['status'] =='rejected' || element['status'] =='pending' ) {
          element["basic"] = false;
        }else {
          element["basic"] = true;

        }
       
      }
      if (element["skillLevel"] == "intermediate") {
      
        if(element['status'] =='rejected' || element['status'] =='pending' ) {
          element["intermediate"] = false;
        }else {
          element["intermediate"] = true;
        }
      }
      if (element["skillLevel"] == "mastered") {
        
        if(element['status'] =='rejected' || element['status'] =='pending' ) {
          element["mastered"] = false;
        }else {
          element["mastered"] = true;
        }
      }
    });
  }
  auditHistory:any;
  skillDetail:any;
  // HistoryModalOpen(row,content,btn) {
  //   this.modelOptions = {
  //     backdrop: "static",
  //     keyboard: false,
  //     centered:true,
  //     size:'lg',
  //   };
  // this.isLoading = true;
  // this.skillDetail = row;
  //   this.EmployeeService.getskillHistory(row._id).subscribe (
	// 	  res => {
	// 		this.isLoading = false;
	// 		if ((res["message"] = "Success")) {
  //      this.auditHistory = res.data.auditLogList;
  //      btn && btn.parentElement && btn.parentElement.parentElement && btn.parentElement.parentElement.blur();
  //   this.modalRef = this.modalService.open(content, this.modelOptions);
  //   this.modalRef.result.then(
  //     result => {
  //       this.closeResult = `Closed with: ${result}`;
  //     },
  //     reason => {
  //       this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
  //     }
  //   );

     

	// 		} else {
			 
	// 		}      },
	// 	  err => {
	// 		this.isLoading = false;
	// 	  }
	// 	);
  //    }
  //    formatTime(epoc) {
  //     let time =   moment(epoc).format('MMMM d, YYYY');
  //     return time;
  //    }
  basicChange(index,value) {
    this.isSubmit  = true;
    if (value == true) {
      this.assementList[index]['basic'] =true;
      this.assementList[index]['skillLevel'] ="basic";
      this.assementList[index]['status'] ="submitted";
      this.assementList[index]['intermediate'] =false;
      this.assementList[index]['mastered'] =false;
    } else {
      this.assementList[index]['basic'] =false;
      this.assementList[index]['skillLevel'] ="NA";
      this.assementList[index]['status'] ="pending";
    }

  }
  intermediateChange(index,value) {
    this.isSubmit  = true;
    if (value == true) {
      this.assementList[index]['basic'] =false;
      this.assementList[index]['intermediate'] =true;
      this.assementList[index]['mastered'] =false;
      this.assementList[index]['skillLevel'] ="intermediate";
      this.assementList[index]['status'] ="submitted";
    } else {
      this.assementList[index]['basic'] =false;
      this.assementList[index]['skillLevel'] ="NA";
      this.assementList[index]['status'] ="pending";
    }
    
  }
  masteredChange(index,value) {
    this.isSubmit  = true;
    if (value == true) {
      this.assementList[index]['basic'] =false;
      this.assementList[index]['intermediate'] =false;
      this.assementList[index]['mastered'] =true;
      this.assementList[index]['skillLevel'] ="mastered";
      this.assementList[index]['status'] ="submitted";
    } else {
      this.assementList[index]['mastered'] =false;
      this.assementList[index]['skillLevel'] ="NA";
      this.assementList[index]['status'] ="pending";
    }
    
  }
  ValidateStarred(index,value) {

    this.assementList[index]['isStarred'] = value;
  }
 
 
  fileNamess:any;
  uploadAttachment(event) {
 
     this.isLoading1 = true;
     this.selectedFile = event.target.files[0];
     this.fileNamess =event.target.files[0].name;
    // this.Upload =  this.Upload.substr(length-10,length);
       if (this.selectedFile.type !=='application/pdf') {
         this.isLoading1 = false;
         Swal.fire({
           icon: 'error',
           html: 'Unsupported File Type. Only Pdf is allowed!'
       });
           return
       }
      this.isLoading1 =true;

    // console.log('selected FIle',this.selectedFile)
     this.EmployeeService.uploadPdf(this.selectedFile).subscribe(res => {
       this.isLoading1 = false;
       if (res["message"] == "Success") { 

         this.isLoading = false;
         this.attachments.push({name:this.fileNamess,type:'doc',url:res.data.imageUrl});
     //    console.log('attachmentFile',this.attachmentFile)
         this.selectedFile = null;
       } else {
         
       }
     }, err => {
      this.isLoading1 = false
     });
   }

  //  fullC () {
  //   let fullVal =  this.full.getHeader();
  //    console.log(fullVal,"HEADER VAL");
  //  }



   isNote:boolean = false;
   submitData(){
    
     if(this.noteText == '') {
      this.isNote = true;
      return;
     }else {
       this.isNote = false;
     }
     let skill =[];
     this.assementList.forEach(element => {
       skill.push({name:element['name'],originalSkillId:element['originalSkillId'],skillLevel:element['skillLevel'],status:element['status'],isStarred:element['isStarred']})
     });
     let request= {
      userAssessmentId:this.assessmentId,
      skills:skill,
      isNewSubmit: true
     }
     if(this.attachments.length > 0) {
       request['attachments'] = this.attachments;
     }
     if(this.noteText !== '') {
       request['notes']= this.noteText;
     }
     this.isLoading = true;
    this.EmployeeService.submitCandidateAssessment(request).subscribe(res => {
      this.isLoading = false;
      if (res["message"] == "Success") { 
 
        this._snackBar.open("Assessment submitted successfully!!","",{
          duration: 3000,
          horizontalPosition:'right',
          verticalPosition:'top',
          panelClass: ['failure']
        });
        this.router.navigate(["thankyou"]);
      } else {
        
      }
    }, err => {
     this.isLoading = false
    });

   }
 
  selectedFile: File;
   changeText() {
    if(this.noteText == '') {
      this.isNote = true;
      return;
     }else {
       this.isNote = false;
     }
   }

   navBack() {
    if(this.activeIndex == 0) {
      this.router.navigate(["main/employee/dashboard"]);
    } if (this.activeIndex == 2 ) {
      this.router.navigate(["main/employee/list-assessment"]);
    }
   }

    // END ----- Update user status block
    private getDismissReason(reason: any): string {
      if (reason === ModalDismissReasons.ESC) {
        return "by pressing ESC";
      } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
        return "by clicking on a backdrop";
      } else {
        return `with: ${reason}`;
      }
    }

}

