import { MediaMatcher } from "@angular/cdk/layout";
import { NavigationStart, Router } from "@angular/router";
import {
  ChangeDetectorRef,
  Component,
  NgZone,
  OnDestroy,
  ViewChild,
  HostListener,
  Directive,
  AfterViewInit,
  OnInit
} from "@angular/core";
import { MenuItems } from "../../shared/menu-items/menu-items";
import { CommonService } from "../../shared/common.service";
import { transition, trigger, query, style, animate, group, animateChild, state } from "@angular/animations";
import { MatSnackBar } from "@angular/material";
import { AuthServiceLocal } from "../../auth/auth.service";
import { StreamService } from "../../shared/stream.service";

declare var $: any;
/** @title Responsive sidenav */
@Component({
  selector: "app-full-layout",
  templateUrl: "full.component.html",
  styleUrls: ["full.component.scss"],
  animations: [
    trigger("routerTransition", [
      transition("* <=> *", [
        query(":enter, :leave", style({ position: "fixed", opacity: 0 }), { optional: true }),
        group([
          query(":enter", [style({ opacity: 0 }), animate("600ms ease-in-out", style({ opacity: 0 }))], {
            optional: true
          }),
          query(":leave", [style({ opacity: 0.5 }), animate("600ms ease-in-out", style({ opacity: 0 }))], {
            optional: true
          })
        ])
      ])
    ])
  ]
})
export class FullComponent implements OnDestroy, AfterViewInit, OnInit {
  mobileQuery: MediaQueryList;
  isMenuOpen = true;
  contentMargin: number;
  activeIndex: any;
  message: number;
  filterMessage: any;
  public screenWidth: any;
  public screenHeight: any;
  isMobile: boolean = false;
  detail: any;
  Name: string;
  routeName: string;
  profilePic: string;
  headerTitle = [];
  filterVal: string;
  userData: any;
  userRole: any;
  showScroll: boolean = true;
  isLoading: boolean = false;
  isPodcast: boolean = false;
  NoScroll = ["Search", "My Matches", "My Favorites"];
  currentScreenWidth: any;
  currentNavition: any;
  adminFilterAccess: [2, 3, 4, 5];
  private _mobileQueryListener: () => void;
  MENUITEMS = [
    {
      state: "admin/dashboard",
      name: "Dashboard",
      iconUrl: "assets/images/Dashboard_icon.svg",
      type: "link",
      iconUrlActive: "assets/images/Dashboard_icon_white.svg"
    },
    // {
    //   state: "admin/all-manager",
    //   name: "Managers",
    //   iconUrl: "assets/images/Managers_icon.svg",
    //   type: "link",
    //   iconUrlActive: "assets/images/Managers_icon_white.svg"
    // },
    // {
    //   state: "admin/view-employee",
    //   name: "Employees",
    //   iconUrl: "assets/images/Employees_icon.svg",
    //   type: "link",
    //   iconUrlActive: "assets/images/Employees_icon_color.svg"
    // },
    // {
    //   state: "admin/view-candidate",
    //   name: "Candidates",
    //   iconUrl: "assets/images/Candidates_icon.svg",
    //   type: "link",
    //   iconUrlActive: "assets/images/Candidates_icon_white.svg"
    // },
    // {
    //   state: "admin/all-resource",
    //   name: "Resources",
    //   iconUrl: "assets/images/Resources_icon.svg",
    //   type: "link",
    //   iconUrlActive: "assets/images/Resources_icon_white.svg"
    // },
    // {
    //   state: "admin/view-assessment",
    //   name: "Worksheet",
    //   iconUrl: "assets/images/Assessment_icon.svg",
    //   type: "link",
    //   iconUrlActive: "assets/images/Assessment_icon_white.svg"
    // },
    {
      state: "admin/view-user",
      name: "User",
      iconUrl: "assets/images/Managers_icon.svg",
      type: "link",
      iconUrlActive: "assets/images/Managers_icon_white.svg"
    },
    {
      state: "admin/view-company",
      name: "Company",
      iconUrl: "assets/images/Dashboard_icon.svg",
      type: "link",
      iconUrlActive: "assets/images/Dashboard_icon_white.svg"
    },
    {
      state: "admin/view-industry",
      name: "Industry",
      iconUrl: "assets/images/Assessment_icon.svg",
      type: "link",
      iconUrlActive: "assets/images/Assessment_icon_white.svg"
    },

    {
      state: "admin/view-investment",
      name: "Investment",
      iconUrl: "assets/images/Dashboard_icon.svg",
      type: "link",
      iconUrlActive: "assets/images/Dashboard_icon_white.svg"
    },
    {
      state: "admin/view-post",
      name: "Post",
      iconUrl: "assets/images/Candidates_icon.svg",
      type: "link",
      iconUrlActive: "assets/images/Candidates_icon_white.svg"
    },
    {
      state: "admin/view-setting",
      name: "Setting",
      iconUrl: "assets/images/ic_company.svg",
      type: "link",
      iconUrlActive: "assets/images/ic_company.svg"
    },
    // {
    //   state: "admin/assessment-details",
    //   name: "Assessment Details",
    //   iconUrl: "assets/images/Candidates_icon.svg",
    //   type: "nolink",
    //   iconUrlActive: "assets/images/Candidates_icon_white.svg"
    // },
    {
      state: "admin/view-master",
      name: "Manage List ",
      iconUrl: "assets/images/folder_color.svg",
      type: "navigate",
      iconUrlActive: "assets/images/folder_white.svg"
    }
  ];
  MENUMANAGER = [
    {
      state: "manager/dashboard",
      name: "Dashboard",
      iconUrl: "assets/images/Dashboard_icon.svg",
      type: "link",
      iconUrlActive: "assets/images/Dashboard_icon_white.svg"
    },
    {
      state: "manager/list-employee",
      name: "Employees",
      iconUrl: "assets/images/Employees_icon.svg",
      type: "link",
      iconUrlActive: "assets/images/Employees_icon_color.svg"
    },
    {
      state: "manager/list-resource",
      name: "Resources",
      iconUrl: "assets/images/Resources_icon.svg",
      type: "link",
      iconUrlActive: "assets/images/Resources_icon_white.svg"
    },
    {
      state: "manager/list-assessment",
      name: "Worksheet",
      iconUrl: "assets/images/Assessment_icon.svg",
      type: "link",
      iconUrlActive: "assets/images/Assessment_icon_white.svg"
    },
    {
      state: "manager/change-password",
      name: "Change Password ",
      iconUrl: "assets/images/folder_color.svg",
      type: "navigate",
      iconUrlActive: "assets/images/folder_white.svg"
    }
  ];
  MENUEMPLOYEE = [
    {
      state: "employee/dashboard",
      name: "Dashboard",
      iconUrl: "assets/images/Dashboard_icon.svg",
      type: "link",
      iconUrlActive: "assets/images/Dashboard_icon_white.svg"
    },
    {
      state: "employee/list-resource",
      name: "Resources",
      iconUrl: "assets/images/Resources_icon.svg",
      type: "link",
      iconUrlActive: "assets/images/Resources_icon_white.svg"
    },
    {
      state: "employee/list-assessment",
      name: "Worksheet ",
      iconUrl: "assets/images/Candidates_icon.svg",
      type: "link",
      iconUrlActive: "assets/images/Candidates_icon_white.svg"
    },
    {
      state: "employee/change-password",
      name: "Change Password ",
      iconUrl: "assets/images/folder_color.svg",
      type: "navigate",
      iconUrlActive: "assets/images/folder_white.svg"
    }
  ];

  constructor(
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    public menuItems: MenuItems,
    public common: CommonService,
    public router: Router,
    private streamService: StreamService
  ) {
    router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        if (event.url.startsWith("/main/")) {
          this.message = parseInt(localStorage.getItem("activeIndex"));
          this.getHeader();
        }
      }
    });
    this.mobileQuery = media.matchMedia("(min-width: 768px)");
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnInit() {
    this.activeIndex = localStorage.getItem("activeIndex");
    this.userRole = localStorage.getItem("role");
    this.userData = this.common.getUser();
    if (this.userData.role == "admin") {
      this.currentNavition = this.MENUITEMS;
    }
    if (this.userData.role == "employee") {
      this.currentNavition = this.MENUEMPLOYEE;
    }
    if (this.userData.role == "manager") {
      this.currentNavition = this.MENUMANAGER;
    }
    this.message = parseInt(localStorage.getItem("activeIndex"));
    this.getHeader();
    this.streamService.getSubscription().subscribe(message => {
      this.message = message.data;
      this.getHeader();
    });

    this.streamService.getFilterSubscription().subscribe(data => {
      this.filterMessage = data.data;
    });

    $(".rotate").click(function() {
      $(this).toggleClass("down");
    });
    this.currentScreenWidth = screen.width;
    if (this.currentScreenWidth <= 780) {
      this.isMobile = true;
    }
  }
  navName: string;
  getHeader() {
    this.headerTitle = this.currentNavition;
    this.navName = this.headerTitle[this.message].name;
  }
  // checkFilterAccessValidation() {
  //   if(this.userData.role = 'admin') {
  //     for(let i =0; i < this.adminFilterAccess.length; i++) {

  //     }

  //   }

  // }
  changePasswordNav() {
    // this.checkFilterAccessValidation()

    if (this.userData.role == "manager") {
      localStorage.setItem("activeIndex", "4");
      this.getHeader();

      this.router.navigate(["/main/manager/change-password"]);
    }
    if (this.userData.role == "employee") {
      localStorage.setItem("activeIndex", "3");
      this.getHeader();
      this.router.navigate(["/main/employee/change-password"]);
    }
  }

  logoutUser() {
    this.common.logOut();
  }
  filterFunction() {
    let filtervalue = this.filterVal.trim();
    this.streamService.streamFilterMessage(filtervalue);
  }
  enterFilterVal() {
    let filtervalue = this.filterVal.trim();
    this.streamService.streamFilterMessage(filtervalue);
  }

  // navigateSroll() {
  //   this.showScroll = this.NoScroll.indexOf(this.headerTitle) == -1;
  // }

  WindowResize(e) {
    // setTimeout(() => { window.dispatchEvent(new Event('resize')); }, 260);
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  readLocalStorageValue(key: string): number {
    return parseInt(localStorage.getItem(key));
  }
  logOut() {
    this.common.logOut();
  }
  ngAfterViewInit() {}

  @HostListener("window:resize", ["$event"])
  onResize(event) {
    this.screenWidth = window.innerWidth;
    this.screenHeight = window.innerHeight;
    if (this.screenWidth <= 780) {
      this.isMobile = true;
    } else {
      this.isMobile = false;
    }
  }
}
