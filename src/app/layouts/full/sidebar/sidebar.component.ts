import {
  EventEmitter,
  ChangeDetectorRef,
  Component,
  NgZone,
  OnDestroy,
  ViewChild,
  HostListener,
  Directive,
  AfterViewInit,
  Output,
  Input,
  OnInit
} from "@angular/core";
import { MediaMatcher } from "@angular/cdk/layout";
import { MenuItems } from "../../../shared/menu-items/menu-items";
import { CommonService } from "../../../shared/common.service";
import { StreamService } from "../../../../app/shared/stream.service";
import { NavigationStart, Router } from "@angular/router";

@Component({
  selector: "app-sidebar",
  templateUrl: "./sidebar.component.html",
  styleUrls: ["./sidebar.component.scss"]
})
export class AppSidebarComponent implements OnInit, OnDestroy {
  MENUITEMS = [
    {
      state: "admin/dashboard",
      name: "Dashboard",
      iconUrl: "assets/images/Dashboard_icon.svg",
      type: "link",
      iconUrlActive: "assets/images/Dashboard_icon_white.svg"
    },
    // {
    //   state: "admin/all-manager",
    //   name: "Managers",
    //   iconUrl: "assets/images/Managers_icon.svg",
    //   type: "link",
    //   iconUrlActive: "assets/images/Managers_icon_white.svg"
    // },
    // {
    //   state: "admin/view-employee",
    //   name: "Employees",
    //   iconUrl: "assets/images/Employees_icon.svg",
    //   type: "link",
    //   iconUrlActive: "assets/images/Employees_icon_color.svg"
    // },
    // {
    //   state: "admin/view-candidate",
    //   name: "Candidates",
    //   iconUrl: "assets/images/Candidates_icon.svg",
    //   type: "link",
    //   iconUrlActive: "assets/images/Candidates_icon_white.svg"
    // },
    // {
    //   state: "admin/all-resource",
    //   name: "Resources",
    //   iconUrl: "assets/images/Resources_icon.svg",
    //   type: "link",
    //   iconUrlActive: "assets/images/Resources_icon_white.svg"
    // },
    // {
    //   state: "admin/view-assessment",
    //   name: "Worksheet",
    //   iconUrl: "assets/images/Assessment_icon.svg",
    //   type: "link",
    //   iconUrlActive: "assets/images/Assessment_icon_white.svg"
    // },
    {
      state: "admin/view-user",
      name: "User",
      iconUrl: "assets/images/Managers_icon.svg",
      type: "link",
      iconUrlActive: "assets/images/Managers_icon_white.svg"
    },
    {
      state: "admin/view-company",
      name: "Company",
      iconUrl: "assets/images/ic_company.svg",
      type: "link",
      iconUrlActive: "assets/images/ic_company.svg"
    },

    {
      state: "admin/view-industry",
      name: "Industry",
      iconUrl: "assets/images/ic_industry.svg",
      type: "link",
      iconUrlActive: "assets/images/ic_industry.svg"
    },
    {
      state: "admin/view-investment",
      name: "Investment",
      iconUrl: "assets/images/ic_investment.svg",
      type: "link",
      iconUrlActive: "assets/images/ic_investment.svg"
    },
    {
      state: "admin/view-post",
      name: "Post",
      iconUrl: "assets/images/ic_post.svg",
      type: "link",
      iconUrlActive: "assets/images/ic_post.svg"
    },
    {
      state: "admin/view-setting",
      name: "Settings",
      iconUrl: "assets/images/settings .svg",
      type: "link",
      iconUrlActive: "assets/images/settings .svg"
    }

    // {
    //   state: "admin/assessment-details",
    //   name: "Assessment Details",
    //   iconUrl: "assets/images/Candidates_icon.svg",
    //   type: "nolink",
    //   iconUrlActive: "assets/images/Candidates_icon_white.svg"
    //},
    // {
    //   state: "admin/view-master",
    //   name: "Manage List ",
    //   iconUrl: "assets/images/folder_color.svg",
    //   type: "navigate",
    //   iconUrlActive: "assets/images/folder_white.svg"
    // }
  ];
  MENUMANAGER = [
    {
      state: "manager/dashboard",
      name: "Dashboard",
      iconUrl: "assets/images/Dashboard_icon.svg",
      type: "link",
      iconUrlActive: "assets/images/Dashboard_icon_white.svg"
    },
    {
      state: "manager/list-employee",
      name: "Employees",
      iconUrl: "assets/images/Employees_icon.svg",
      type: "link",
      iconUrlActive: "assets/images/Employees_icon_color.svg"
    },
    {
      state: "manager/list-resource",
      name: "Resources",
      iconUrl: "assets/images/Resources_icon.svg",
      type: "link",
      iconUrlActive: "assets/images/Resources_icon_white.svg"
    },
    {
      state: "manager/list-assessment",
      name: "Worksheet",
      iconUrl: "assets/images/Assessment_icon.svg",
      type: "link",
      iconUrlActive: "assets/images/Assessment_icon_white.svg"
    }
    // { state: 'manager/change-password', name: 'Change Password ', iconUrl:"assets/images/password_color.svg", type: 'navigate', iconUrlActive:'assets/images/password_white.svg'},
  ];
  MENUEMPLOYEE = [
    {
      state: "employee/dashboard",
      name: "Dashboard",
      iconUrl: "assets/images/Dashboard_icon.svg",
      type: "link",
      iconUrlActive: "assets/images/Dashboard_icon_white.svg"
    },
    {
      state: "employee/list-resource",
      name: "Resources",
      iconUrl: "assets/images/Resources_icon.svg",
      type: "link",
      iconUrlActive: "assets/images/Resources_icon_white.svg"
    },
    {
      state: "employee/list-assessment",
      name: "Worksheet",
      iconUrl: "assets/images/Assessment_icon.svg",
      type: "link",
      iconUrlActive: "assets/images/Assessment_icon_white.svg"
    }
    // { state: 'employee/change-password', name: 'Change Password ', iconUrl:"assets/images/password_color.svg", type: 'navigate', iconUrlActive:'assets/images/password_white.svg'},
  ];
  @Output() messageEvent = new EventEmitter<string>();
  @Input() isMobileActive: any;
  mobileQuery: MediaQueryList;
  isAllow: boolean = false;
  isPodcast: boolean = false;
  currentNavition = [];
  userData: any;
  userP: any;
  userG: any;
  userName: any;
  podcastVal: boolean = false;
  private _mobileQueryListener: () => void;
  decodedToken: any;
  constructor(
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    public menuItems: MenuItems,
    public common: CommonService,
    private streamService: StreamService,
    public router: Router
  ) {
    router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        if (event.url.startsWith("/main")) {
          this.menuItems;
        }
      }
    });
    this.mobileQuery = media.matchMedia("(min-width: 768px)");
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }
  ngOnInit() {
    this.userData = this.common.getUser();
    if (this.userData.role == "admin") {
      this.currentNavition = this.MENUITEMS;
    }
    if (this.userData.role == "employee") {
      this.currentNavition = this.MENUEMPLOYEE;
    }
    if (this.userData.role == "manager") {
      this.currentNavition = this.MENUMANAGER;
    }
  }

  sendRouteVal(index) {
    this.streamService.streamMessage(index);
    // this.messageEvent.emit(index);
  }
  activeRoute(index) {
    localStorage.setItem("activeIndex", index);
    this.sendRouteVal(index);
  }

  readLocalStorageValue(key: string): number {
    return parseInt(localStorage.getItem(key));
  }
  logOut() {
    this.common.logOut();
  }
  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }
}
